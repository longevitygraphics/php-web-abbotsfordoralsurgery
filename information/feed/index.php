<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>Abbotsford Oral Surgery and Dental Implant Centre</title>
	<atom:link href="https://www.abbotsfordoralsurgery.com/information/feed/" rel="self" type="application/rss+xml" />
	<link>https://www.abbotsfordoralsurgery.com</link>
	<description></description>
	<lastBuildDate>
	Tue, 26 Mar 2019 21:53:02 +0000	</lastBuildDate>
	<language>en-US</language>
	<sy:updatePeriod>
	hourly	</sy:updatePeriod>
	<sy:updateFrequency>
	1	</sy:updateFrequency>
	<generator>https://wordpress.org/?v=5.1.1</generator>

<image>
	<url>https://www.abbotsfordoralsurgery.com/wp-content/uploads/cropped-ABOT-Favicon-32x32.png</url>
	<title>Abbotsford Oral Surgery and Dental Implant Centre</title>
	<link>https://www.abbotsfordoralsurgery.com</link>
	<width>32</width>
	<height>32</height>
</image> 
	<item>
		<title>First Visit</title>
		<link>https://www.abbotsfordoralsurgery.com/information/first-visit/</link>
				<comments>https://www.abbotsfordoralsurgery.com/information/first-visit/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/information/first-visit/</guid>
				<description><![CDATA[<p>The team at Abbotsford Oral Surgery and Dental Implant Centre is excited to offer you a warm welcome. Our goal is to help you feel relaxed and comfortable during all phases of your treatment at our practice and to offer you the very highest quality of care. During your initial consultation appointment with Dr. Esmail, you will receive a full&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/information/first-visit/">First Visit</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The team at Abbotsford Oral Surgery and Dental Implant Centre is excited to offer you a warm welcome. Our goal is to help you feel relaxed and comfortable during all phases of your treatment at our practice and to offer you the very highest quality of care. During your initial consultation appointment with Dr. Esmail, you will receive a full examination and an explanation of your diagnosis, and a treatment plan will develop based on your oral surgery goals and condition. Most oral surgery treatments are performed during a later appointment, but some procedures may be performed the same day as your consultation.  </p>
<p>It is likely that diagnostic images will be taken during your first visit. If your general dentist or referring physician has already taken X-rays or scans, please remember to bring them with you to your appointment. You may also request that your dentist or doctor forward the images to our practice for review. </p>
<p>For the most efficient evaluation and treatment planning, we ask that you please bring the following to your first appointment:</p>
<ul>
<li>Your health and/or dental insurance information</li>
<li>A list of current medications you are taking, if any</li>
<li>Any referral forms or X-rays given to you by your dentist or physician</li>
<li>A valid driver’s license or photo ID</li>
<li>Your payment information</li>
</ul>
<p>It is important to note that a parent or guardian must accompany all patients under the age of 18 for the consultation visit.</p>
<p>Please be sure to alert us to any prior medical conditions that your oral surgeon should be aware of before surgery, including diabetes, high blood pressure, artificial heart valves and joints, rheumatic fever, and other significant medical conditions. If you are currently taking medications such as heart medications, aspirin, anticoagulant therapy, etc., please let us know. If you have not previously completed the medical/dental health questionnaire, the patient information form, and the financial information form, we ask you to please arrive a few minutes early to allow time to complete these important forms. </p>
<p>Thank you for taking the time to review this information. We look forward to providing you with excellent oral surgery care in an inviting and professional setting.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/information/first-visit/">First Visit</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/information/first-visit/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Anesthesia Options</title>
		<link>https://www.abbotsfordoralsurgery.com/information/anesthesia-options/</link>
				<comments>https://www.abbotsfordoralsurgery.com/information/anesthesia-options/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/information/anesthesia-options/</guid>
				<description><![CDATA[<p>Our team understands that undergoing surgical procedures of any kind can cause some anxiety, and we want you to know that we work hard to ensure that you feel comfortable and relaxed throughout your experience. Our practice offers a variety of anesthesia options to help ease your anxiety and eliminate pain during surgery. When you come in for your initial&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/information/anesthesia-options/">Anesthesia Options</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>Our team understands that undergoing surgical procedures of any kind can cause some anxiety, and we want you to know that we work hard to ensure that you feel comfortable and relaxed throughout your experience. Our practice offers a variety of anesthesia options to help ease your anxiety and eliminate pain during surgery. When you come in for your initial consultation appointment, your oral surgeon will choose the best anesthesia option for you based on the type of procedure you are having, your personal preference, and your health requirements. Anesthesia carries some risk, so it is important that you receive anesthesia from a doctor that has the proper training and expertise to ensure a safe sedation experience. As an oral and maxillofacial surgeon, Dr. Esmail has completed hospital-based anesthesia training and maintains certification in Advanced Cardiac Life Support. He is highly experienced in administering several types of office-based anesthesia, and our staff members are trained in assisting with anesthesia, including IV anesthesia. We monitor our patients continuously during and after surgery.  </p>
<h2>Types of Anesthesia</h2>
<p>At our practice, we offer local anesthesia, nitrous oxide, IV sedation, and, in some cases, general anesthesia. </p>
<ul>
<li><strong>Local Anesthesia.</strong> A local anesthetic called lidocaine is administered into the surgical area. A local anesthetic is used along with the other methods of anesthesia but can be used alone for simple oral surgery procedures, such as tooth extraction. Local anesthesia allows the patient to remain completely conscious throughout the procedure.</li>
<li><strong>Office-Based Intravenous (IV) Sedation.</strong> This type of anesthesia is administered through an intravenous line (IV). Also known as Dental Intravenous Anesthesia, or Twilight Sedation/Sleep, this type of anesthesia is very calming and will alleviate the anxiety associated with your treatment. Though you may not always be asleep while under intravenous sedation, you will be comfortable, relaxed, and drifting in and out of consciousness. A designated driver is required to take you home after your procedure if you receive IV anesthesia.</li>
<li><strong>Hospital- or Surgery Center-Based General Anesthesia.</strong> If you are undergoing extensive procedures such as face and jaw reconstruction, or TMJ surgery, you may be admitted to a hospital or surgery center where you will receive general anesthesia from an anesthesiologist. Hospital-based general anesthesia is also indicated for patients with medical conditions such as heart disease or lung disease who require general anesthesia for their procedure. </li>
</ul>
<p>If you have any concerns regarding the anesthesia that will be administered during your oral surgery procedure, please share your thoughts with Dr. Esmail and your care team when you have your consultation. Your comfort, well-being, and safety are important to us.</p>
<h2>Anesthesia Options in Abbotsford, BC</h2>
<p>The surgical staff at Abbotsford Oral Surgery and Dental Implant Centre is highly trained and qualified to administer several types of anesthesia in the comfort of our practice. You will be in excellent hands during your oral surgery procedure. For more information about your anesthesia options at our practice, or to schedule a consultation with Dr. Esmail, please call us. Thank you for the opportunity to offer you the best of care.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/information/anesthesia-options/">Anesthesia Options</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/information/anesthesia-options/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>3D Scanning</title>
		<link>https://www.abbotsfordoralsurgery.com/information/3d-scanning/</link>
				<comments>https://www.abbotsfordoralsurgery.com/information/3d-scanning/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/information/3d-scanning/</guid>
				<description><![CDATA[<p>It is crucial for our team to have the most detailed information available about your oral health to develop and carry out your treatment plan for the very best long-term results. To this end, Abbotsford Oral Surgery and Dental Implant Centre is outfitted with state-of-the-art diagnostic tools such as cone beam CT technology for highly accurate three-dimensional images. When detailed&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/information/3d-scanning/">3D Scanning</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>It is crucial for our team to have the most detailed information available about your oral health to develop and carry out your treatment plan for the very best long-term results. To this end, Abbotsford Oral Surgery and Dental Implant Centre is outfitted with state-of-the-art diagnostic tools such as cone beam CT technology for highly accurate three-dimensional images. When detailed 3D diagnostic images are employed in surgeries, patients not only experience a more precise result, they often have a faster recovery because their procedure was less invasive. Taking the guesswork out of the surgery allows your oral surgeon to provide you with the best experience possible. </p>
<p>3D imaging is used to diagnose and develop treatment plans for a wide variety of procedures, from assessing the exact position of impacted teeth in wisdom teeth extractions to placing dental implants. If you haven’t received recent 3D imaging before your initial consultation, you may receive it in our practice. Our imaging is digital; therefore, the amount of radiation exposure you will receive is just a fraction of what is received from conventional X-rays. Digital imagery is also helpful for quick sharing with your referring doctor, allowing your medical team to collaborate easily on your care.</p>
<h2>Benefits of 3D Scanning</h2>
<p>3D scanning offers comprehensive digital information about your specific anatomy and condition — with far more detail than traditional X-rays. This type of cutting-edge diagnostic technology helps your oral surgeon perform efficient and precise procedures.</p>
<p>Benefits of 3D scanning include the following:</p>
<ul>
<li>Reduced radiation exposure (in comparison to traditional X-rays).</li>
<li>Detailed images of your anatomy and condition.</li>
<li>Multi-angled viewing of your anatomy.</li>
<li>Improved diagnostic capability and treatment plans.</li>
<li>Lower risk of surprises during the procedure and no guesswork.</li>
<li>Instant results! 3D images are immediately available for your oral surgeon to view and share with other members of your care team.</li>
</ul>
<p>3D scanning provides high-quality, three-dimensional images of your anatomy for the most detailed information available about your condition.  </p>
<h2>3D Scanning in Abbotsford, BC</h2>
<p>If you require additional imaging beyond what your dentist has provided us, it is likely that Dr. Esmail may recommend 3D scans during your consultation appointment at Abbotsford Oral Surgery and Dental Implant Centre. If you have questions about the advanced diagnostic tools offered at our practice, or if you or a member of your family would like to schedule a consultation with the oral surgeon, please contact our practice.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/information/3d-scanning/">3D Scanning</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/information/3d-scanning/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Financing &#038; Insurance</title>
		<link>https://www.abbotsfordoralsurgery.com/information/financing-insurance/</link>
				<comments>https://www.abbotsfordoralsurgery.com/information/financing-insurance/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/information/financing-insurance/</guid>
				<description><![CDATA[<p>Our Abbotsford Oral Surgery and Dental Implant Centre team is dedicated to delivering excellent oral surgery care at a reasonable cost to our patients. For your convenience, we accept Visa®, Mastercard®, and debit cards. We do not accept cheques. Payment is due at the time of service unless other arrangements have been made in advance. We are happy to work&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/information/financing-insurance/">Financing &#038; Insurance</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>Our Abbotsford Oral Surgery and Dental Implant Centre team is dedicated to delivering excellent oral surgery care at a reasonable cost to our patients. For your convenience, we accept Visa<sup>®</sup>, Mastercard<sup>®</sup>, and debit cards. We do not accept cheques. Payment is due at the time of service unless other arrangements have been made in advance. </p>
<p>We are happy to work with you to maximize your insurance reimbursement for covered procedures. Pre-determinations must be in writing and provided before your surgery for our practice to honor it. Please remember that you are fully responsible for all fees charged by this practice regardless of your insurance coverage.</p>
<p>Thank you for taking the time to review this information. If you have questions or concerns regarding your account, please contact us, and our well-informed staff will be happy to help.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/information/financing-insurance/">Financing &#038; Insurance</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/information/financing-insurance/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
	</channel>
</rss>
