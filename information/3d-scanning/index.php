<!DOCTYPE html>
<?php
$page_title       = '3D Scanning in Abbotsford, BC';
$doc_title        = '3D Scanning';
$meta_description = '3D scanning is crucial because it helps us obtain accurate images of your condition, which we use to plan a detailed and personalized treatment plan.';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="information-template-default single single-information postid-99091 single-format-standard custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/WebPage">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="headline">3D Scanning</h1>
            </header>
            <div class="breadcrumb" itemprop="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">You are
                here: <span class="breadcrumb-link-wrap" itemprop="itemListElement" itemscope
                            itemtype="https://schema.org/ListItem"><a class="breadcrumb-link" href="../../index.html"
                                                                      itemprop="item"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                <span class="breadcrumb-link-wrap" itemprop="itemListElement" itemscope
                      itemtype="https://schema.org/ListItem"><a class="breadcrumb-link" href="../index.html"
                                                                itemprop="item"><span class="breadcrumb-link-text-wrap"
                                                                                      itemprop="name">Patient Information</span></a><meta
                            itemprop="position" content="2"></span> <span aria-label="breadcrumb separator">/</span> 3D
                Scanning
            </div>
            <div class="content-sidebar-wrap">
                <main class="content" id="genesis-content">
                    <div class="row no-gutters">
                        <div class=" col-md-12 main_header" itemscope itemtype="http://schema.org/ImageObject">
                            <a href="http://youtu.be/SG-tGWNAtgM?rel=0&amp;autohide=1&amp;showinfo=0"
                               class="fancybox-youtube" title='3D imaging in Abbotsford, BC'>
                                <span id="playhover"></span>
                                <img width="1144" height="600"
                                     src="../../wp-content/uploads/CBCT-procedure-1144x600.jpg"
                                     class="attachment-testimonial-img size-testimonial-img wp-post-image"
                                     alt="What is the importance of 3D imaging in Abbotsford, BC?"/> </a>
                        </div>
                    </div>
                    <div class="clearboth"></div>

                    <article
                            class="post-99091 information type-information status-publish format-standard has-post-thumbnail entry secondary_color">
                        <div class="entry-content" itemprop="text"><p>It is crucial for our team to have the most
                                detailed information available about your oral health to develop and carry out your
                                treatment plan for the very best long-term results. To this end, Abbotsford Oral Surgery
                                and Dental Implant Centre is outfitted with state-of-the-art diagnostic tools such as
                                cone beam CT technology for highly accurate three-dimensional images. When detailed 3D
                                diagnostic images are employed in surgeries, patients not only experience a more precise
                                result, they often have a faster recovery because their procedure was less invasive.
                                Taking the guesswork out of the surgery allows your oral surgeon to provide you with the
                                best experience possible. </p>
                            <p>3D imaging is used to diagnose and develop treatment plans for a wide variety of
                                procedures, from assessing the exact position of impacted teeth in wisdom teeth
                                extractions to placing dental implants. If you haven’t received recent 3D imaging before
                                your initial consultation, you may receive it in our practice. Our imaging is digital;
                                therefore, the amount of radiation exposure you will receive is just a fraction of what
                                is received from conventional X-rays. Digital imagery is also helpful for quick sharing
                                with your referring doctor, allowing your medical team to collaborate easily on your
                                care.</p>
                            <h2>Benefits of 3D Scanning</h2>
                            <p>3D scanning offers comprehensive digital information about your specific anatomy and
                                condition — with far more detail than traditional X-rays. This type of cutting-edge
                                diagnostic technology helps your oral surgeon perform efficient and precise
                                procedures.</p>
                            <p>Benefits of 3D scanning include the following:</p>
                            <ul>
                                <li>Reduced radiation exposure (in comparison to traditional X-rays).</li>
                                <li>Detailed images of your anatomy and condition.</li>
                                <li>Multi-angled viewing of your anatomy.</li>
                                <li>Improved diagnostic capability and treatment plans.</li>
                                <li>Lower risk of surprises during the procedure and no guesswork.</li>
                                <li>Instant results! 3D images are immediately available for your oral surgeon to view
                                    and share with other members of your care team.
                                </li>
                            </ul>
                            <p>3D scanning provides high-quality, three-dimensional images of your anatomy for the most
                                detailed information available about your condition. </p>
                            <h2>3D Scanning in Abbotsford, BC</h2>
                            <p>If you require additional imaging beyond what your dentist has provided us, it is likely
                                that Dr. Esmail may recommend 3D scans during your consultation appointment at
                                Abbotsford Oral Surgery and Dental Implant Centre. If you have questions about the
                                advanced diagnostic tools offered at our practice, or if you or a member of your family
                                would like to schedule a consultation with the oral surgeon, please contact our
                                practice.</p>

                        </div>
                    </article>
                    <div class="row cta-footer ">
                        <div class="col-md-3">
                            <a href="../../contact-us/index.html">
                                <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                                </button>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <p>
                                As a patient of our office, we want it to be as easy as possible for you to visit our
                                team for care. You can request an appointment by filling out our online form. </p>
                        </div>
                    </div>
                </main>
            </div>
	        <?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
    <style type="text/css">
        .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
            background-color: #1171AF !important;
            color: #fff !important;
        }

        .secondary_color {
            background-color: #46AD4C;
        }

        .highlight_color {
            background-color: #32C5F4 !important;
        }

        ;
        .site-inner {
            background-color: #1171AF !important;
        }

        /*Main Homepage*/
        .gradient {
            background: #00AEEF; /* Old browsers */
            background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
            background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
            Padding: 10%;
        }

        .home input {
            color: rgba(17, 113, 175, 1) !important;
        }

        /*body.custom-background {
		  background-color: rgba(17,113,175,1) !important;
		}*/

        /*Menu*/
        .nav-primary .sub-menu a {
            background-color: #1171AF;
            border-color: #fff;
            color: #fff !important;
        }

        .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
            background-color: #1171AF;
        }

        /*Add primary color to the number CTA*/
        .callus a {
            font-weight: bold;
            color: #1171AF !important;
        }

        /*Images*/
        .background_cta {
            background-image: url();
        }

        .home-map-image {
            background-image: url();
            background-size: cover;
            background-position: Center Center !important;
        }

        /*Testimonial Page*/
        .related_videos {
            border-top: 7px solid #46AD4C;
        }

        /*Youtube Video */
        .video_thumb {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: top center;
            height: 400px;
        }

        /*Change play button color on all inline video images*/
        .content #playhover,
        .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
        .main-home #playhover,
        .page-template-hero-min-landing #playhover,
        .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
        .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
            background: url(../../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
        }

        /*Add line to the botton of inline video image*/
        article #playhover + img {
            border-bottom: 5px solid #46AD4C;
        }


        /*Fancy Overlay*/
        #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
            background-image: url(../../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
            background-size: cover !important;
            background-position: left top !important;
        }

        /*Archive Pages*/
        .archive-description, .author-box {
            border-top: 7px solid #46AD4C;
            border-left: 0px;
            border-right: 0px;
        }

        .archive .entry-header a {
            color: #fff;
            font-size: 1em;
        }


        /*Team Page*/
        .tiled-gallery .gallery-row, .bio-image img {
            border-bottom: 3px solid #1171AF;
        }

        /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
        .team .entry-content img, .team .gallery-item img {
            border-bottom: 5px solid #1171AF;
        }

        /*Links*/
        a {
            color: #1171AF;
            text-decoration: none !important;
        }

        /*Change current menu color*/
        /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

        /*BTNS*/
        .button {
            background-color: #46AD4C !important;
        }

        /*Add colored bullet points*/
        .entry-content ul {
            list-style-type: none;
            position: relative;
            padding-left: 0;
        }

        .entry-content ul > li {
            list-style: none;
        }

        .entry-content ul > li:before {
            content: "• \00a0 \00a0 \00a0";
            color: #1171AF; /* Color of the bullet */
            position: absolute;
            left: -1em;
            margin-right: 5px;
        }

        /*Remove bullets on feedback page*/
        .page-template-page_feedback .entry-content ul > li:before {
            content: "" !important;
        }

        /* Video Player Pages
		--------------------------------------------- */
        .fancybox-wrap:before {
            background: url(../../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
            content: ".";
            position: relative;
            width: 100%;
            height: 130px;
            z-index: 8040;
            display: block;
            margin-bottom: 20px;
            background-size: contain !important;
        }

        /*Homepage - Max Width HeroCard - change line under image*/
        .page-template-hero-max-landing > a > img {
            border-bottom: 4px solid #1171AF;
        }

        /*change color on landinage page background*/
        .animated-home {
            background-color: #1171AF;
        }

        /*Homepage - Max Width HeroCard and full page content - change line under image*/
        .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
        .page-template-hero-max-landing > .row > .col-md-12 a img {
            border-bottom: 4px solid #1171AF;
            width: 100%;
        }


        /* Archive Page
		--------------------------------------------- */
        .circle-text:after {
            background-color: #32C5F4 !important;
        }

        body .entry-content .gfield_label {
            font-size: 30px !important;
            color: #1171AF;
        }

        /*Add BG image to full screen pages.  Going to remove this for now*/
        /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
			background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
			background-size: cover;
			background-position: left top !important;
		}*/

        .full-width-content.custom-background {
            background-image: url("../../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
            background-attachment: fixed;
        }


        body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
            border: 1px solid #1171AF;
            background-color: #FFF8BD;
        }


        .gform_wrapper .percentbar_blue {
            background-image: url('../../wp-content/themes/nuvo-express/images/opas_15.png');
            background-repeat: repeat-x;
            background-color: #1171AF;
            color: #FFF;
        }




        /* Make some mobile Tweaks
		--------------------------------------------- */
        @media only screen and (max-width: 1023px) {
            .mobile-cta {
                color: #1171AF !important;
            }

            /*Make mobile help menu primary color*/
        }

        /* This tweaks only Small devices (tablets and phones, 768px and up) */
        @media (max-width: 900px) {
            .fancybox-youtube > .feedperson, .feedperson.shade30 {
                border-bottom: 2px solid #32C5F4;
            }

            /*Add line under main videos on homepage / Move background face down*/
            div.col-md-12.row.no-gutters > a > div.feedperson {
                border-bottom: 2px solid #32C5F4;
            }

            /*Add line under CTA buttons on homepage*/
        }

        /*Add custom color to the case study block on homepage*/
        .image-holder:after {
            background-color: rgba(70, 173, 76, 0.7);

        }

        /*Add video overlay on main sizzle as a dynamic layer*/
        .home-hero {
            background: rgba(17, 113, 175, 1) !important;
        }

        @media only screen and (max-width: 1024px) {

            .home-hero:after {
                background: rgba(17, 113, 175, 0.8);
            }

        }

    </style>
</div>
	    <?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
