<?php
$page_title       = 'Nayeem Esmail, BSc, DMD, FRCD(C) in Abbotsford, BC';
$doc_title        = 'Nayeem Esmail, BSc, DMD, FRCD(C)';
$meta_description = 'Dr. Esmail is passionate about oral surgery and stays up to date with the latest techniques so he can provide the best quality care.';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="page-template page-template-single-doc page-template-single-doc-php page page-id-99100 custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/Dentist">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="name">Nayeem Esmail, BSc, DMD, FRCD(C)</h1>
            </header>
            <div class="breadcrumb">You are here: <span class="breadcrumb-link-wrap"><a class="breadcrumb-link"
                                                                                        href="../index.html"
                                                                                        itemprop="item"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                Nayeem Esmail, BSc, DMD, FRCD(C)
            </div>
            <div class="content-sidebar-wrap">
                <div class="row no-gutters">
                    <div class=" col-md-12 main_header" itemscope itemtype="http://schema.org/ImageObject">
                        <a href="http://youtu.be/evxyq-E3JnU?rel=0&amp;autohide=1&amp;showinfo=0"
                           class="fancybox-youtube" title='Nayeem Esmail, BSc, DMD, FRCD(C)'>
                            <span id="playhover"></span>
                            <img width="1144" height="600" src="../wp-content/uploads/Doc-Profile-ABOT-1144x600.jpg"
                                 class="attachment-testimonial-img size-testimonial-img wp-post-image"
                                 alt="Learn about Nayeem Esmail, BSc, DMD, FRCD(C)" itemprop="contentUrl" itemscope=""
                                 srcset="https://www.abbotsfordoralsurgery.com/wp-content/uploads/Doc-Profile-ABOT-1144x600.jpg 1144w, https://www.abbotsfordoralsurgery.com/wp-content/uploads/Doc-Profile-ABOT-300x157.jpg 300w, https://www.abbotsfordoralsurgery.com/wp-content/uploads/Doc-Profile-ABOT-1024x537.jpg 1024w"
                                 sizes="(max-width: 1144px) 100vw, 1144px" title="Nayeem Esmail, BSc, DMD, FRCD(C)"/>
                        </a>
                    </div>
                </div>
                <div class="clearboth"></div>

                <main class="content" id="genesis-content">
                    <article class="post-99100 page type-page status-publish has-post-thumbnail entry secondary_color">
                        <div class="col-md-12 docmeta">
                            <div class="spacer10 clearboth"></div>
                            <div class="col-md-2 col-sm-3"><h3>Name</h3>Dr. Nayeem Esmail<h3>Title</h3>BSc, DMD, FRCD(C)
                            </div>
                            <div class="col-md-3 col-sm-3"><h3 class="">Address</h3><a
                                        href="../contact-us/index.html"><strong>Abbotsford Oral Surgery and Dental
                                        Implant Centre</strong></a><br/>
                                #305-2180 Gladwin Road<br/>
                                Abbotsford, BC V2S 0H4
                            </div>
                            <div class="col-md-3 col-sm-6"><h3 class="">Education</h3>
                                <li>McGill University</li>
                                <li>Boston University Goldman School of Dental Medicine</li>
                                <h3 class="">Residency</h3>
                                <li>Washington Hospital Center</li>
                            </div>
                            <div class="spacer10 clearboth hidden-lg hidden-md"></div>
                            <div class="col-md-4 col-sm-6"><h3 class="">Certification(s)</h3>
                                <li><span itemprop="memberOf">American Board of Oral and Maxillofacial Surgery</span>
                                </li>
                                <li><span itemprop="memberOf">Fellow of the Royal College of Dentists of Canada</span>
                                </li>
                            </div>
                            <div class="col-md-4 col-sm-6"><h3 class="">Interests</h3>
                                <li>Volunteering • Running<br/>
                                    Biking • Tennis • Hiking<br/>
                                    <br/>
                                </li>
                                <div class="spacer10 clearboth"></div>
                            </div>
                        </div>
                        <div class="col-md-12 row">
                            <div class="spacer20 clearboth"></div>
                            <hr>
                            <div class="spacer20 clearboth"></div>
                        </div>
                        <div class="entry-content" itemprop="description"><h2>About Dr. Esmail</h2>
                            <p>Dr. Nayeem Esmail developed a passion for helping others at a very young age. Born with a
                                rare illness, Dr. Esmail was cared for by many different healthcare professionals whose
                                collaborative and impactful work inspired him to pursue a career in healthcare. Born and
                                raised in East Africa (Rwanda), Dr. Esmail immigrated to Canada in 1996 and began his
                                training at McGill University.</p>
                            <p>Dr. Esmail’s approach to oral and maxillofacial surgery is inclusive of his experience as
                                a trained surgeon and his experience as a patient: Dr. Esmail strives to make a positive
                                impact on every patient he treats by making sure they feel safe, comfortable, and
                                informed.</p>
                            <h2>Education and Residency</h2>
                            <p>After graduating from McGill University with a Bachelor of Science degree in
                                Biochemistry, Dr. Esmail attended Boston University Goldman School of Dental Medicine,
                                where he graduated <i>Magna Cum Laude</i>. Dr. Esmail began his professional career by
                                completing a four-year surgical residency in Oral and Maxillofacial Surgery and serving
                                as Chief Resident at Washington Hospital Center. </p>
                            <p>Dr. Esmail is a Board Certified Oral and Maxillofacial Surgeon and a Fellow of the Royal
                                College of Dentists of Canada. His residency training has equipped him with the
                                knowledge and skills in a wide scope of oral and maxillofacial surgery, with special
                                interests in dental implants and tissue engineering, reconstructive surgery and trauma,
                                as well as facial cosmetic procedures. He has also conducted extensive research on sleep
                                apnea. </p>
                            <p>Dr. Esmail has a passion for teaching as well as learning advanced techniques. He
                                continues to keep updated with the newest advances in Oral and Maxillofacial Surgery and
                                is the provider of continuing education for dentists and dental assistants in BC. He
                                lectures on such topics as dental implants, sleep apnea, and anesthesia. He is also a
                                part-time clinical instructor at the University of British Columbia Dental School. </p>
                            <p>Dr. Esmail is also involved in many professional organizations and is a member of the
                                College of Dental Surgeons of BC Sedation Committee. </p>
                            <h2>Hobbies and Interests</h2>
                            <p>Dr. Esmail enjoys volunteering in the community and has participated in several medical
                                mission trips to Asia, Africa, and South America. He speaks many languages, being fluent
                                in French, Kutchi, and Swahili. He enjoys staying active through sports (tennis,
                                volleyball), yoga, and outdoor activities such as hiking, biking, running. When he has
                                time off, he and his wife like to travel and explore new places, especially those with
                                good food and wine for tasting. </p>
                        </div>
                        <div class="col-md-12 ">
                            <hr>
                            <div class="spacer20 clearboth"></div>
                        </div>
                        <div class="col-md-12 entry-content row-centered">
                            <div class="col-md-2 col-sm-6 col-xs-12 col-centered"><img
                                        src="../wp-content/uploads/ABOMS.jpg"></div>
                            <div class="col-md-2 col-sm-6 col-xs-12 col-centered"><img
                                        src="../wp-content/uploads/RCDC.jpg"></div>
                            <div class="col-md-2 col-sm-6 col-xs-12 col-centered"><img
                                        src="../wp-content/uploads/BU-Logo.jpg"></div>
                            <div class="col-md-2 col-sm-6 col-xs-12 col-centered"><img
                                        src="../wp-content/uploads/Mcgill.png"></div>
                            <div class="col-md-2 col-sm-6 col-xs-12 col-centered"><img
                                        src="../wp-content/uploads/Washington-Hospital.jpg"></div>
                        </div>
                    </article>
                </main>
            </div>
			<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
            <style type="text/css">
                .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
                    background-color: #1171AF !important;
                    color: #fff !important;
                }

                .secondary_color {
                    background-color: #46AD4C;
                }

                .highlight_color {
                    background-color: #32C5F4 !important;
                }

                ;
                .site-inner {
                    background-color: #1171AF !important;
                }

                /*Main Homepage*/
                .gradient {
                    background: #00AEEF; /* Old browsers */
                    background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
                    background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
                    background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
                    background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
                    background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
                    background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
                    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
                    Padding: 10%;
                }

                .home input {
                    color: rgba(17, 113, 175, 1) !important;
                }

                /*body.custom-background {
				  background-color: rgba(17,113,175,1) !important;
				}*/

                /*Menu*/
                .nav-primary .sub-menu a {
                    background-color: #1171AF;
                    border-color: #fff;
                    color: #fff !important;
                }

                .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
                    background-color: #1171AF;
                }

                /*Add primary color to the number CTA*/
                .callus a {
                    font-weight: bold;
                    color: #1171AF !important;
                }

                /*Images*/
                .background_cta {
                    background-image: url();
                }

                .home-map-image {
                    background-image: url();
                    background-size: cover;
                    background-position: Center Center !important;
                }

                /*Testimonial Page*/
                .related_videos {
                    border-top: 7px solid #46AD4C;
                }

                /*Youtube Video */
                .video_thumb {
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: top center;
                    height: 400px;
                }

                /*Change play button color on all inline video images*/
                .content #playhover,
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
                .main-home #playhover,
                .page-template-hero-min-landing #playhover,
                .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
                .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
                    background: url(../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
                }

                /*Add line to the botton of inline video image*/
                article #playhover + img {
                    border-bottom: 5px solid #46AD4C;
                }


                /*Fancy Overlay*/
                #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
                    background-image: url(../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
                    background-size: cover !important;
                    background-position: left top !important;
                }

                /*Archive Pages*/
                .archive-description, .author-box {
                    border-top: 7px solid #46AD4C;
                    border-left: 0px;
                    border-right: 0px;
                }

                .archive .entry-header a {
                    color: #fff;
                    font-size: 1em;
                }


                /*Team Page*/
                .tiled-gallery .gallery-row, .bio-image img {
                    border-bottom: 3px solid #1171AF;
                }

                /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
                .team .entry-content img, .team .gallery-item img {
                    border-bottom: 5px solid #1171AF;
                }

                /*Links*/
                a {
                    color: #1171AF;
                    text-decoration: none !important;
                }

                /*Change current menu color*/
                /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

                /*BTNS*/
                .button {
                    background-color: #46AD4C !important;
                }

                /*Add colored bullet points*/
                .entry-content ul {
                    list-style-type: none;
                    position: relative;
                    padding-left: 0;
                }

                .entry-content ul > li {
                    list-style: none;
                }

                .entry-content ul > li:before {
                    content: "• \00a0 \00a0 \00a0";
                    color: #1171AF; /* Color of the bullet */
                    position: absolute;
                    left: -1em;
                    margin-right: 5px;
                }

                /*Remove bullets on feedback page*/
                .page-template-page_feedback .entry-content ul > li:before {
                    content: "" !important;
                }

                /* Video Player Pages
				--------------------------------------------- */
                .fancybox-wrap:before {
                    background: url(../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
                    content: ".";
                    position: relative;
                    width: 100%;
                    height: 130px;
                    z-index: 8040;
                    display: block;
                    margin-bottom: 20px;
                    background-size: contain !important;
                }

                /*Homepage - Max Width HeroCard - change line under image*/
                .page-template-hero-max-landing > a > img {
                    border-bottom: 4px solid #1171AF;
                }

                /*change color on landinage page background*/
                .animated-home {
                    background-color: #1171AF;
                }

                /*Homepage - Max Width HeroCard and full page content - change line under image*/
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
                .page-template-hero-max-landing > .row > .col-md-12 a img {
                    border-bottom: 4px solid #1171AF;
                    width: 100%;
                }


                /* Archive Page
				--------------------------------------------- */
                .circle-text:after {
                    background-color: #32C5F4 !important;
                }

                body .entry-content .gfield_label {
                    font-size: 30px !important;
                    color: #1171AF;
                }

                /*Add BG image to full screen pages.  Going to remove this for now*/
                /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
					background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
					background-size: cover;
					background-position: left top !important;
				}*/

                .full-width-content.custom-background {
                    background-image: url("../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
                    background-attachment: fixed;
                }


                body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
                    border: 1px solid #1171AF;
                    background-color: #FFF8BD;
                }


                .gform_wrapper .percentbar_blue {
                    background-image: url('../wp-content/themes/nuvo-express/images/opas_15.png');
                    background-repeat: repeat-x;
                    background-color: #1171AF;
                    color: #FFF;
                }




                /* Make some mobile Tweaks
				--------------------------------------------- */
                @media only screen and (max-width: 1023px) {
                    .mobile-cta {
                        color: #1171AF !important;
                    }

                    /*Make mobile help menu primary color*/
                }

                /* This tweaks only Small devices (tablets and phones, 768px and up) */
                @media (max-width: 900px) {
                    .fancybox-youtube > .feedperson, .feedperson.shade30 {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under main videos on homepage / Move background face down*/
                    div.col-md-12.row.no-gutters > a > div.feedperson {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under CTA buttons on homepage*/
                }

                /*Add custom color to the case study block on homepage*/
                .image-holder:after {
                    background-color: rgba(70, 173, 76, 0.7);

                }

                /*Add video overlay on main sizzle as a dynamic layer*/
                .home-hero {
                    background: rgba(17, 113, 175, 1) !important;
                }

                @media only screen and (max-width: 1024px) {

                    .home-hero:after {
                        background: rgba(17, 113, 175, 0.8);
                    }

                }

            </style>
        </div>
		<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
