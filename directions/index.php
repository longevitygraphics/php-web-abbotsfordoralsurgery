<?php
$page_title       = 'Directions to Abbotsford Oral Surgery &amp; Dental Implant Centre in Abbotsford, BC';
$doc_title        = 'Directions';
$meta_description = 'To help you find your way to our office, we have provided a helpful link to give you directions from your current location.';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
    <body class="page-template-default page page-id-99109 custom-background custom-header header-image full-width-content"
          itemscope itemtype="https://schema.org/WebPage">
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
    <div class="site-inner">
    <div class="wrap">
        <header class="entry-header"><h1 class="entry-title" itemprop="headline">Directions</h1>
        </header>
        <div class="breadcrumb" itemprop="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">You are
            here: <span class="breadcrumb-link-wrap" itemprop="itemListElement" itemscope
                        itemtype="https://schema.org/ListItem"><a class="breadcrumb-link" href="../index.html"
                                                                  itemprop="item"><span
                            class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                        itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
            Directions
        </div>
        <div class="content-sidebar-wrap">
            <main class="content" id="genesis-content">
                <article class="post-99109 page type-page status-publish entry secondary_color">
                    <div class="entry-content" itemprop="text"><h2>Get Directions to Our Office</h2>
                        <p>We&#39;d love to help you with any and all of your oral surgery needs! Once you&#39;ve
                            made your appointment, come see us at Abbotsford Oral Surgery and Dental Implant Centre!
                            You&#39;ll find our address below for directions to our office!</p>
                        <h2>Directions to Abbotsford Oral Surgery and Dental Implant Centre</h2>
                        <h3>Abbotsford, BC</h3>
                        <p>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7397.767641803216!2d-122.31575674195159!3d49.03900507166582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5484354461678c0f%3A0xd39d9619933dd651!2sAbbotsford%20Oral%20Surgery%20and%20Dental%20Implant%20Centre%3A%20Dr.%20Nayeem%20Esmail%2C%20Inc.!5e0!3m2!1sen!2sca!4v1570469357043!5m2!1sen!2sca"
                                    width="1144" height="450" frameborder="0" style="border:0"
                                    allowfullscreen></iframe>
                        </p>
                        <h2>Need More Help?</h2>
                        <p>If you still need assistance finding our office, we are more than happy to help! Please
                            call our Abbotsford office at (604) 504-7522, and we will help ensure your journey to
                            our office is easy and your experience is pleasant!</p>
                    </div>
                </article>
                <div class="row cta-footer ">
                    <div class="col-md-3">
                        <a href="../contact-us/index.html">
                            <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                            </button>
                        </a>
                    </div>
                    <div class="col-md-9">
                        <p>
                            As a patient of our office, we want it to be as easy as possible for you to visit our
                            team for care. You can request an appointment by filling out our online form. </p>
                    </div>
                </div>
            </main>
        </div>
		<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
        <style type="text/css">
            .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
                background-color: #1171AF !important;
                color: #fff !important;
            }

            .secondary_color {
                background-color: #46AD4C;
            }

            .highlight_color {
                background-color: #32C5F4 !important;
            }

            ;
            .site-inner {
                background-color: #1171AF !important;
            }

            /*Main Homepage*/
            .gradient {
                background: #00AEEF; /* Old browsers */
                background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
                background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
                Padding: 10%;
            }

            .home input {
                color: rgba(17, 113, 175, 1) !important;
            }

            /*body.custom-background {
			  background-color: rgba(17,113,175,1) !important;
			}*/

            /*Menu*/
            .nav-primary .sub-menu a {
                background-color: #1171AF;
                border-color: #fff;
                color: #fff !important;
            }

            .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
                background-color: #1171AF;
            }

            /*Add primary color to the number CTA*/
            .callus a {
                font-weight: bold;
                color: #1171AF !important;
            }

            /*Images*/
            .background_cta {
                background-image: url();
            }

            .home-map-image {
                background-image: url();
                background-size: cover;
                background-position: Center Center !important;
            }

            /*Testimonial Page*/
            .related_videos {
                border-top: 7px solid #46AD4C;
            }

            /*Youtube Video */
            .video_thumb {
                background-size: cover;
                background-repeat: no-repeat;
                background-position: top center;
                height: 400px;
            }

            /*Change play button color on all inline video images*/
            .content #playhover,
            .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
            .main-home #playhover,
            .page-template-hero-min-landing #playhover,
            .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
            .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
                background: url(../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
            }

            /*Add line to the botton of inline video image*/
            article #playhover + img {
                border-bottom: 5px solid #46AD4C;
            }


            /*Fancy Overlay*/
            #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
                background-image: url(../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
                background-size: cover !important;
                background-position: left top !important;
            }

            /*Archive Pages*/
            .archive-description, .author-box {
                border-top: 7px solid #46AD4C;
                border-left: 0px;
                border-right: 0px;
            }

            .archive .entry-header a {
                color: #fff;
                font-size: 1em;
            }


            /*Team Page*/
            .tiled-gallery .gallery-row, .bio-image img {
                border-bottom: 3px solid #1171AF;
            }

            /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
            .team .entry-content img, .team .gallery-item img {
                border-bottom: 5px solid #1171AF;
            }

            /*Links*/
            a {
                color: #1171AF;
                text-decoration: none !important;
            }

            /*Change current menu color*/
            /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

            /*BTNS*/
            .button {
                background-color: #46AD4C !important;
            }

            /*Add colored bullet points*/
            .entry-content ul {
                list-style-type: none;
                position: relative;
                padding-left: 0;
            }

            .entry-content ul > li {
                list-style: none;
            }

            .entry-content ul > li:before {
                content: "• \00a0 \00a0 \00a0";
                color: #1171AF; /* Color of the bullet */
                position: absolute;
                left: -1em;
                margin-right: 5px;
            }

            /*Remove bullets on feedback page*/
            .page-template-page_feedback .entry-content ul > li:before {
                content: "" !important;
            }

            /* Video Player Pages
			--------------------------------------------- */
            .fancybox-wrap:before {
                background: url(../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
                content: ".";
                position: relative;
                width: 100%;
                height: 130px;
                z-index: 8040;
                display: block;
                margin-bottom: 20px;
                background-size: contain !important;
            }

            /*Homepage - Max Width HeroCard - change line under image*/
            .page-template-hero-max-landing > a > img {
                border-bottom: 4px solid #1171AF;
            }

            /*change color on landinage page background*/
            .animated-home {
                background-color: #1171AF;
            }

            /*Homepage - Max Width HeroCard and full page content - change line under image*/
            .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
            .page-template-hero-max-landing > .row > .col-md-12 a img {
                border-bottom: 4px solid #1171AF;
                width: 100%;
            }


            /* Archive Page
			--------------------------------------------- */
            .circle-text:after {
                background-color: #32C5F4 !important;
            }

            body .entry-content .gfield_label {
                font-size: 30px !important;
                color: #1171AF;
            }

            /*Add BG image to full screen pages.  Going to remove this for now*/
            /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
				background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
				background-size: cover;
				background-position: left top !important;
			}*/

            .full-width-content.custom-background {
                background-image: url("../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
                background-attachment: fixed;
            }


            body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
                border: 1px solid #1171AF;
                background-color: #FFF8BD;
            }


            .gform_wrapper .percentbar_blue {
                background-image: url('../wp-content/themes/nuvo-express/images/opas_15.png');
                background-repeat: repeat-x;
                background-color: #1171AF;
                color: #FFF;
            }




            /* Make some mobile Tweaks
			--------------------------------------------- */
            @media only screen and (max-width: 1023px) {
                .mobile-cta {
                    color: #1171AF !important;
                }

                /*Make mobile help menu primary color*/
            }

            /* This tweaks only Small devices (tablets and phones, 768px and up) */
            @media (max-width: 900px) {
                .fancybox-youtube > .feedperson, .feedperson.shade30 {
                    border-bottom: 2px solid #32C5F4;
                }

                /*Add line under main videos on homepage / Move background face down*/
                div.col-md-12.row.no-gutters > a > div.feedperson {
                    border-bottom: 2px solid #32C5F4;
                }

                /*Add line under CTA buttons on homepage*/
            }

            /*Add custom color to the case study block on homepage*/
            .image-holder:after {
                background-color: rgba(70, 173, 76, 0.7);

            }

            /*Add video overlay on main sizzle as a dynamic layer*/
            .home-hero {
                background: rgba(17, 113, 175, 1) !important;
            }

            @media only screen and (max-width: 1024px) {

                .home-hero:after {
                    background: rgba(17, 113, 175, 0.8);
                }

            }

        </style>
    </div>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>