<?php
$page_title       = 'Contact Us in Abbotsford, BC';
$doc_title        = 'Contact Us';
$meta_description = 'Your experience should be seamless from the very start. If you need to contact our Abbotsford, BC, office, we provide all our office information online.';
$og_type          = 'article';

$other_html = "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
$secretKey = "6Lepj7wUAAAAAOt54x3JYkYzxoQTii0qzEVwVkBb";

?>
<?php
session_start();
if ( isset( $_POST ) and ! empty( $_POST ) ) {
	$captcha = '';

	// EDIT THE 2 LINES BELOW AS REQUIRED
	$email_to      = "info@abbotsfordoralsurgery.com";
	$email_subject = "Contact Form Request";

	function died( $error ) {
		// your error code can go here
		echo "We are very sorry, but there were error(s) found with the form you submitted. ";
		echo "These errors appear below.<br /><br />";
		echo $error . "<br /><br />";
		echo "Please go back and fix these errors.<br /><br />";
		die();
	}

	//validate recaptcha
	if ( isset( $_POST['g-recaptcha-response'] ) ) {
		$captcha = $_POST['g-recaptcha-response'];
	}
	if(!$captcha){
		died( 'Please check the the captcha form.' );
	}

	// validation expected data exists
	if ( ! isset( $_POST['first-name'] ) ||
	     ! isset( $_POST['last-name'] ) ||
	     ! isset( $_POST['contact-email'] ) ||
	     ! isset( $_POST['contact-phone'] ) ) {
		died( 'We are sorry, but there appears to be a problem with the form you submitted.' );
	}

	$url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
	$response = file_get_contents($url);
	$responseKeys = json_decode($response,true);
	if(!$responseKeys["success"]) {
		died( 'Please check the the captcha form.' );
    }

	$first_name      = $_POST['first-name']; // required
	$last_name       = $_POST['last-name']; // required
	$email_from      = $_POST['contact-email']; // required
	$telephone       = $_POST['contact-phone']; // not required
	$contact_how_did = $_POST['contact-how-did']; // not required
	$contact_how_can = $_POST['contact-how-can']; // required

	$error_message = "";
	$email_message = '';

	function clean_string( $string ) {
		$bad = array( "content-type", "bcc:", "to:", "cc:", "href" );

		return str_replace( $bad, "", $string );
	}


	$email_message .= "First Name: " . clean_string( $first_name ) . "\n";
	$email_message .= "Last Name: " . clean_string( $last_name ) . "\n";
	$email_message .= "Email: " . clean_string( $email_from ) . "\n";
	$email_message .= "Telephone: " . clean_string( $telephone ) . "\n";
	$email_message .= "How did you hear about us? :" . clean_string( $contact_how_did ) . "\n";
	$email_message .= "How can we help you? :" . clean_string( $contact_how_can ) . "\n";

// create email headers
	$headers                     = 'From: ' . $email_from . "\r\n" .
	                               'Reply-To: ' . $email_from . "\r\n" .
	                               'X-Mailer: PHP/' . phpversion();
	$mail_status                 = mail( $email_to, $email_subject, $email_message, $headers );
	$_SESSION['success_message'] = 'The Request has been sent successfully';
	header( 'Location: ' . $_SERVER['REQUEST_URI'] );
	exit();
}
?>


<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
    <body class="page-template-default page page-id-99105 custom-background custom-header header-image full-width-content"
          itemscope itemtype="https://schema.org/WebPage">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
    <div class="wrap">
        <header class="entry-header"><h1 class="entry-title" itemprop="headline">Contact Us</h1>
        </header>
        <div class="breadcrumb" itemprop="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">You are
            here: <span class="breadcrumb-link-wrap" itemprop="itemListElement" itemscope
                        itemtype="https://schema.org/ListItem"><a class="breadcrumb-link" href="../index.html"
                                                                  itemprop="item"><span
                            class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                        itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
            Contact Us
        </div>
        <div class="content-sidebar-wrap">
            <main class="content" id="genesis-content">
                <article class="post-99105 page type-page status-publish entry secondary_color">
                    <div class="entry-content" itemprop="text">

                        <p>
                            <iframe style="border: 0;"
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7397.767641803216!2d-122.31575674195159!3d49.03900507166582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5484354461678c0f%3A0xd39d9619933dd651!2sAbbotsford%20Oral%20Surgery%20and%20Dental%20Implant%20Centre%3A%20Dr.%20Nayeem%20Esmail%2C%20Inc.!5e0!3m2!1sen!2sca!4v1570469357043!5m2!1sen!2sca"
                                    width="1144" height="450" frameborder="0"
                                    allowfullscreen="allowfullscreen"></iframe>
                        </p>
                        <p>We are happy to address your questions, concerns, and inquiries. Please contact
                            our
                            office through one of the following methods of communication:</p>

						<?php if ( isset( $_SESSION ) and ! empty( $_SESSION['success_message'] ) ) : ?>
                            <div class="alert alert-success" role="alert">
								<?php echo $_SESSION['success_message']; ?>
                            </div>
						<?php endif; ?>
                        <div class="contact-row">
                            <div class="contact-left">
                                <div class="contact-info">
                                    <div class="contact-info-inner">
                                        <h2>Mailing Address</h2>
                                        <p>Abbotsford Oral Surgery and Dental Implant Centre<br/>
                                            #305-2180 Gladwin Road<br/>
                                            Abbotsford, BC V2S 0H4</p>
                                        <p><strong>Phone:</strong> <a href="tel:1-604-504-7522">1-604-504-7522</a><br/>
                                            <strong>Fax:</strong> 1-604-504-7525</p>
                                        <p><strong>Hours:</strong><br/>
                                            Mon: 9:00 AM–4:00 PM<br/>
                                            Tue: 8:30 AM –4:00 PM<br/>
                                            Wed, Thu: 8:30 AM–4:30 PM<br/>
                                            Fri: 8:30 AM–3:00 PM</p>
                                        <h2>Email Us</h2>
                                        <p>Please use this for general practice inquiries only – specific patient care
                                            questions
                                            must be addressed with your doctor during a consultation appointment.<br/>
                                        </p>
                                        <p>
                                            <a href="mailto:info@abbotsfordoralsurgery.com">info@abbotsfordoralsurgery.com</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="contact-right">
                                <div class="contact-form">
                                    <h2>SCHEDULE A CONSULTATION</h2>
                                    <form method="post">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="first-name">FIRST NAME*</label>
                                                    <input type="text" required name="first-name" id="first-name"
                                                           class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="last-name">LAST NAME*</label>
                                                    <input type="text" required name="last-name" id="last-name"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="contact-phone">PHONE*</label>
                                                    <input type="tel" required name="contact-phone"
                                                           id="contact-phone"
                                                           class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="contact-email">EMAIL*</label>
                                                    <input type="email" required name="contact-email"
                                                           id="contact-email"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="contact-how-did">HOW DID YOU HEAR ABOUT US?</label>
                                                    <input type="text" name="contact-how-did" id="contact-how-did"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="contact-how-can">HOW CAN WE HELP YOU?</label>
                                                    <input type="text" name="contact-how-can" id="contact-how-can"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="g-recaptcha"
                                             data-sitekey="6Lepj7wUAAAAAC_X7P3k0jbOcgYAPNK7d-4ASjMb"></div>

                                        <br>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary">SUBMIT</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </article>

            </main>
        </div>
		<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
        <style type="text/css">
            .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
                background-color: #1171AF !important;
                color: #fff !important;
            }

            .secondary_color {
                background-color: #46AD4C;
            }

            .highlight_color {
                background-color: #32C5F4 !important;
            }

            ;
            .site-inner {
                background-color: #1171AF !important;
            }

            /*Main Homepage*/
            .gradient {
                background: #00AEEF; /* Old browsers */
                background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
                background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
                Padding: 10%;
            }

            .home input {
                color: rgba(17, 113, 175, 1) !important;
            }

            /*body.custom-background {
			  background-color: rgba(17,113,175,1) !important;
			}*/

            /*Menu*/
            .nav-primary .sub-menu a {
                background-color: #1171AF;
                border-color: #fff;
                color: #fff !important;
            }

            .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
                background-color: #1171AF;
            }

            /*Add primary color to the number CTA*/
            .callus a {
                font-weight: bold;
                color: #1171AF !important;
            }

            /*Images*/
            .background_cta {
                background-image: url();
            }

            .home-map-image {
                background-image: url();
                background-size: cover;
                background-position: Center Center !important;
            }

            /*Testimonial Page*/
            .related_videos {
                border-top: 7px solid #46AD4C;
            }

            /*Youtube Video */
            .video_thumb {
                background-size: cover;
                background-repeat: no-repeat;
                background-position: top center;
                height: 400px;
            }

            /*Change play button color on all inline video images*/
            .content #playhover,
            .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
            .main-home #playhover,
            .page-template-hero-min-landing #playhover,
            .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
            .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
                background: url(../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
            }

            /*Add line to the botton of inline video image*/
            article #playhover + img {
                border-bottom: 5px solid #46AD4C;
            }

            /*Fancy Overlay*/
            #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
                background-image: url(../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
                background-size: cover !important;
                background-position: left top !important;
            }

            /*Archive Pages*/
            .archive-description, .author-box {
                border-top: 7px solid #46AD4C;
                border-left: 0px;
                border-right: 0px;
            }

            .archive .entry-header a {
                color: #fff;
                font-size: 1em;
            }

            /*Team Page*/
            .tiled-gallery .gallery-row, .bio-image img {
                border-bottom: 3px solid #1171AF;
            }

            /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
            .team .entry-content img, .team .gallery-item img {
                border-bottom: 5px solid #1171AF;
            }

            /*Links*/
            a {
                color: #1171AF;
                text-decoration: none !important;
            }

            /*Change current menu color*/
            /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

            /*BTNS*/
            .button {
                background-color: #46AD4C !important;
            }

            /*Add colored bullet points*/
            .entry-content ul {
                list-style-type: none;
                position: relative;
                padding-left: 0;
            }

            .entry-content ul > li {
                list-style: none;
            }

            .entry-content ul > li:before {
                content: "• \00a0 \00a0 \00a0";
                color: #1171AF; /* Color of the bullet */
                position: absolute;
                left: -1em;
                margin-right: 5px;
            }

            /*Remove bullets on feedback page*/
            .page-template-page_feedback .entry-content ul > li:before {
                content: "" !important;
            }

            /* Video Player Pages
			--------------------------------------------- */
            .fancybox-wrap:before {
                background: url(../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
                content: ".";
                position: relative;
                width: 100%;
                height: 130px;
                z-index: 8040;
                display: block;
                margin-bottom: 20px;
                background-size: contain !important;
            }

            /*Homepage - Max Width HeroCard - change line under image*/
            .page-template-hero-max-landing > a > img {
                border-bottom: 4px solid #1171AF;
            }

            /*change color on landinage page background*/
            .animated-home {
                background-color: #1171AF;
            }

            /*Homepage - Max Width HeroCard and full page content - change line under image*/
            .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
            .page-template-hero-max-landing > .row > .col-md-12 a img {
                border-bottom: 4px solid #1171AF;
                width: 100%;
            }

            /* Archive Page
			--------------------------------------------- */
            .circle-text:after {
                background-color: #32C5F4 !important;
            }

            body .entry-content .gfield_label {
                font-size: 30px !important;
                color: #1171AF;
            }

            /*Add BG image to full screen pages.  Going to remove this for now*/
            /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
				background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
				background-size: cover;
				background-position: left top !important;
			}*/

            .full-width-content.custom-background {
                background-image: url("../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
                background-attachment: fixed;
            }

            body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
                border: 1px solid #1171AF;
                background-color: #FFF8BD;
            }

            .gform_wrapper .percentbar_blue {
                background-image: url('../wp-content/themes/nuvo-express/images/opas_15.png');
                background-repeat: repeat-x;
                background-color: #1171AF;
                color: #FFF;
            }

            /* Make some mobile Tweaks
			--------------------------------------------- */
            @media only screen and (max-width: 1023px) {
                .mobile-cta {
                    color: #1171AF !important;
                }

                /*Make mobile help menu primary color*/
            }

            /* This tweaks only Small devices (tablets and phones, 768px and up) */
            @media (max-width: 900px) {
                .fancybox-youtube > .feedperson, .feedperson.shade30 {
                    border-bottom: 2px solid #32C5F4;
                }

                /*Add line under main videos on homepage / Move background face down*/
                div.col-md-12.row.no-gutters > a > div.feedperson {
                    border-bottom: 2px solid #32C5F4;
                }

                /*Add line under CTA buttons on homepage*/
            }

            /*Add custom color to the case study block on homepage*/
            .image-holder:after {
                background-color: rgba(70, 173, 76, 0.7);

            }

            /*Add video overlay on main sizzle as a dynamic layer*/
            .home-hero {
                background: rgba(17, 113, 175, 1) !important;
            }

            @media only screen and (max-width: 1024px) {

                .home-hero:after {
                    background: rgba(17, 113, 175, 0.8);
                }

            }

        </style>
    </div>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>


<?php session_destroy(); ?>