<?php
$page_title       = 'Dental Implants &amp; Bone Graft Instructions in Abbotsford, BC';
$doc_title        = 'Dental Implants &#038; Bone Graft Instructions';
$meta_description = 'Reviewing your instructions for implants and bone grafting will help you prepare for your treatment and recovery, helping you heal quicker and more comfortably.';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="instruction-template-default single single-instruction postid-99094 custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/WebPage">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="headline">Dental Implants &#038; Bone Graft
                    Instructions</h1>
            </header>
            <div class="breadcrumb" itemprop="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">You are
                here: <span class="breadcrumb-link-wrap" itemprop="itemListElement" itemscope
                            itemtype="https://schema.org/ListItem"><a class="breadcrumb-link" href="../../index.html"
                                                                      itemprop="item"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                <span class="breadcrumb-link-wrap" itemprop="itemListElement" itemscope
                      itemtype="https://schema.org/ListItem"><a class="breadcrumb-link" href="../index.html"
                                                                itemprop="item"><span class="breadcrumb-link-text-wrap"
                                                                                      itemprop="name">Instructions</span></a><meta
                            itemprop="position" content="2"></span> <span aria-label="breadcrumb separator">/</span>
                Dental Implants &#038; Bone Graft Instructions
            </div>
            <div class="content-sidebar-wrap">
                <main class="content" id="genesis-content">
                    <div class="row no-gutters">
                        <div class=" col-md-12 main_header" itemscope itemtype="http://schema.org/ImageObject">
                            <a href="http://youtu.be/FYxTALDb0G8?rel=0&amp;autohide=1&amp;showinfo=0"
                               class="fancybox-youtube"
                               title='Dental implant & bone grafting instructions in Abbotsford, BC'>
                                <span id="playhover"></span>
                                <img width="1144" height="600"
                                     src="../../wp-content/uploads/dental-implant-instructions-1144x600.jpg"
                                     class="attachment-testimonial-img size-testimonial-img wp-post-image"
                                     alt="What are care instructions for dental implants and bone grafting in Abbotsford, BC?"/>
                            </a>
                        </div>
                    </div>
                    <div class="clearboth"></div>

                    <article
                            class="post-99094 instruction type-instruction status-publish has-post-thumbnail entry secondary_color">
                        <div class="entry-content" itemprop="text"><p>The information in this video has been compiled to
                                help our patients understand the healing process and how to avoid problems. By following
                                these instructions closely, you will ensure that your recovery is as comfortable as
                                possible.</p>
                            <h2>Post-Operative Instructions for Dental Implant &#038; Bone Grafting Surgery</h2>
                            <h3>Prescribed Medications (use as directed):</h3>
                            <p>Take Amoxicillin (500 mg) 3 times a day (breakfast, lunch, dinner) until completed. If
                                you are allergic, an alternative antibiotic will be prescribed. The antibiotic is very
                                important and must be taken to completion.</p>
                            <p>Use Chlorhexidine oral rinse (Peridex<sup>&#x2122;</sup>) twice a day for 2 weeks or
                                until instructed by the doctor.</p>
                            <p>You can take a narcotic analgesic (Tylenol<sup>®</sup> #3 or equivalent) for moderate to
                                severe pain. Take 2 tablets within 1 hour after surgery or as needed.</p>
                            <hr>
                            Do not disturb the wound. Avoid spitting or touching the wound for a few days after surgery.
                            There may be a metal stump slightly protruding through the gum tissue.</p>
                            <p>Bleeding after implant surgery is normal and may continue for several hours. If bleeding
                                is excessive, roll a pad of sterile gauze or clean linen the size of the wound and hold
                                firmly on the wound by closing teeth or by using finger pressure for 30 minutes. If
                                bleeding does not subside, please call the office for further instructions.</p>
                            <p>Swelling is a normal occurrence after surgery. Put an ice pack or cold towel your face
                                for 20 minutes on and 20 minutes off during the first 24 hours. After 24 hours, it is
                                usually best to switch from ice to moist heat to the same areas. The earlier this is
                                started, the less swelling you will experience.</p>
                            <p>Avoid rinsing your mouth for 24 hours. (Rinsing may dislodge the blood clot and interrupt
                                the normal healing process.) If no bleeding problem has occurred within the first 24
                                hours, rinse your mouth gently with warm salt water (1 teaspoonful of salt to glass of
                                warm water) 3 times a day for 7 days, especially after meals. If you were given a
                                prescription mouth rinse (such as Peridex), use it as directed in addition to the salt
                                water.</p>
                            <p>Drink plenty of fluids. Have nothing hot to eat or drink for 24 hours; lukewarm liquids
                                are recommended. Soft foods only for 2 weeks. You may gradually return to a normal diet
                                as tolerated 2 weeks after surgery unless otherwise noted.</p>
                            <p>Brushing your teeth is important to decrease the risk of infection. Be very careful with
                                brushing around the surgical site(s) until the gum tissue is fully healed.</p>
                            <p>If a temporary denture/flipper was provided, it should be used for aesthetic purposes
                                only (i.e., absolutely no chewing, eating, or direct pressure/trauma on the provided
                                temporary denture unit). Failure to follow these instructions may jeopardize the outcome
                                of the implant(s) and may lead to certain complications, such as implant loss.</p>
                            <p>If a sinus lift surgery is done, do not blow your nose for 2 weeks, and sneeze only with
                                your mouth open (follow sinus precautions).</p>
                            <p>Your bone graft is made up of many particles. You may find some small granules in your
                                mouth for the first several days. Do not be alarmed; it’s normal to have some of them
                                come out of the graft site and into your mouth. Do not apply pressure with your tongue
                                or fingers to the grafted area, as the material is movable during the initial
                                healing.</p>
                            <p>Keep physical activities to a minimum for several days after surgery.</p>
                            <p>IT IS OUR DESIRE THAT YOUR RECOVERY BE AS SMOOTH AS POSSIBLE. FOLLOWING THESE
                                INSTRUCTIONS WILL ASSIST YOU, BUT IF YOU HAVE QUESTIONS ABOUT YOUR PROGRESS, PLEASE CALL
                                OUR OFFICE AT (604) 504-7522.</p>
                            <p>After normal working hours (evenings or weekends), our answering service will page Dr.
                                Esmail or the doctor on call.</p>
                        </div>
                    </article>
                    <div class="row cta-footer ">
                        <div class="col-md-3">
                            <a href="../../contact-us/index.html">
                                <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                                </button>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <p>
                                As a patient of our office, we want it to be as easy as possible for you to visit our
                                team for care. You can request an appointment by filling out our online form. </p>
                        </div>
                    </div>
                </main>
            </div>
	        <?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
    <style type="text/css">
        .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
            background-color: #1171AF !important;
            color: #fff !important;
        }

        .secondary_color {
            background-color: #46AD4C;
        }

        .highlight_color {
            background-color: #32C5F4 !important;
        }

        ;
        .site-inner {
            background-color: #1171AF !important;
        }

        /*Main Homepage*/
        .gradient {
            background: #00AEEF; /* Old browsers */
            background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
            background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
            Padding: 10%;
        }

        .home input {
            color: rgba(17, 113, 175, 1) !important;
        }

        /*body.custom-background {
		  background-color: rgba(17,113,175,1) !important;
		}*/

        /*Menu*/
        .nav-primary .sub-menu a {
            background-color: #1171AF;
            border-color: #fff;
            color: #fff !important;
        }

        .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
            background-color: #1171AF;
        }

        /*Add primary color to the number CTA*/
        .callus a {
            font-weight: bold;
            color: #1171AF !important;
        }

        /*Images*/
        .background_cta {
            background-image: url();
        }

        .home-map-image {
            background-image: url();
            background-size: cover;
            background-position: Center Center !important;
        }

        /*Testimonial Page*/
        .related_videos {
            border-top: 7px solid #46AD4C;
        }

        /*Youtube Video */
        .video_thumb {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: top center;
            height: 400px;
        }

        /*Change play button color on all inline video images*/
        .content #playhover,
        .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
        .main-home #playhover,
        .page-template-hero-min-landing #playhover,
        .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
        .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
            background: url(../../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
        }

        /*Add line to the botton of inline video image*/
        article #playhover + img {
            border-bottom: 5px solid #46AD4C;
        }


        /*Fancy Overlay*/
        #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
            background-image: url(../../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
            background-size: cover !important;
            background-position: left top !important;
        }

        /*Archive Pages*/
        .archive-description, .author-box {
            border-top: 7px solid #46AD4C;
            border-left: 0px;
            border-right: 0px;
        }

        .archive .entry-header a {
            color: #fff;
            font-size: 1em;
        }


        /*Team Page*/
        .tiled-gallery .gallery-row, .bio-image img {
            border-bottom: 3px solid #1171AF;
        }

        /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
        .team .entry-content img, .team .gallery-item img {
            border-bottom: 5px solid #1171AF;
        }

        /*Links*/
        a {
            color: #1171AF;
            text-decoration: none !important;
        }

        /*Change current menu color*/
        /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

        /*BTNS*/
        .button {
            background-color: #46AD4C !important;
        }

        /*Add colored bullet points*/
        .entry-content ul {
            list-style-type: none;
            position: relative;
            padding-left: 0;
        }

        .entry-content ul > li {
            list-style: none;
        }

        .entry-content ul > li:before {
            content: "• \00a0 \00a0 \00a0";
            color: #1171AF; /* Color of the bullet */
            position: absolute;
            left: -1em;
            margin-right: 5px;
        }

        /*Remove bullets on feedback page*/
        .page-template-page_feedback .entry-content ul > li:before {
            content: "" !important;
        }

        /* Video Player Pages
		--------------------------------------------- */
        .fancybox-wrap:before {
            background: url(../../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
            content: ".";
            position: relative;
            width: 100%;
            height: 130px;
            z-index: 8040;
            display: block;
            margin-bottom: 20px;
            background-size: contain !important;
        }

        /*Homepage - Max Width HeroCard - change line under image*/
        .page-template-hero-max-landing > a > img {
            border-bottom: 4px solid #1171AF;
        }

        /*change color on landinage page background*/
        .animated-home {
            background-color: #1171AF;
        }

        /*Homepage - Max Width HeroCard and full page content - change line under image*/
        .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
        .page-template-hero-max-landing > .row > .col-md-12 a img {
            border-bottom: 4px solid #1171AF;
            width: 100%;
        }


        /* Archive Page
		--------------------------------------------- */
        .circle-text:after {
            background-color: #32C5F4 !important;
        }

        body .entry-content .gfield_label {
            font-size: 30px !important;
            color: #1171AF;
        }

        /*Add BG image to full screen pages.  Going to remove this for now*/
        /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
			background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
			background-size: cover;
			background-position: left top !important;
		}*/

        .full-width-content.custom-background {
            background-image: url("../../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
            background-attachment: fixed;
        }


        body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
            border: 1px solid #1171AF;
            background-color: #FFF8BD;
        }


        .gform_wrapper .percentbar_blue {
            background-image: url('../../wp-content/themes/nuvo-express/images/opas_15.png');
            background-repeat: repeat-x;
            background-color: #1171AF;
            color: #FFF;
        }




        /* Make some mobile Tweaks
		--------------------------------------------- */
        @media only screen and (max-width: 1023px) {
            .mobile-cta {
                color: #1171AF !important;
            }

            /*Make mobile help menu primary color*/
        }

        /* This tweaks only Small devices (tablets and phones, 768px and up) */
        @media (max-width: 900px) {
            .fancybox-youtube > .feedperson, .feedperson.shade30 {
                border-bottom: 2px solid #32C5F4;
            }

            /*Add line under main videos on homepage / Move background face down*/
            div.col-md-12.row.no-gutters > a > div.feedperson {
                border-bottom: 2px solid #32C5F4;
            }

            /*Add line under CTA buttons on homepage*/
        }

        /*Add custom color to the case study block on homepage*/
        .image-holder:after {
            background-color: rgba(70, 173, 76, 0.7);

        }

        /*Add video overlay on main sizzle as a dynamic layer*/
        .home-hero {
            background: rgba(17, 113, 175, 1) !important;
        }

        @media only screen and (max-width: 1024px) {

            .home-hero:after {
                background: rgba(17, 113, 175, 0.8);
            }

        }

    </style>
</div>
	    <?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
