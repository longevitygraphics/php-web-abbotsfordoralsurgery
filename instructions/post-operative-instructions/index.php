<!DOCTYPE html>
<?php
$page_title       = 'Post-Operative Instructions in Abbotsford, BC';
$doc_title        = 'Post-Operative Instructions';
$meta_description = 'Reviewing your post-operative instructions for oral surgery will help you prepare for your treatment and recovery, helping you heal quicker and more comfortably.';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="instruction-template-default single single-instruction postid-99095 custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/WebPage">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="headline">Post-Operative Instructions</h1>
            </header>
            <div class="breadcrumb" itemprop="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">You are
                here: <span class="breadcrumb-link-wrap" itemprop="itemListElement" itemscope
                            itemtype="https://schema.org/ListItem"><a class="breadcrumb-link" href="../../index.html"
                                                                      itemprop="item"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                <span class="breadcrumb-link-wrap" itemprop="itemListElement" itemscope
                      itemtype="https://schema.org/ListItem"><a class="breadcrumb-link" href="../index.html"
                                                                itemprop="item"><span class="breadcrumb-link-text-wrap"
                                                                                      itemprop="name">Instructions</span></a><meta
                            itemprop="position" content="2"></span> <span aria-label="breadcrumb separator">/</span>
                Post-Operative Instructions
            </div>
            <div class="content-sidebar-wrap">
                <main class="content" id="genesis-content">
                    <div class="row no-gutters">
                        <div class=" col-md-12 main_header" itemscope itemtype="http://schema.org/ImageObject">
                            <a href="http://youtu.be/U8mlogCj6Fs?rel=0&amp;autohide=1&amp;showinfo=0"
                               class="fancybox-youtube" title='Post-operative instructions in Abbotsford, BC'>
                                <span id="playhover"></span>
                                <img width="1144" height="600"
                                     src="../../wp-content/uploads/post-op-instructions-1144x600.jpg"
                                     class="attachment-testimonial-img size-testimonial-img wp-post-image"
                                     alt="What are instructions for post-operative care in Abbotsford, BC?"/> </a>
                        </div>
                    </div>
                    <div class="clearboth"></div>

                    <article
                            class="post-99095 instruction type-instruction status-publish has-post-thumbnail entry secondary_color">
                        <div class="entry-content" itemprop="text"><p>The information in this video has been compiled to
                                help our patients understand the healing process and how to avoid problems. By following
                                these instructions closely, you will ensure that your recovery is as comfortable as
                                possible.</p>
                            <p>Following oral surgery, it will be important to follow these care instructions exactly as
                                we present them. They are intended to guide you to a comfortable recovery and assist
                                your healing.</p>
                            <h3>DISCOMFORT</h3>
                            <p>A certain amount of discomfort should be expected and varies with the amount of surgery
                                that was performed. Your doctor will instruct you on what medications to take the day of
                                your surgery. If your pain is not controlled by the ibuprofen (also known as
                                Advil<sup>®</sup> or Motrin<sup>®</sup>) alone, take your prescribed medication in
                                addition to keep you comfortable. It is best taken with fluid or food in your stomach;
                                this will help prevent nausea. If a rash develops, stop taking the medication and call
                                our office.</p>
                            <h3>BLEEDING</h3>
                            <p>When you leave the office, you will be biting on gauze pads that have been placed over
                                the surgical area. Maintain firm biting pressure on the gauze for 30–40 minutes after
                                leaving. You may replace the gauze if active bleeding occurs upon its removal. Most of
                                your bleeding will slow within 3–4 hours, but a small amount of oozing is common for up
                                to 24 hours after surgery.</p>
                            <h3>SWELLING AND BRUISING</h3>
                            <p>These are part of normal healing. Swelling typically peaks by the third day and then
                                starts to resolve gradually; it can be reduced by the use of an ice pack. Apply the ice
                                pack on your face over the area of surgery in 15-minute intervals for 24 hours after
                                surgery. After 24 hours, it is usually best to switch from ice to moist heat to the same
                                areas. The earlier this is started, the less swelling you will experience. Also, keep
                                your head elevated (on several pillows) for 24–48 hours after surgery. Some bruising on
                                your face is normal and may gravitate to areas below the site of your surgery.</p>
                            <h3>DIET</h3>
                            <p>Good nutrition and adequate fluid intake are important after surgery. Immediately
                                following surgery, a liquid or soft diet is suggested (for example, soup, milkshake,
                                eggs, mashed potatoes, etc.). Avoid any HOT foods or liquids during the first 24 hours.
                                You may gradually return to a normal diet as tolerated 3–5 days after surgery unless
                                otherwise directed. To help prevent dry socket, do not use a straw for the first 5 days
                                after surgery.</p>
                            <h3>ORAL HYGIENE</h3>
                            <p>Good oral hygiene is very important to ensure uneventful healing. You may begin brushing
                                your teeth using a soft toothbrush the day after surgery. It is important to brush all
                                of your teeth, even if the teeth and gums are sensitive. Bacterial plaque and food
                                accumulation near the extraction site will delay healing.</p>
                            <p>Do NOT rinse your mouth or spit on the day of surgery. Begin rinsing the day after
                                surgery; rinse with warm salt water 3–4 times a day for 7 days (1 teaspoonful of salt to
                                a glass of warm water). Do NOT use mouthwash or hydrogen peroxide, as this will delay
                                healing. Begin use of the syringe provided (on lower extraction sites only) 5 days after
                                surgery.</p>
                            <p>Do not smoke for at least 1 week after surgery (smoking will cause significant pain) or
                                drink alcohol for at least 48 hours after surgery (or while taking pain medication);
                                smoking and alcohol both delay healing and may lead to complications such as dry
                                socket.</p>
                            <h3>STITCHES</h3>
                            <p>If stitches (sutures) have been placed in your mouth, they will dissolve and fall out on
                                their own in 7–10 days. If they fall out sooner, do not be alarmed, as this is
                                normal.</p>
                            <h3>NAUSEA</h3>
                            <p>This is usually due to not eating or taking too much pain medication on an empty stomach.
                                It may also be caused by not keeping firm pressure on the gauze and swallowing some
                                blood after surgery. Drinking small amounts of clear carbonated beverages every hour for
                                5–6 hours usually helps. Gravol&#x2122; may also be taken without a prescription. If
                                nausea continues, please call our office.</p>
                            <h3>ACTIVITY</h3>
                            <p>Unless told otherwise, avoid vigorous physical activity for 5 days following your
                                surgery. Physical activity increases your blood pressure, which will cause an increase
                                in your swelling, pain, and bleeding.</p>
                            <p>IT IS OUR DESIRE THAT YOUR RECOVERY BE AS SMOOTH AS POSSIBLE. FOLLOWING THESE
                                INSTRUCTIONS WILL ASSIST YOU, BUT IF YOU HAVE QUESTIONS ABOUT YOUR PROGRESS, PLEASE CALL
                                OUR OFFICE AT (604) 504-7522.</p>
                            <p>After normal working hours (evenings or weekends), our answering service will page Dr.
                                Esmail or the doctor on call.</p>
                        </div>
                    </article>
                    <div class="row cta-footer ">
                        <div class="col-md-3">
                            <a href="../../contact-us/index.html">
                                <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                                </button>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <p>
                                As a patient of our office, we want it to be as easy as possible for you to visit our
                                team for care. You can request an appointment by filling out our online form. </p>
                        </div>
                    </div>
                </main>
            </div>
	        <?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
    <style type="text/css">
        .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
            background-color: #1171AF !important;
            color: #fff !important;
        }

        .secondary_color {
            background-color: #46AD4C;
        }

        .highlight_color {
            background-color: #32C5F4 !important;
        }

        ;
        .site-inner {
            background-color: #1171AF !important;
        }

        /*Main Homepage*/
        .gradient {
            background: #00AEEF; /* Old browsers */
            background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
            background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
            Padding: 10%;
        }

        .home input {
            color: rgba(17, 113, 175, 1) !important;
        }

        /*body.custom-background {
		  background-color: rgba(17,113,175,1) !important;
		}*/

        /*Menu*/
        .nav-primary .sub-menu a {
            background-color: #1171AF;
            border-color: #fff;
            color: #fff !important;
        }

        .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
            background-color: #1171AF;
        }

        /*Add primary color to the number CTA*/
        .callus a {
            font-weight: bold;
            color: #1171AF !important;
        }

        /*Images*/
        .background_cta {
            background-image: url();
        }

        .home-map-image {
            background-image: url();
            background-size: cover;
            background-position: Center Center !important;
        }

        /*Testimonial Page*/
        .related_videos {
            border-top: 7px solid #46AD4C;
        }

        /*Youtube Video */
        .video_thumb {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: top center;
            height: 400px;
        }

        /*Change play button color on all inline video images*/
        .content #playhover,
        .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
        .main-home #playhover,
        .page-template-hero-min-landing #playhover,
        .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
        .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
            background: url(../../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
        }

        /*Add line to the botton of inline video image*/
        article #playhover + img {
            border-bottom: 5px solid #46AD4C;
        }


        /*Fancy Overlay*/
        #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
            background-image: url(../../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
            background-size: cover !important;
            background-position: left top !important;
        }

        /*Archive Pages*/
        .archive-description, .author-box {
            border-top: 7px solid #46AD4C;
            border-left: 0px;
            border-right: 0px;
        }

        .archive .entry-header a {
            color: #fff;
            font-size: 1em;
        }


        /*Team Page*/
        .tiled-gallery .gallery-row, .bio-image img {
            border-bottom: 3px solid #1171AF;
        }

        /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
        .team .entry-content img, .team .gallery-item img {
            border-bottom: 5px solid #1171AF;
        }

        /*Links*/
        a {
            color: #1171AF;
            text-decoration: none !important;
        }

        /*Change current menu color*/
        /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

        /*BTNS*/
        .button {
            background-color: #46AD4C !important;
        }

        /*Add colored bullet points*/
        .entry-content ul {
            list-style-type: none;
            position: relative;
            padding-left: 0;
        }

        .entry-content ul > li {
            list-style: none;
        }

        .entry-content ul > li:before {
            content: "• \00a0 \00a0 \00a0";
            color: #1171AF; /* Color of the bullet */
            position: absolute;
            left: -1em;
            margin-right: 5px;
        }

        /*Remove bullets on feedback page*/
        .page-template-page_feedback .entry-content ul > li:before {
            content: "" !important;
        }

        /* Video Player Pages
		--------------------------------------------- */
        .fancybox-wrap:before {
            background: url(../../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
            content: ".";
            position: relative;
            width: 100%;
            height: 130px;
            z-index: 8040;
            display: block;
            margin-bottom: 20px;
            background-size: contain !important;
        }

        /*Homepage - Max Width HeroCard - change line under image*/
        .page-template-hero-max-landing > a > img {
            border-bottom: 4px solid #1171AF;
        }

        /*change color on landinage page background*/
        .animated-home {
            background-color: #1171AF;
        }

        /*Homepage - Max Width HeroCard and full page content - change line under image*/
        .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
        .page-template-hero-max-landing > .row > .col-md-12 a img {
            border-bottom: 4px solid #1171AF;
            width: 100%;
        }


        /* Archive Page
		--------------------------------------------- */
        .circle-text:after {
            background-color: #32C5F4 !important;
        }

        body .entry-content .gfield_label {
            font-size: 30px !important;
            color: #1171AF;
        }

        /*Add BG image to full screen pages.  Going to remove this for now*/
        /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
			background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
			background-size: cover;
			background-position: left top !important;
		}*/

        .full-width-content.custom-background {
            background-image: url("../../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
            background-attachment: fixed;
        }


        body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
            border: 1px solid #1171AF;
            background-color: #FFF8BD;
        }


        .gform_wrapper .percentbar_blue {
            background-image: url('../../wp-content/themes/nuvo-express/images/opas_15.png');
            background-repeat: repeat-x;
            background-color: #1171AF;
            color: #FFF;
        }




        /* Make some mobile Tweaks
		--------------------------------------------- */
        @media only screen and (max-width: 1023px) {
            .mobile-cta {
                color: #1171AF !important;
            }

            /*Make mobile help menu primary color*/
        }

        /* This tweaks only Small devices (tablets and phones, 768px and up) */
        @media (max-width: 900px) {
            .fancybox-youtube > .feedperson, .feedperson.shade30 {
                border-bottom: 2px solid #32C5F4;
            }

            /*Add line under main videos on homepage / Move background face down*/
            div.col-md-12.row.no-gutters > a > div.feedperson {
                border-bottom: 2px solid #32C5F4;
            }

            /*Add line under CTA buttons on homepage*/
        }

        /*Add custom color to the case study block on homepage*/
        .image-holder:after {
            background-color: rgba(70, 173, 76, 0.7);

        }

        /*Add video overlay on main sizzle as a dynamic layer*/
        .home-hero {
            background: rgba(17, 113, 175, 1) !important;
        }

        @media only screen and (max-width: 1024px) {

            .home-hero:after {
                background: rgba(17, 113, 175, 0.8);
            }

        }

    </style>
</div>
	    <?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
