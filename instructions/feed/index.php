<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>Abbotsford Oral Surgery and Dental Implant Centre</title>
	<atom:link href="https://www.abbotsfordoralsurgery.com/instructions/feed/" rel="self" type="application/rss+xml" />
	<link>https://www.abbotsfordoralsurgery.com</link>
	<description></description>
	<lastBuildDate>
	Tue, 26 Mar 2019 21:53:02 +0000	</lastBuildDate>
	<language>en-US</language>
	<sy:updatePeriod>
	hourly	</sy:updatePeriod>
	<sy:updateFrequency>
	1	</sy:updateFrequency>
	<generator>https://wordpress.org/?v=5.1.1</generator>

<image>
	<url>https://www.abbotsfordoralsurgery.com/wp-content/uploads/cropped-ABOT-Favicon-32x32.png</url>
	<title>Abbotsford Oral Surgery and Dental Implant Centre</title>
	<link>https://www.abbotsfordoralsurgery.com</link>
	<width>32</width>
	<height>32</height>
</image> 
	<item>
		<title>Instructions for Intravenous Sedation</title>
		<link>https://www.abbotsfordoralsurgery.com/instructions/pre-operative-instructions/</link>
				<comments>https://www.abbotsfordoralsurgery.com/instructions/pre-operative-instructions/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/instructions/pre-operative-instructions/</guid>
				<description><![CDATA[<p>This video has been made to ensure that your experience on the day of surgery is as seamless and pleasant as possible. By following these instructions closely, you will ensure that your procedure is as safe and comfortable as possible. Instructions Before Intravenous Sedation To make your next visit to our office as comfortable and easy for you as possible,&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/instructions/pre-operative-instructions/">Instructions for Intravenous Sedation</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>This video has been made to ensure that your experience on the day of surgery is as seamless and pleasant as possible. By following these instructions closely, you will ensure that your procedure is as safe and comfortable as possible.</p>
<h2>Instructions Before Intravenous Sedation</h2>
<p>To make your next visit to our office as comfortable and easy for you as possible, we offer you this list of reminders. It is imperative that you follow the instructions listed below carefully.</p>
<p>DO NOT EAT OR DRINK ANYTHING (not even water) after midnight the night before surgery, or for at least 6 hours prior to your appointment.</p>
<p>You must bring a responsible adult with you who will be able to drive you home and remain with you after surgery. A parent or legal guardian must accompany minors.</p>
<p>Wear a short-sleeved shirt and comfortable, loose-fitting clothing. Please do not wear contact lenses.</p>
<p>You may take your regular morning medications with a SMALL sip of water.</p>
<p>No herbal/naturopathic remedies for 2 weeks prior to surgery.</p>
<p>Take prescribed medications 1 hour before your surgery with a small sip of water.</p>
<p><u>Failure to comply with these instructions could result in a delay of your surgery.</u></p>
<p>If you have any questions about these instructions, please call our office.</p>
<p>Please allow 48 hours’ notice for cancellation to avoid a late cancellation fee.</p>
<h2>Instructions After Intravenous Sedation</h2>
<p>Following intravenous sedation, it may take several hours for the full effects of the drugs to wear off. Please follow these recommendations.</p>
<p>Remain at home, resting quietly for the remainder of the day.</p>
<p>If you feel dizzy, immediately lie flat until the dizziness is gone.</p>
<p>Do not drink alcohol for 48 hours after surgery or while taking pain medication.</p>
<p>Do not operate a motor vehicle, power tools, or machinery for 24 hours after surgery.</p>
<p>Do not sign or enter any legal contracts for 24 hours.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/instructions/pre-operative-instructions/">Instructions for Intravenous Sedation</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/instructions/pre-operative-instructions/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Dental Implants &#038; Bone Graft Instructions</title>
		<link>https://www.abbotsfordoralsurgery.com/instructions/dental-implants-bone-graft-instructions/</link>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/instructions/dental-implants-bone-graft-instructions/</guid>
				<description><![CDATA[<p>The information in this video has been compiled to help our patients understand the healing process and how to avoid problems. By following these instructions closely, you will ensure that your recovery is as comfortable as possible. Post-Operative Instructions for Dental Implant &#038; Bone Grafting Surgery Prescribed Medications (use as directed): Take Amoxicillin (500 mg) 3 times a day (breakfast,&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/instructions/dental-implants-bone-graft-instructions/">Dental Implants &#038; Bone Graft Instructions</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The information in this video has been compiled to help our patients understand the healing process and how to avoid problems. By following these instructions closely, you will ensure that your recovery is as comfortable as possible.</p>
<h2>Post-Operative Instructions for Dental Implant &#038; Bone Grafting Surgery</h2>
<h3>Prescribed Medications (use as directed):</h3>
<p>Take Amoxicillin (500 mg) 3 times a day (breakfast, lunch, dinner) until completed. If you are allergic, an alternative antibiotic will be prescribed. The antibiotic is very important and must be taken to completion.</p>
<p>Use Chlorhexidine oral rinse (Peridex<sup><img src="https://s.w.org/images/core/emoji/11.2.0/72x72/2122.png" alt="™" class="wp-smiley" style="height: 1em; max-height: 1em;" /></sup>) twice a day for 2 weeks or until instructed by the doctor.</p>
<p>You can take a narcotic analgesic (Tylenol<sup>®</sup> #3 or equivalent) for moderate to severe pain. Take 2 tablets within 1 hour after surgery or as needed.</p>
<hr>
Do not disturb the wound. Avoid spitting or touching the wound for a few days after surgery. There may be a metal stump slightly protruding through the gum tissue.</p>
<p>Bleeding after implant surgery is normal and may continue for several hours. If bleeding is excessive, roll a pad of sterile gauze or clean linen the size of the wound and hold firmly on the wound by closing teeth or by using finger pressure for 30 minutes. If bleeding does not subside, please call the office for further instructions.</p>
<p>Swelling is a normal occurrence after surgery. Put an ice pack or cold towel your face for 20 minutes on and 20 minutes off during the first 24 hours. After 24 hours, it is usually best to switch from ice to moist heat to the same areas. The earlier this is started, the less swelling you will experience.</p>
<p>Avoid rinsing your mouth for 24 hours. (Rinsing may dislodge the blood clot and interrupt the normal healing process.) If no bleeding problem has occurred within the first 24 hours, rinse your mouth gently with warm salt water (1 teaspoonful of salt to glass of warm water) 3 times a day for 7 days, especially after meals. If you were given a prescription mouth rinse (such as Peridex), use it as directed in addition to the salt water.</p>
<p>Drink plenty of fluids. Have nothing hot to eat or drink for 24 hours; lukewarm liquids are recommended. Soft foods only for 2 weeks. You may gradually return to a normal diet as tolerated 2 weeks after surgery unless otherwise noted.</p>
<p>Brushing your teeth is important to decrease the risk of infection. Be very careful with brushing around the surgical site(s) until the gum tissue is fully healed.</p>
<p>If a temporary denture/flipper was provided, it should be used for aesthetic purposes only (i.e., absolutely no chewing, eating, or direct pressure/trauma on the provided temporary denture unit). Failure to follow these instructions may jeopardize the outcome of the implant(s) and may lead to certain complications, such as implant loss.</p>
<p>If a sinus lift surgery is done, do not blow your nose for 2 weeks, and sneeze only with your mouth open (follow sinus precautions).</p>
<p>Your bone graft is made up of many particles. You may find some small granules in your mouth for the first several days. Do not be alarmed; it’s normal to have some of them come out of the graft site and into your mouth. Do not apply pressure with your tongue or fingers to the grafted area, as the material is movable during the initial healing.</p>
<p>Keep physical activities to a minimum for several days after surgery.</p>
<p>IT IS OUR DESIRE THAT YOUR RECOVERY BE AS SMOOTH AS POSSIBLE. FOLLOWING THESE INSTRUCTIONS WILL ASSIST YOU, BUT IF YOU HAVE QUESTIONS ABOUT YOUR PROGRESS, PLEASE CALL OUR OFFICE AT (604) 504-7522.</p>
<p>After normal working hours (evenings or weekends), our answering service will page Dr. Esmail or the doctor on call.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/instructions/dental-implants-bone-graft-instructions/">Dental Implants &#038; Bone Graft Instructions</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
										</item>
		<item>
		<title>Post-Operative Instructions</title>
		<link>https://www.abbotsfordoralsurgery.com/instructions/post-operative-instructions/</link>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/instructions/post-operative-instructions/</guid>
				<description><![CDATA[<p>The information in this video has been compiled to help our patients understand the healing process and how to avoid problems. By following these instructions closely, you will ensure that your recovery is as comfortable as possible. Following oral surgery, it will be important to follow these care instructions exactly as we present them. They are intended to guide you&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/instructions/post-operative-instructions/">Post-Operative Instructions</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The information in this video has been compiled to help our patients understand the healing process and how to avoid problems. By following these instructions closely, you will ensure that your recovery is as comfortable as possible.</p>
<p>Following oral surgery, it will be important to follow these care instructions exactly as we present them. They are intended to guide you to a comfortable recovery and assist your healing.</p>
<h3>DISCOMFORT</h3>
<p>A certain amount of discomfort should be expected and varies with the amount of surgery that was performed. Your doctor will instruct you on what medications to take the day of your surgery. If your pain is not controlled by the ibuprofen (also known as Advil<sup>®</sup> or Motrin<sup>®</sup>) alone, take your prescribed medication in addition to keep you comfortable. It is best taken with fluid or food in your stomach; this will help prevent nausea. If a rash develops, stop taking the medication and call our office.</p>
<h3>BLEEDING</h3>
<p>When you leave the office, you will be biting on gauze pads that have been placed over the surgical area. Maintain firm biting pressure on the gauze for 30–40 minutes after leaving. You may replace the gauze if active bleeding occurs upon its removal. Most of your bleeding will slow within 3–4 hours, but a small amount of oozing is common for up to 24 hours after surgery.</p>
<h3>SWELLING AND BRUISING</h3>
<p>These are part of normal healing. Swelling typically peaks by the third day and then starts to resolve gradually; it can be reduced by the use of an ice pack. Apply the ice pack on your face over the area of surgery in 15-minute intervals for 24 hours after surgery. After 24 hours, it is usually best to switch from ice to moist heat to the same areas. The earlier this is started, the less swelling you will experience. Also, keep your head elevated (on several pillows) for 24–48 hours after surgery. Some bruising on your face is normal and may gravitate to areas below the site of your surgery.</p>
<h3>DIET</h3>
<p>Good nutrition and adequate fluid intake are important after surgery. Immediately following surgery, a liquid or soft diet is suggested (for example, soup, milkshake, eggs, mashed potatoes, etc.). Avoid any HOT foods or liquids during the first 24 hours. You may gradually return to a normal diet as tolerated 3–5 days after surgery unless otherwise directed. To help prevent dry socket, do not use a straw for the first 5 days after surgery.</p>
<h3>ORAL HYGIENE</h3>
<p>Good oral hygiene is very important to ensure uneventful healing. You may begin brushing your teeth using a soft toothbrush the day after surgery. It is important to brush all of your teeth, even if the teeth and gums are sensitive. Bacterial plaque and food accumulation near the extraction site will delay healing.</p>
<p>Do NOT rinse your mouth or spit on the day of surgery. Begin rinsing the day after surgery; rinse with warm salt water 3–4 times a day for 7 days (1 teaspoonful of salt to a glass of warm water). Do NOT use mouthwash or hydrogen peroxide, as this will delay healing. Begin use of the syringe provided (on lower extraction sites only) 5 days after surgery.</p>
<p>Do not smoke for at least 1 week after surgery (smoking will cause significant pain) or drink alcohol for at least 48 hours after surgery (or while taking pain medication); smoking and alcohol both delay healing and may lead to complications such as dry socket.</p>
<h3>STITCHES</h3>
<p>If stitches (sutures) have been placed in your mouth, they will dissolve and fall out on their own in 7–10 days. If they fall out sooner, do not be alarmed, as this is normal.</p>
<h3>NAUSEA</h3>
<p>This is usually due to not eating or taking too much pain medication on an empty stomach. It may also be caused by not keeping firm pressure on the gauze and swallowing some blood after surgery. Drinking small amounts of clear carbonated beverages every hour for 5–6 hours usually helps. Gravol<img src="https://s.w.org/images/core/emoji/11.2.0/72x72/2122.png" alt="™" class="wp-smiley" style="height: 1em; max-height: 1em;" /> may also be taken without a prescription. If nausea continues, please call our office.</p>
<h3>ACTIVITY</h3>
<p>Unless told otherwise, avoid vigorous physical activity for 5 days following your surgery. Physical activity increases your blood pressure, which will cause an increase in your swelling, pain, and bleeding.</p>
<p>IT IS OUR DESIRE THAT YOUR RECOVERY BE AS SMOOTH AS POSSIBLE. FOLLOWING THESE INSTRUCTIONS WILL ASSIST YOU, BUT IF YOU HAVE QUESTIONS ABOUT YOUR PROGRESS, PLEASE CALL OUR OFFICE AT (604) 504-7522.</p>
<p>After normal working hours (evenings or weekends), our answering service will page Dr. Esmail or the doctor on call.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/instructions/post-operative-instructions/">Post-Operative Instructions</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
										</item>
		<item>
		<title>Jaw Surgery Post-Operative Instructions</title>
		<link>https://www.abbotsfordoralsurgery.com/instructions/jaw-surgery-post-operative-instructions/</link>
				<comments>https://www.abbotsfordoralsurgery.com/instructions/jaw-surgery-post-operative-instructions/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/instructions/jaw-surgery-post-operative-instructions/</guid>
				<description><![CDATA[<p>The information in this video has been compiled to help our patients understand the healing process and how to avoid problems. By following these instructions closely, you will ensure that your recovery is as comfortable as possible. Oral Care Oral hygiene is very important after jaw surgery. Keeping your mouth and teeth clean will help you heal better and reduce&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/instructions/jaw-surgery-post-operative-instructions/">Jaw Surgery Post-Operative Instructions</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The information in this video has been compiled to help our patients understand the healing process and how to avoid problems. By following these instructions closely, you will ensure that your recovery is as comfortable as possible.</p>
<h3>Oral Care</h3>
<p>Oral hygiene is very important after jaw surgery. Keeping your mouth and teeth clean will help you heal better and reduce the chances of developing infections or cavities.</p>
<p>Brush your teeth with a soft-bristled toothbrush (child size works well). Use a circular motion and angle the brush at 45 degrees toward the gum line.</p>
<p>Rinse your mouth with a solution of warm salt water (1 teaspoon salt in one 8-ounce glass of warm water). Oral rinsing should be done after every meal.</p>
<p>Avoid the use of mouthwashes except if prescribed. Use Blistex<sup>®</sup> or Vaseline<sup>®</sup> to avoid dry, cracked lips.</p>
<p>Use orthodontic wax on the end of wires that are protruding or irritating the mouth (wax should be removed before and after eating and brushing teeth). This can be purchased at most pharmacies.</p>
<p>Keep your skin clean and apply topical antibiotics to any wounds (Neosporin<sup>®</sup>, bacitracin) outside of the mouth.</p>
<p>Do not smoke! Smoking dries and irritates oral mucosa. Avoid alcohol, which can cause nausea and dehydration.</p>
<h3>Diet</h3>
<p>You can reduce the risk of dehydration and keep secretions thin by drinking at least 10 8-ounce glasses of fluids a day.</p>
<p>Drink a minimum of 8 glasses of water a day.</p>
<p>Eat a high-protein, high-calorie blended soft or liquid diet. Calorie and protein supplements can be used. Drink Ensure<sup>®</sup> and Boost<sup>®</sup>. Add protein powders. Protein powder can be purchased at GNC and most supermarkets. Increased protein intake after surgery is vital for maintaining the body&#8217;s protective systems and for building healthy new tissue.</p>
<p>Eat soft foods (mashed potatoes, blended soups, soft vegetables, pasta, etc.)</p>
<p>Eat a lot of snacks (juices, smoothies, puddings, yogurt, etc.). Using a straw is okay.</p>
<h3>Activity</h3>
<p>Do not participate in sports or strenuous activities or return to work for 2 weeks or until permitted by your surgeon.</p>
<p>Pain medications may cause alterations of visual perception and impair judgment.</p>
<p>A side effect of pain medication is constipation. This can be alleviated by getting out of bed, adequate fluid intake, moving around, and taking stool softeners if needed.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/instructions/jaw-surgery-post-operative-instructions/">Jaw Surgery Post-Operative Instructions</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/instructions/jaw-surgery-post-operative-instructions/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Sinus Precautions Instructions</title>
		<link>https://www.abbotsfordoralsurgery.com/instructions/sinus-precautions-instructions/</link>
				<comments>https://www.abbotsfordoralsurgery.com/instructions/sinus-precautions-instructions/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/instructions/sinus-precautions-instructions/</guid>
				<description><![CDATA[<p>The information in this video has been compiled to help our patients understand the healing process and how to avoid problems. By following these instructions closely, you will ensure that your recovery is as comfortable as possible. There are 2 common scenarios where patients are asked to follow these instructions: The first and most common situation involves the removal of&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/instructions/sinus-precautions-instructions/">Sinus Precautions Instructions</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The information in this video has been compiled to help our patients understand the healing process and how to avoid problems. By following these instructions closely, you will ensure that your recovery is as comfortable as possible.</p>
<h3>There are 2 common scenarios where patients are asked to follow these instructions:</h3>
<p>The first and most common situation involves the removal of upper back teeth. (The roots of your upper teeth have grown through bone and into your sinus. Following their removal, an opening into the sinus was visible.)</p>
<p>The second involves patients undergoing a sinus lift (graft) procedure to allow for the placement of dental implants.</p>
<p>The GENERAL POST-OPERATIVE INSTRUCTIONS still apply, but we would like to emphasize a few important points about your particular procedure and remind you of some instructions.  By adhering to the following SINUS INSTRUCTIONS, you may ensure that your recovery is uneventful and comfortable:</p>
<ol>
<li>Avoid blowing your nose for at least 7 days, although some discharge (perhaps containing blood for the first few days) may be expected.</li>
<li>If you must sneeze, do so with your mouth open to avoid any unnecessary pressure on the sinus area.</li>
<li>Do not smoke or use smokeless tobacco for 2 weeks. Smoking greatly inhibits healing, especially in the sinus graft.</li>
<li>Do not use a straw for 2 weeks.</li>
<li>Do not lift or pull up your lip to look at stitches, as this may create damage and tear the stitches.</li>
<li>Refrain from any vigorous mouth rinsing.</li>
<li>Please take prescribed antibiotics as directed and until finished. They are essential to prevent infection.</li>
<li>If you feel congested, you may need to use antihistamines or nasal decongestants, such as over-the-counter tablets or sprays. Most of the time, patients do fine without any nasal sprays, antihistamines, or decongestants.</li>
</ol>
<p>STRICT ADHERENCE TO THESE INSTRUCTIONS MAY PREVENT THE NECESSITY OF ANOTHER SURGICAL PROCEDURE AT A LATER DATE. IF YOU HAVE ANY PROBLEMS OR QUESTIONS, PLEASE CALL OUR OFFICE AT (604) 504-7522.</p>
<p>After normal working hours, our answering service will page Dr. Esmail or the doctor on call.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/instructions/sinus-precautions-instructions/">Sinus Precautions Instructions</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/instructions/sinus-precautions-instructions/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
	</channel>
</rss>
