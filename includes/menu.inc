<?php
$site_menu = array(
	array
	(
		'title' => 'Oral Surgery Home',
		'url'   => 'index.html'
	),

	array
	(
		'title' => 'Services',
		'url'   => 'procedure/index.html',
		'items' => array(
			array
			(
				'title' => 'Dental Implants',
				'url'   => 'procedure/dental-implants-abbotsford-bc/index.html',
				'items' => array(
					array
					(
						'title' => 'Tooth Replacement Options',
						'url'   => 'procedure/tooth-replacement-options/index.html'
					),

					array
					(
						'title' => 'Dental Implants Overview',
						'url'   => 'procedure/dental-implants-abbotsford-bc/index.html'
					),

					array
					(
						'title' => 'Dental Implant Cost',
						'url'   => 'procedure/dental-implants-cost/index.html'
					),

					array
					(
						'title' => 'Dental Implant FAQ',
						'url'   => 'procedure/dental-implants-faq/index.html'
					),
				)
			),


			array
			(
				'title' => 'Wisdom Teeth Removal',
				'url'   => 'procedure/wisdom-teeth-removal-abbotsford-bc/index.html',
				'items' => array(
					array
					(
						'title' => 'Wisdom Teeth Removal Overview',
						'url'   => 'procedure/wisdom-teeth-removal-abbotsford-bc/index.html'
					),

					array
					(
						'title' => 'Cost of Wisdom Teeth Removal',
						'url'   => 'procedure/wisdom-teeth-removal-cost/index.html'
					),

					array
					(
						'title' => 'Wisdom Teeth Removal FAQ',
						'url'   => 'procedure/wisdom-teeth-removal-faq/index.html'
					),

					array
					(
						'title' => 'Impacted Wisdom Teeth',
						'url'   => 'procedure/impacted-wisdom-teeth/index.html'
					),
				)
			),


			array
			(
				'title' => 'Full-Arch Restoration',
				'url'   => 'procedure/full-arch-restoration/index.html'
			),

			array
			(
				'title' => 'Bone Grafting',
				'url'   => 'procedure/bone-grafting/index.html'
			),

			array
			(
				'title' => 'Tooth Extraction',
				'url'   => 'procedure/general-tooth-extraction/index.html'
			),

			array
			(
				'title' => 'Impacted Tooth Exposure',
				'url'   => 'procedure/impacted-tooth-exposure/index.html'
			),

			array
			(
				'title' => 'Oral Pathology',
				'url'   => 'procedure/oral-pathology/index.html'
			),

			array
			(
				'title' => 'Orthognathic Surgery',
				'url'   => 'procedure/orthognathic-surgery/index.html'
			),

			array
			(
				'title' => 'Pre-Prosthetic Surgery',
				'url'   => 'procedure/pre-prosthetic-surgery/index.html'
			),

			array
			(
				'title' => 'Facial Trauma',
				'url'   => 'procedure/facial-trauma/index.html'
			),

			array
			(
				'title' => 'Surgical Treatment of Sleep Apnea',
				'url'   => 'procedure/surgical-treatment-sleep-apnea/index.html'
			),

			array
			(
				'title' => 'Distraction Osteogenesis',
				'url'   => 'procedure/distraction-osteogenesis/index.html'
			),

			array
			(
				'title' => 'TMJ Treatment',
				'url'   => 'procedure/tmj-treatment/index.html'
			),

			array
			(
				'title' => 'Platelet-Rich Fibrin',
				'url'   => 'procedure/platelet-rich-fibrin/index.html'
			),

			array
			(
				'title' => 'Surgically Assisted Rapid Palatal Expansion',
				'url'   => 'procedure/surgical-palate-expansion/index.html'
			),

			array
			(
				'title' => 'Botox®',
				'url'   => 'procedure/botox/index.html'
			),
		)
	),


	array
	(
		'title' => 'About Us',
		'url'   => 'about-our-oral-surgery-practice/index.html',
		'items' => array(
			array
			(
				'title' => 'About Our Practice',
				'url'   => 'about-our-oral-surgery-practice/index.html'
			),

			array
			(
				'title' => 'Nayeem Esmail, BSc, DMD, FRCD( C )',
				'url'   => 'nayeem-esmail-bsc-dmd-frcd-c/index.html'
			),

			array
			(
				'title' => 'Meet Our Team',
				'url'   => 'meet-our-team/index.html'
			),

			array
			(
				'title' => 'Contact Us',
				'url'   => 'contact-us/index.html'
			),
		)
	),

//Patient Information
	array
	(
		'title' => 'Patient Information',
		'url'   => 'information/index.html',
		'items' => array(
			array
			(
				'title' => 'First Visit',
				'url'   => 'information/first-visit/index.html'
			),

			array
			(
				'title' => 'Anesthesia Options',
				'url'   => 'information/anesthesia-options/index.html'
			),

			array
			(
				'title' => '3D Scanning',
				'url'   => 'information/3d-scanning/index.html'
			),

			array
			(
				'title' => 'Financing & Insurance',
				'url'   => 'information/financing-insurance/index.html'
			),

			array
			(
				'title' => 'Forms',
				'url'   => 'request-appointment/index.html',
				'items' => array(
					array
					(
						'title' => 'Request an Appointment',
						'url'   => 'request-appointment/index.html'
					),

					array
					(
						'title' => 'Referral Form',
						'url'   => 'referral/index.html'
					),

					array
					(
						'title' => 'Patient Registration',
						'url'   => 'registration/index.html'
					),
				)
			),
		)
	),

//Stories
	array
	(
		'title' => 'Stories',
		'url'   => 'testimonial/index.html',
		'items' => array(
			array
			(
				'title' => 'Dental Implants Stories',
				'url'   => 'testimonial-types/dental-implants-reviews/index.html',
				'items' => array(
					array
					(
						'title' => 'Larry Is Very Pleased With His Dental Implants',
						'url'   => 'testimonial/dental-implants-larry-abbotsford-bc/index.html'
					),

					array
					(
						'title' => 'Anna Needed an Extraction and Dental Implant',
						'url'   => 'testimonial/dental-implant-tooth-extraction-anna-abbotsford-bc/index.html'
					),

					array
					(
						'title' => 'Guya Chose a Dental Implant to Replace Her Tooth',
						'url'   => 'testimonial/dental-implant-guya-abbotsford-bc/index.html'
					),
				)
			),


			array
			(
				'title' => 'Bone Graft Stories',
				'url'   => 'testimonial-types/bone-graft-reviews/index.html',
				'items' => array(
					array
					(
						'title' => 'Bone Grafting and Dental Implants Restored Donna’s Smile',
						'url'   => 'testimonial/dental-implants-bone-graft-donna-abbotsford-bc/index.html'
					),
				)
			),


			array
			(
				'title' => 'Wisdom Teeth Removal Stories',
				'url'   => 'testimonial-types/wisdom-teeth-removal-reviews/index.html',
				'items' => array(
					array
					(
						'title' => 'Chris Underwent Wisdom Teeth Removal',
						'url'   => 'testimonial/wisdom-teeth-removal-chris-abbotsford-bc/index.html'
					),

					array
					(
						'title' => 'Justine Needed Her Wisdom Teeth Removed',
						'url'   => 'testimonial/wisdom-teeth-removal-justine-abbotsford-bc/index.html'
					),
				)
			),


			array
			(
				'title' => 'Oral Pathology Stories',
				'url'   => 'testimonial-types/oral-pathology-reviews/index.html',
				'items' => array(
					array
					(
						'title' => 'Aatif’s Oral Pathology Treatment',
						'url'   => 'testimonial/oral-pathology-aatif-abbotsford-bc/index.html'
					),

					array
					(
						'title' => 'Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed',
						'url'   => 'testimonial/oral-pathology-wisdom-teeth-removal-brenda-abbotsford-bc/index.html'
					),

					array
					(
						'title' => 'Jamin Needed an Oral Pathology Treated',
						'url'   => 'testimonial/oral-pathology-jamin-abbotsford-bc/index.html'
					),
				)
			),


			array
			(
				'title' => 'Tooth Extraction Stories',
				'url'   => 'testimonial-types/tooth-extraction-reviews/index.html',
				'items' => array(
					array
					(
						'title' => 'Dorothy Jean Needed a Tooth Extraction',
						'url'   => 'testimonial/tooth-extraction-dorothy-jean-abbotsford-bc/index.html'
					),

					array
					(
						'title' => 'Full-Arch Restoration Stories',
						'url'   => 'testimonial-types/full-arch-restoration-reviews/index.html'
					),

					array
					(
						'title' => 'Les Underwent a Full-Arch Restoration',
						'url'   => 'testimonial/full-arch-restoration-les-abbotsford-bc/index.html'
					),
				)
			),


			array
			(
				'title' => 'Jaw Surgery Stories',
				'url'   => 'testimonial-types/jaw-surgery-reviews/index.html',
				'items' => array(
					array
					(
						'title' => 'Brennan Needed Jaw Surgery After a Facial Trauma',
						'url'   => 'testimonial/facial-trauma-jaw-surgery-brennan-abbotsford-bc/index.html'
					),
				)
			),

			array
			(
				'title' => 'Facial Trauma Stories',
				'url'   => 'testimonial-types/facial-trauma-reviews/index.html',
				'items' => array(
					array
					(
						'title' => 'Marc Suffered a Facial Trauma',
						'url'   => 'testimonial/facial-trauma-marc-abbotsford-bc/index.html'
					),
				)
			),
		)
	),

//Instructions
	array
	(
		'title' => 'Instructions',
		'url'   => 'instructions/index.html',
		'items' => array(
			array
			(
				'title' => 'Instructions for Intravenous Sedation',
				'url'   => 'instructions/pre-operative-instructions/index.html'
			),

			array
			(
				'title' => 'Post-Operative Instructions',
				'url'   => 'instructions/post-operative-instructions/index.html'
			),

			array
			(
				'title' => 'Dental Implants & Bone Graft Instructions',
				'url'   => 'instructions/dental-implants-bone-graft-instructions/index.html'
			),

			array
			(
				'title' => 'Jaw Surgery Post-Operative Instructions',
				'url'   => 'instructions/jaw-surgery-post-operative-instructions/index.html'
			),

			array
			(
				'title' => 'Sinus Precautions Instructions',
				'url'   => 'instructions/sinus-precautions-instructions/index.html'
			),
		)
	),


	array
	(
		'title' => '604.504.7522',
		'url'   => 'tel:6045047522'
	),

);