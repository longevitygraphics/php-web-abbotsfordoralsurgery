<div class=" content-sidebar-wrap row " style="clear: both;">
    <div class=" content-sidebar-wrap row " style="clear: both;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 footer text-light secondary_color shade45"
             style=" text-align:center;">
            <h2 class="text-light">Abbotsford</h2>
            <div itemscope="" itemtype="http://schema.org/">
                <a href="../../contact-us/index.html" target="_blank">
                    <span itemprop="name"><strong>Abbotsford Oral Surgery and Dental Implant Centre</strong></span><br>
                </a>
                <span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress">#305-2180 Gladwin Road</span><br>
<span itemprop="addressLocality">Abbotsford</span>,
<span itemprop="addressRegion">BC</span>
<span itemprop="postalCode">V2S 0H4</span><br>
<strong>Main: </strong> <span itemprop="telephone"><a href="tel:6045047522">(604) 504-7522</a></span><br>
</span>
                <strong>Hours:</strong>
                <time itemprop="openingHours" datetime="Mo 9:00-16:00">Mon: 9:00 AM–4:00 PM</time>
                <br>
                <time itemprop="openingHours" datetime="T 8:30-16:00">Tue: 8:30 AM–4:00 PM</time>
                <br>
                <time itemprop="openingHours" datetime="W, Th 8:30-16:30">Wed, Thu: 8:30 AM–4:30 PM <br>
                    Fri: 8:30 AM–3:00 PM
                </time>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 footer secondary_color shade30">
            <h2 class="text-light ">Social</h2>
            <ul style="display:table; margin:0 auto;">
                <li class="little-links"><a href="https://www.facebook.com/abbotsfordoralsurgery/"
                                            target="_blank">
                        <div class="circle-text little  ">
                            <div><span class="dashicons icon-facebook "></span></div>
                        </div>
                    </a><a class="text-light" href="https://www.facebook.com/abbotsfordoralsurgery/"
                           target="_blank">Facebook</a></li>
                <li class="little-links"><a
                            href="https://www.google.com/maps/place/2180+Gladwin+Rd+%23305,+Abbotsford,+BC+V2S+4P8/data=!4m2!3m1!1s0x5484355cc4724009:0x199653d80f07b705?sa=X&ved=2ahUKEwiRocTvwM7kAhVGqp4KHRleCjcQ8gEwAHoECAcQAQ"
                            target="_blank">
                        <div class="circle-text little  ">
                            <div><span class="dashicons icon-google "></span></div>
                        </div>
                    </a><a class="text-light"
                           href="https://www.google.com/maps/place/2180+Gladwin+Rd+%23305,+Abbotsford,+BC+V2S+4P8/data=!4m2!3m1!1s0x5484355cc4724009:0x199653d80f07b705?sa=X&ved=2ahUKEwiRocTvwM7kAhVGqp4KHRleCjcQ8gEwAHoECAcQAQ"
                           target="_blank">Google Maps</a></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 footer secondary_color shade15">
            <h2 class="text-light ">Links</h2>
            <ul style="display:table; margin:0 auto;">
                <li class="little-links"><span style="color:#46AD4C; font-size:larger"> ◉ </span><a
                            class="text-light"
                            href="../../procedure/dental-implants-abbotsford-bc/index.html" target="_blank">Dental
                        Implants</a></li>
                <li class="little-links"><span style="color:#46AD4C; font-size:larger"> ◉ </span><a
                            class="text-light"
                            href="../../procedure/wisdom-teeth-removal-abbotsford-bc/index.html"
                            target="_blank">Wisdom Teeth Removal</a></li>
                <li class="little-links"><span style="color:#46AD4C; font-size:larger"> ◉ </span><a
                            class="text-light" href="../../request-appointment/index.html" target="_blank">Request
                        Appointment</a></li>
                <li class="little-links"><span style="color:#46AD4C; font-size:larger"> ◉ </span><a
                            class="text-light" href="../../referral/index.html" target="_blank">Referral
                        Form</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
</div>
<footer class="site-footer" itemscope itemtype="https://schema.org/WPFooter">
    <div class="wrap">Abbotsford Oral Surgery and Dental Implant Centre
        ©
        2019
    </div>
</footer>