<?php
$site_name = 'Abbotsford Oral Surgery and Dental Implant Centre';
$page_link = ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] === 'on' ? "https" : "http" ) . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//pr($_SERVER);
function pr( $a ) {
	echo '<pre>';
	print_r( $a );
	echo '</pre>';
}

?><!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<!-- Mirrored from www.abbotsfordoralsurgery.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 26 Mar 2019 21:55:59 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title><?php echo $page_title ?></title>
    <script type="text/javascript">
        var trackScrolling = false;
        var trackScrollingPercentage = false;
        var ScrollingPercentageNumber = 25;
        var stLogInterval = 60 * 1000;
        var cutOffTime = 900;
        var trackNoEvents = false;
        var trackNoMaxTime = false;
        var docTitle = '<?php echo $doc_title ?>';
    </script>

	<?php if ( $meta_description ) : ?>
        <meta name="description" content="<?php echo $meta_description ?>"/>
	<?php endif; ?>

    <link rel="canonical" href="index.html"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="<?php echo $og_type ?>"/>
    <meta property="og:title" content="<?php echo $page_title ?>"/>
	<?php if ( $meta_description ) : ?>
        <meta property="og:description" content="<?php echo $meta_description ?>"/>
	<?php endif; ?>


    <meta property="og:url" content="<?php echo $page_link ?>"/>
    <meta property="og:site_name" content="<?php echo $site_name ?>"/>
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description"
          content="<?php echo $meta_description ?>"/>
    <meta name="twitter:title" content="<?php echo $page_title ?>"/>
	<?php
	// include only for home page
	if ( $_SERVER['REQUEST_URI'] == '/index.html' ) {
		require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/schema.inc';
	}
	?>
    <link rel='dns-prefetch' href='http://ajax.googleapis.com/'/>
    <link rel='dns-prefetch' href='http://s.w.org/'/>
    <link rel="alternate" type="application/rss+xml"
          title="<?php echo $site_name ?> &raquo; Feed" href="/feed/index.html"/>
    <link rel="alternate" type="application/rss+xml"
          title="<?php echo $site_name ?> &raquo; Comments Feed"
          href="/comments/feed/index.html"/>
    <link rel="alternate" type="application/rss+xml"
          title="<?php echo $site_name ?> &raquo; <?php echo $doc_title ?> Comments Feed"
          href="feed/index.html"/>

    <link rel='stylesheet' id='wp-block-library-css'
          href='/wp-includes/css/dist/block-library/style.min3c21.css?ver=5.1.1' type='text/css' media='all'/>
    <link rel='stylesheet' id='custum-css-css' href='/wp-content/themes/nuvo-express/custom3c21.css?ver=5.1.1'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='main-css-css' href='/wp-content/themes/nuvo-express/style3c21.css?ver=5.1.1'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='bootstrap-css-css' href='/wp-content/themes/nuvo-express/bootstrap3c21.css?ver=5.1.1'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='longevity-css-css' href='/wp-content/themes/longevity/css/style.css'
          type='text/css' media='all'/>

	<?php if ( $_SERVER['REQUEST_URI'] == '/index.html' ) : ?>
        <link rel='stylesheet' id='bigvideo-css-css'
              href='/wp-content/themes/nuvo-express/css/bigvideo3c21.css?ver=5.1.1'
              type='text/css' media='all'/>
        <link rel='stylesheet' id='freezeframe-css'
              href='/wp-content/themes/nuvo-express/css/freezeframe_stylesd961.css?ver=.1' type='text/css' media='all'/>
	<?php endif; ?>

    <!--[if lt IE 9]>
    <script type='text/javascript'
            src='https://www.abbotsfordoralsurgery.com/wp-content/themes/genesis/lib/js/html5shiv.min.js?ver=3.7.3'></script>
    <![endif]-->
    <script type='text/javascript'
            src='https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js?ver=2.2.4'></script>
    <script type='text/javascript' src='/wp-content/themes/nuvo-express/js/freezeframe.pkgd3c21.js?ver=5.1.1'></script>
    <link rel='https://api.w.org/' href='/wp-json/index.html'/>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml"/>
    <link rel='shortlink' href='index.html'/>
    <link rel="alternate" type="application/json+oembed"
          href="/wp-json/oembed/1.0/embed2448.json?url=https%3A%2F%2Fwww.abbotsfordoralsurgery.com%2F"/>
    <link rel="alternate" type="text/xml+oembed"
          href="/wp-json/oembed/1.0/embed4075?url=https%3A%2F%2Fwww.abbotsfordoralsurgery.com%2F&amp;format=xml"/>

    <script>
        window.ga = window.ga || function () {
            (ga.q = ga.q || []).push(arguments)
        };
        ga.l = +new Date;
        ga('create', 'UA-124964091-5', 'auto');
        ga('send', 'pageview');
    </script>
    <script async src='//www.google-analytics.com/analytics.js'></script>

    <meta name="google-site-verification" content="T7J03D_LbHDDos8X7p3_rL54QXgDZsqZG_raJvxA28M"/>

    <style type="text/css">
        .home > div.animated-home.bounce.site-container > div > div > div:nth-child(2) > div:nth-child(1) > div > div:nth-child(4) > a > div {
            background: linear-gradient(rgba(17, 113, 175, 0.1), rgba(17, 113, 175, 0.1)), url(wp-content/uploads/brenda-oral-pathology-abbotsford-bc-1024x576.jpg) center !important;
            background-size: cover !important;
        }

        .home > div.animated-home.bounce.site-container > div > div > div:nth-child(2) > div:nth-child(1) > div > div:nth-child(5) > a > div {
            background: linear-gradient(rgba(17, 113, 175, 0.1), rgba(17, 113, 175, 0.1)), url(wp-content/uploads/Doc-Profile-1024x537.jpg) center !important;
            background-size: cover !important;
        }</style>


    <style type="text/css">
        body > div.animated-home.bounce.site-container > div > div > div.content-sidebar-wrap.row > div,
        div.content-sidebar-wrap.row {
            background-color: #4B8F49 !important;
        }</style>


    <style type="text/css">
        .single-format-standard .entry-content a {
            font-weight: 500;
            font-style: italic;
            text-decoration: underline !important;
        }</style>


    <style type="text/css">
        @media only screen and (max-width: 1024px) {

            .home .home-hero {
                background: url(wp-content/uploads/About-Our-Practice.jpg) 50% 50% fixed no-repeat !important;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                position: relative;
                background-size: cover !important;
                background-attachment: scroll !important;
            }

        }</style>


    <style type="text/css">
        .home > div.animated-home.bounce.site-container > div > div > div:nth-child(2) > div:nth-child(1) > div > div:nth-child(1),
        .home > div.animated-home.bounce.site-container > div > div > div:nth-child(2) > div:nth-child(1) > div > div:nth-child(3) > div {
            text-align: center;
        }</style>


    <style type="text/css">
        #big-video-vid {
            opacity: .8 !important;
        }

        .page-template-video-max-landing .custom-html-widget {
            text-shadow: 2px 2px 2px #00000063;
        }

        .page-template-video-max-landing .home-hero {
            background: transparent;
        }</style>


    <style type="text/css">
        @media (min-width: 768px) {
            .footer {
                height: 232px;
            }
        }</style>


    <style type="text/css">
        .home > div:nth-child(2) > div > div > div:nth-child(1) > div > div:nth-child(1) > div > div > p {
            margin-bottom: 0;
        }

        .home h1.mainh1 {
            margin-top: 0 !important;
            font-size: 1.8em;
        }

        /**
		.home > div:nth-child(2) > div > div > div:nth-child(1) > div > div:nth-child(1) > div,
		.home > div.animated-home.bounce.site-container > div > div > div:nth-child(2) > div > div > div{
		  text-align: center;
		  display: -webkit-box;
		  display: -ms-flexbox;
		  display: flex;
		  -webkit-box-orient: vertical;
		  -webkit-box-direction: normal;
			  -ms-flex-direction: column;
				  flex-direction: column;
		  -webkit-box-align: center;
			  -ms-flex-align: center;
				  align-items: center;
		  -webkit-box-pack: center;
			  -ms-flex-pack: center;
				  justify-content: center;
		}

		.home > div:nth-child(2) > div > div > div:nth-child(1) > div > div:nth-child(1) > div > div > span {
		  display: none;
		}**/</style>


    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var newFooterText = " | <a href='/terms'>Terms</a> | <a href='/privacy-policy'>Privacy Policy</a>";
            $(".site-footer .wrap").append(newFooterText);
        });</script>

    <style type="text/css">.site-title a {
            background: url(/wp-content/uploads/ABOT-Logo-NonRetna.png) no-repeat !important;
        }</style>
    <style type="text/css" id="custom-background-css">
        body.custom-background {
            background-image: url("wp-content/uploads/ABOT-Background-Main.jpg");
            background-position: center center;
            background-size: contain;
            background-repeat: repeat;
            background-attachment: scroll;
        }
    </style>
    <link rel="icon" href="wp-content/uploads/cropped-ABOT-Favicon-32x32.png" sizes="32x32"/>
    <link rel="icon" href="wp-content/uploads/cropped-ABOT-Favicon-192x192.png" sizes="192x192"/>
    <link rel="apple-touch-icon-precomposed" href="wp-content/uploads/cropped-ABOT-Favicon-180x180.png"/>
    <meta name="msapplication-TileImage"
          content="https://www.abbotsfordoralsurgery.com/wp-content/uploads/cropped-ABOT-Favicon-270x270.png"/>

	<?php if ( isset( $other_html ) && ! empty( $other_html ) ) {
		echo $other_html;
	} ?>

</head>