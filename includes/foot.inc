<link rel='stylesheet' id='dashicons-css' href='/wp-includes/css/dashicons.min3c21.css?ver=5.1.1' type='text/css'
      media='all'/>
<link rel='stylesheet' id='google-fonts-css'
      href='http://fonts.googleapis.com/css?family=Lato%3A300%2C900%2C700%2C300italic%7CTitillium+Web%3A600%7CBilbo%3A%3Alatin%7CLibre+Baskerville%3A400italic&amp;ver=.1'
      type='text/css' media='all'/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500&display=swap" rel="stylesheet">

<link rel='stylesheet' id='nuvo-iconset-css' href='/wp-content/themes/nuvo-express/css/nuvo-iconsetd961.css?ver=.1'
      type='text/css' media='all'/>
<link rel='stylesheet' id='fancybox-css-css' href='/wp-content/themes/nuvo-express/jquery.fancyboxd961.css?ver=.1'
      type='text/css' media='all'/>


<script type='text/javascript' src='/wp-includes/js/comment-reply.min3c21.js?ver=5.1.1'></script>
<script type='text/javascript' src='/wp-content/themes/genesis/lib/js/skip-links.min1102.js?ver=2.8.1'></script>
<script type='text/javascript' src='/wp-content/plugins/reduce-bounce-rate/js/analyticsjs3c21.js?ver=5.1.1'></script>
<script type='text/javascript' src='/wp-includes/js/wp-embed.min3c21.js?ver=5.1.1'></script>
<script type='text/javascript' src='/wp-content/themes/nuvo-express/js/responsive-menu8a54.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/themes/nuvo-express/js/jquery.fancybox8a54.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/themes/nuvo-express/js/jquery.fancybox-media8a54.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/themes/longevity/js/affix.js'></script>
<script type='text/javascript' src='/wp-content/themes/longevity/js/main.js'></script>
<?php
//add these only on home page
if ( $_SERVER['REQUEST_URI'] == '/index.html' ): ?>
    <script type='text/javascript' src='/wp-content/plugins/wp-retina-2x/js/retina.min001e.js?ver=2.0.0'></script>
    <!--    <script type='text/javascript' src='/wp-content/themes/nuvo-express/js/video.mince52.js?ver=5.0.2'></script>-->
    <!--    <script type='text/javascript' src='/wp-content/themes/nuvo-express/js/bigvideo3c21.js?ver=5.1.1'></script>-->
    <!--    <script type='text/javascript' src='/wp-content/themes/nuvo-express/js/bigvideo-init8a54.js?ver=1.0.0'></script>-->
<?php endif; ?>
<div class="floating-button">
    <a href="/contact-us/">Contact Us</a>
</div>
</body>
<!-- Mirrored from www.abbotsfordoralsurgery.com/information/anesthesia-options/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 26 Mar 2019 21:57:38 GMT -->
</html>