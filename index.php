<?php
$page_title       = 'Abbotsford Oral Surgery and Dental Implant Centre';
$doc_title        = 'Home';
$meta_description = 'Our Abbotsford, BC, practice provides comprehensive and compassionate oral surgery care, including dental implant treatments and wisdom teeth removal.';
$og_type          = 'website';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="home page-template page-template-landing-pages page-template-video-max-landing page-template-landing-pagesvideo-max-landing-php page page-id-128 custom-background custom-header header-image content-sidebar"
      itemscope itemtype="https://schema.org/WebPage">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="home-hero fill">
    <div id="big-video-wrap" style="display:block;width: 100%;height:100%;">
        <div style="width: 100%;height:100%;max-height:100%" webkit-playsinline="" data-setup="{}" preload="auto"
             class="video-js vjs-default-skin big-video-vid-dimensions vjs-controls-disabled vjs-has-started vjs-playing vjs-user-inactive"
             id="big-video-vid" muted="true">
            <video muted="" id="big-video-vid_html5_api" class="vjs-tech" preload="auto" data-setup="{}"
                   webkit-playsinline=""
                   style="position: relative; overflow: hidden;object-fit: cover;max-width:100%;max-height:100%;height: 100%;width: 100%;"
                   autoplay="" src="wp-content/themes/nuvo-express/videos/sizzle.mp4"></video>
        </div>
    </div>
    <div id="home-hero-content" style="z-index: 1;">
        <div class="wrap">
            <section id="custom_html-2" class="widget_text widget widget_custom_html">
                <div class="widget_text widget-wrap">
                    <div class="textwidget custom-html-widget"><strong>About</strong> the Practice<a id="play"
                                                                                                     class="fancybox-youtube btn-play"
                                                                                                     href="https://www.youtube.com/embed/ZXwiXYGhBX8?autoplay=1&amp;autohide=1&amp;rel=0&amp;showinfo=0">Play</a>Watch
                        the <strong>Video</strong><br></div>
                </div>
            </section>
        </div>
    </div>
</div>
<div class="animated-home bounce site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
        <li><a href="#genesis-sidebar-primary" class="screen-reader-shortcut"> Skip to primary sidebar</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">

            <div class="content-sidebar-wrap">
                <div class="row no-gutters">

                    <div class="col-md-12 row no-gutters"
                         style="background: linear-gradient( rgba(17,113,175,0.7), rgba(17,113,175,0.7) ), url() center;">

                        <div class="col-xs-12 col-md-6 row no-gutters">
                            <div class="col-xs-12 col-md-12 feedperson shade30">
                                <div class="col-md-12 col-xs-12">
                                    <span class="spacer20"></span>
                                    <h1 class="text-light mainh1">Abbotsford Oral Surgery and Dental Implant Centre</h1>
                                    <p class="text-light">Our Abbotsford, BC, practice provides comprehensive and
                                        compassionate oral surgery care, including dental implant treatments and wisdom
                                        teeth removal.</p>
                                </div>
                            </div>
                        </div>

                        <a rel="nofollow" class="fancybox-youtube type3" href="patient-registration/index.html"
                           target="_blank">
                            <div class="col-xs-12 col-sm-6 col-md-3 feedperson text-center ">
                                <div class="col-md-12 col-xs-12 ">
                                    <span class="spacer50"></span><span class="spacer30"></span>
                                    <button type="button" class="btn-md pri ">Patient Registration</button>
                                </div>
                            </div>
                        </a>
                        <a rel="nofollow" class="fancybox-youtube type3" href="referral-form/index.html"
                           target="_blank">
                            <div class="col-xs-12 col-sm-6 col-md-3 feedperson text-center ">
                                <div class="col-md-12 col-xs-12 ">
                                    <span class="spacer50"></span><span class="spacer30"></span>
                                    <button type="button" class="btn-md pri ">Referral Form</button>
                                </div>
                            </div>
                        </a>

                        <div class="col-xs-12 col-md-6 row no-gutters">
                            <a class="fancybox-youtube"
                               href="https://www.youtube.com/embed/ohEWhZra3_E?autoplay=1&amp;autohide=1&amp;rel=0&amp;showinfo=0">
                                <div class="col-xs-12 col-md-12 feedperson"
                                     style="background: linear-gradient( rgba(17,113,175,0.1), rgba(17,113,175,0.1) ), url(wp-content/uploads/brenda-oral-pathology-abbotsford-bc-750x600.jpg) center ; background-position: center ;">
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) ; background-position: center ;">
<div class="col-md-12 col-xs-12">
<div class="col-md-3 col-xs-3 feedperson "><span id="playhover"></span></div>
<div class="col-md-9 col-xs-9">
<span class="spacer50"></span><span class="spacer10"></span>
<h2 class="text-light">Patient Stories</h2>
<h4 class="text-light">Watch the Video</h4>
</div>
</div>
</span>
                                </div>
                            </a>
                        </div>


                        <div class="col-xs-12 col-md-6 row no-gutters">
                            <a class="fancybox-youtube"
                               href="https://www.youtube.com/embed/evxyq-E3JnU?autoplay=1&amp;autohide=1&amp;rel=0&amp;showinfo=0">
                                <div class="col-xs-12 col-md-12 feedperson"
                                     style="background: linear-gradient( rgba(17,113,175,0.1), rgba(17,113,175,0.1) ), url(wp-content/uploads/Doc-Profile-750x600.jpg) center ; background-position: center ;">
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) ; background-position: center ;">
<div class="col-md-12 col-xs-12">
<div class="col-md-3 col-xs-3 feedperson "><span id="playhover"></span></div>
<div class="col-md-9 col-xs-9">
 <span class="spacer50"></span><span class="spacer10"></span>
<h2 class="text-light">Meet the Doctor</h2>
<h4 class="text-light">Watch the Video</h4>
</div>
</div>
</span>
                                </div>
                            </a>
                        </div>

                        <div class="row">

                            <div class="col-xs-12 col-sm-12  primary_color text-light featured-text no-gutters">
                                <div class=" col-md-12"
                                     style="background: linear-gradient( rgba(17,113,175,0.1), rgba(17,113,175,0.3) ), url() center; padding: 8%;">
                                    <h2><a href="about-our-oral-surgery-practice/index.html">
                                            <strong>About Our Practice</strong>
                                        </a></h2>
                                    <p>Welcome to Abbotsford Oral Surgery and Dental Implant Centre in Abbotsford, BC,
                                        the practice of Dr. Nayeem Esmail. Dr. Esmail and his team provide the highest
                                        quality oral surgery care for patients of all ages with optimal comfort in mind.
                                        Dr. Esmail’s specialized surgical and anesthesia training gives him the ability
                                        to diagnose and treat the full scope of oral and maxillofacial conditions, from
                                        dental implant placement, bone grafting and wisdom teeth removal to complex
                                        corrective jaw surgery and full-arch restoration. </p>
                                    <p>We encourage you to browse our website to learn more about our trusted practice
                                        and the services we offer. If you live in Abbotsford or the surrounding
                                        communities and are looking to restore your oral function or if you have any
                                        further questions about our procedures, please feel free to call our office with
                                        any questions you may have or to schedule a consultation appointment with Dr
                                        Esmail. We appreciate the opportunity to care for you at Abbotsford Oral Surgery
                                        and Dental Implant Centre. </p>
                                </div>
                            </div>

                        </div>


                    </div>

                </div>

                <div class="row no-gutters"
                     style="background: linear-gradient( rgba(0,169,222,0.1), rgba(0,169,222,0.3) ), url() center;">
                    <div class="row">
                        <div class="col-xs-12 related_videos secondary_color">
                            <h2 class="clearboth">Watch Stories & Reviews</h2>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3 ">
                        <a href="testimonial/wisdom-teeth-removal-rita-mother-abbotsford-bc/index.html"
                           title='Rita’s Son Had His Wisdom Teeth Removed'>
<span id="playhover" class="blueboxplay"
      style="background: linear-gradient( rgba(17,113,175,0.8) , rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">Rita - 03/20/2019</sub>
</div>
</span>
                            <span id="playhover" class="col-md-4 col-xs-12 feedperson "
                                  style="background-position: center !important;"></span>
                            <img class="fit-img"
                                 src="wp-content/uploads/rita-mother-wisdom-teeth-removal-abbotsford-bc-286x200.jpg"
                                 width="286" height="200"
                                 alt='Wisdom Teeth Removal in Abbotsford, BC: Rita&#039;s son | Abbotsford Oral Surgery and Dental Implant Centre'
                                 title='Rita’s Son Had His Wisdom Teeth Removed'/>
                        </a>
                    </div>
                    <div class="col-xs-6 col-md-3 ">
                        <a href="testimonial/dental-implants-larry-abbotsford-bc/index.html"
                           title='Larry Is Very Pleased With His Dental Implants'>
<span id="playhover" class="blueboxplay"
      style="background: linear-gradient( rgba(17,113,175,0.8) , rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">Larry - 02/13/2019</sub>
</div>
</span>
                            <span id="playhover" class="col-md-4 col-xs-12 feedperson "
                                  style="background-position: center !important;"></span>
                            <img class="fit-img"
                                 src="wp-content/uploads/larry-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                                 height="200"
                                 alt='Dental implants in Abbotsford, BC: Larry | Abbotsford Oral Surgery and Dental Implant Centre'
                                 title='Larry Is Very Pleased With His Dental Implants'/>
                        </a>
                    </div>
                    <div class="col-xs-6 col-md-3 ">
                        <a href="testimonial/tooth-extraction-rosa-abbotsford-bc/index.html"
                           title='Rosa Needed a Tooth Extraction'>
<span id="playhover" class="blueboxplay"
      style="background: linear-gradient( rgba(17,113,175,0.8) , rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">Rosa - 01/07/2019</sub>
</div>
</span>
                            <span id="playhover" class="col-md-4 col-xs-12 feedperson "
                                  style="background-position: center !important;"></span>
                            <img class="fit-img"
                                 src="wp-content/uploads/rosa-tooth-extraction-abbotsford-bc-286x200.jpg" width="286"
                                 height="200" alt='Rosa the extraction patient in Abbotsford, BC'
                                 title='Rosa Needed a Tooth Extraction'/>
                        </a>
                    </div>
                    <div class="col-xs-6 col-md-3 ">
                        <a href="testimonial/wisdom-teeth-removal-kristin-abbotsford-bc/index.html"
                           title='Kristin Needed Her Wisdom Teeth Extracted'>
<span id="playhover" class="blueboxplay"
      style="background: linear-gradient( rgba(17,113,175,0.8) , rgba(17,113,175,0.8) ) !important; background-position: center !important;">
 <div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">Kristin - 12/17/2018</sub>
</div>
</span>
                            <span id="playhover" class="col-md-4 col-xs-12 feedperson "
                                  style="background-position: center !important;"></span>
                            <img class="fit-img"
                                 src="wp-content/uploads/kristin-wisdom-teeth-removal-abbotsford-bc-286x200.jpg"
                                 width="286" height="200" alt='Kristin the wisdom teeth patient in Abbotsford, BC'
                                 title='Kristin Needed Her Wisdom Teeth Extracted'/>
                        </a>
                    </div>
                </div>


                <div class="row no-gutters">
                </div>


                <div class="row">
                    <div class="col-xs-12 col-md-12 keylocations row primary_color gradient">
                        <div class="col-xs-12 col-sm-12 col-md-6 ">
                            <hr class="visible-xs mobile-line">
                            <h2 class="text-center text-light">Procedures</h2>
                            <hr class="visible-xs mobile-line">

                            <ul class="row">
                                <li class="linkpod col-sm-6 col-md-12 landing-mobile-text">
                                    <div class="col-md-3">
                                        <div>
                                            <a href="procedure/dental-implants-abbotsford-bc/index.html">
                                                <div class=" img-responsive center-block">
                                                    <div>
                                                        <span class="icon-implant"></span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <a href="procedure/dental-implants-abbotsford-bc/index.html">
                                            <strong>Dental Implants</strong>
                                            <br>
                                            <p class="hidden-xs">Replacing teeth with dental implants provides a
                                                functional and natural-looking solution.</p>
                                        </a>
                                    </div>
                                </li>
                                <hr class="visible-xs mobile-line">
                                <li class="linkpod col-sm-6 col-md-12 landing-mobile-text">
                                    <div class="col-md-3">
                                        <div>
                                            <a href="procedure/wisdom-teeth-removal-abbotsford-bc/index.html">
                                                <div class=" img-responsive center-block">
                                                    <div>
                                                        <span class="icon-wisdom-teeth"></span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <a href="procedure/wisdom-teeth-removal-abbotsford-bc/index.html">
                                            <strong>Wisdom Teeth</strong>
                                            <br>
                                            <p class="hidden-xs">We recommend having wisdom teeth removed to prevent
                                                oral health issue from arising.</p>
                                        </a>
                                    </div>
                                </li>
                                <hr class="visible-xs mobile-line">
                                <li class="linkpod col-sm-6 col-md-12 landing-mobile-text">
                                    <div class="col-md-3">
                                        <div>
                                            <a href="procedure/full-arch-restoration/index.html">
                                                <div class=" img-responsive center-block">
                                                    <div>
                                                        <span class="icon-all-on-four"></span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <a href="procedure/full-arch-restoration/index.html">
                                            <strong>Full-Arch Restoration</strong>
                                            <br>
                                            <p class="hidden-xs">You can replace an entire arch of teeth using dental
                                                implants and full-arch restoration.</p>
                                        </a>
                                    </div>
                                </li>
                                <hr class="visible-xs mobile-line">
                                <li class="linkpod col-sm-6 col-md-12 landing-mobile-text">
                                    <div class="col-md-3">
                                        <div>
                                            <a href="procedure/bone-grafting/index.html">
                                                <div class=" img-responsive center-block">
                                                    <div>
                                                        <span class="icon-bone-grafting"></span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <a href="procedure/bone-grafting/index.html">
                                            <strong>Bone Grafts</strong>
                                            <br>
                                            <p class="hidden-xs">Bone grafting can supplement the jaw bone and provide
                                                an adequate foundation for implants.</p>
                                        </a>
                                    </div>
                                </li>
                                <hr class="visible-xs mobile-line">
                            </ul>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6  ">
                            <h2 class="text-center text-light">Stories</h2>
                            <hr class="visible-xs mobile-line">

                            <ul class="row">
                                <a href="dental-implant-guya-abbotsford-bc/index.html">
                                    <li class="linkpod col-sm-6 col-md-12">
                                        <div class="col-md-3">

                                            <div style="max-width: 150px; margin: auto;">

                                                <img src="wp-content/uploads/guya-dental-implants-abbotsford-bc-e1538090225803-150x150.jpg"
                                                     alt="Guya the dental implants patient in Abbotsford, BC"
                                                     width="150" height="150" class="img-circle center-block"/>
                                            </div>
                                        </div>
                                        <div class="col-md-9 landing-mobile-text">
                                            <strong>Guya's Dental Implant</strong><br>
                                            <p class="hidden-xs">A dental implant perfectly restored Guya's tooth after
                                                a cavity caused her cap to fail.</p>
                                        </div>
                                    </li>
                                </a>
                                <hr class="visible-xs mobile-line">
                                <a href="wisdom-teeth-removal-justine-abbotsford-bc/index.html">
                                    <li class="linkpod col-sm-6 col-md-12">
                                        <div class="col-md-3">

                                            <div style="max-width: 150px; margin: auto;">

                                                <img src="wp-content/uploads/justine-wisdom-teeth-removal-abbotsford-bc-150x150.jpg"
                                                     alt="Justine the wisdom teeth patient in Abbotsford, BC"
                                                     width="150" height="150" class="img-circle center-block"/>
                                            </div>
                                        </div>
                                        <div class="col-md-9 landing-mobile-text">
                                            <strong>Justine's Wisdom Teeth</strong><br>
                                            <p class="hidden-xs">Justine felt comfortable with Dr. Esmail's experience
                                                during her wisdom teeth removal.</p>
                                        </div>
                                    </li>
                                </a>
                                <hr class="visible-xs mobile-line">
                                <a href="full-arch-restoration-les-abbotsford-bc/index.html">
                                    <li class="linkpod col-sm-6 col-md-12">
                                        <div class="col-md-3">

                                            <div style="max-width: 150px; margin: auto;">

                                                <img src="wp-content/uploads/les-full-arch-restoration-abbotsford-bc-150x150.jpg"
                                                     alt="Les the full-arch patient in Abbotsford, BC" width="150"
                                                     height="150" class="img-circle center-block"/>
                                            </div>
                                        </div>
                                        <div class="col-md-9 landing-mobile-text">
                                            <strong>Les' Full Arch</strong><br>
                                            <p class="hidden-xs">Les chose to completely restore all of his teeth and
                                                smile with a full-arch restoration.</p>
                                        </div>
                                    </li>
                                </a>
                                <hr class="visible-xs mobile-line">
                                <a href="dental-implants-bone-graft-donna-abbotsford-bc/index.html">
                                    <li class="linkpod col-sm-6 col-md-12">
                                        <div class="col-md-3">

                                            <div style="max-width: 150px; margin: auto;">

                                                <img src="wp-content/uploads/donna-dental-implants-abbotsford-bc-150x150.jpg"
                                                     alt="Donna the dental implants patient in Abbotsford, BC"
                                                     width="150" height="150" class="img-circle center-block"/>
                                            </div>
                                        </div>
                                        <div class="col-md-9 landing-mobile-text">
                                            <strong>Donna's Bone Graft</strong><br>
                                            <p class="hidden-xs">Donna needed a sinus lift bone graft when she restored
                                                her upper teeth with implants.</p>
                                        </div>
                                    </li>
                                </a>
                                <hr class="visible-xs mobile-line">
                            </ul>

                        </div>
                    </div>
                </div>

                <div class="row">


                </div>
                <ul>
                    <li class="slide hidden-xs hidden-sm">
                        <a href="patient-stories/index.html">
                            <img src="wp-content/uploads/ABOT_FaceWall_Banner-1144x200.jpg"
                                 alt="Patient Stories in Abbotsford, BC" title="Patient Stories in Abbotsford, BC"/>
                        </a>
                    </li>
                </ul>
                <div class="row no-gutters">
                    <div class="col-xs-12 col-md-12">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7397.767641803216!2d-122.31575674195159!3d49.03900507166582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5484354461678c0f%3A0xd39d9619933dd651!2sAbbotsford%20Oral%20Surgery%20and%20Dental%20Implant%20Centre%3A%20Dr.%20Nayeem%20Esmail%2C%20Inc.!5e0!3m2!1sen!2sca!4v1570469357043!5m2!1sen!2sca"
                                width="1144" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <main class="content" id="genesis-content"></main>
                <aside class="sidebar sidebar-primary widget-area primary_color text-light  " role="complementary"
                       aria-label="Primary Sidebar" itemscope itemtype="https://schema.org/WPSideBar"
                       id="genesis-sidebar-primary"><h2 class="genesis-sidebar-title screen-reader-text">Primary
                        Sidebar</h2></aside>
            </div>
			<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
            <style type="text/css">
                .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
                    background-color: #1171AF !important;
                    color: #fff !important;
                }

                .secondary_color {
                    background-color: #46AD4C;
                }

                .highlight_color {
                    background-color: #32C5F4 !important;
                }

                ;
                .site-inner {
                    background-color: #1171AF !important;
                }

                /*Main Homepage*/
                .gradient {
                    background: #00AEEF; /* Old browsers */
                    background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
                    background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
                    background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
                    background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
                    background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
                    background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
                    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
                    Padding: 10%;
                }

                .home input {
                    color: rgba(17, 113, 175, 1) !important;
                }

                /*body.custom-background {
				  background-color: rgba(17,113,175,1) !important;
				}*/

                /*Menu*/
                .nav-primary .sub-menu a {
                    background-color: #1171AF;
                    border-color: #fff;
                    color: #fff !important;
                }

                .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
                    background-color: #1171AF;
                }

                /*Add primary color to the number CTA*/
                .callus a {
                    font-weight: bold;
                    color: #1171AF !important;
                }

                /*Images*/
                .background_cta {
                    background-image: url();
                }

                .home-map-image {
                    background-image: url();
                    background-size: cover;
                    background-position: Center Center !important;
                }

                /*Testimonial Page*/
                .related_videos {
                    border-top: 7px solid #46AD4C;
                }

                /*Youtube Video */
                .video_thumb {
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: top center;
                    height: 400px;
                }

                /*Change play button color on all inline video images*/
                .content #playhover,
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
                .main-home #playhover,
                .page-template-hero-min-landing #playhover,
                .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
                .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
                    background: url(wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
                }

                /*Add line to the botton of inline video image*/
                article #playhover + img {
                    border-bottom: 5px solid #46AD4C;
                }


                /*Fancy Overlay*/
                #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
                    background-image: url(wp-content/uploads/ABOT-Background-Dark.jpg) !important;
                    background-size: cover !important;
                    background-position: left top !important;
                }

                /*Archive Pages*/
                .archive-description, .author-box {
                    border-top: 7px solid #46AD4C;
                    border-left: 0px;
                    border-right: 0px;
                }

                .archive .entry-header a {
                    color: #fff;
                    font-size: 1em;
                }


                /*Team Page*/
                .tiled-gallery .gallery-row, .bio-image img {
                    border-bottom: 3px solid #1171AF;
                }

                /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
                .team .entry-content img, .team .gallery-item img {
                    border-bottom: 5px solid #1171AF;
                }

                /*Links*/
                a {
                    color: #1171AF;
                    text-decoration: none !important;
                }

                /*Change current menu color*/
                /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

                /*BTNS*/
                .button {
                    background-color: #46AD4C !important;
                }

                /*Add colored bullet points*/
                .entry-content ul {
                    list-style-type: none;
                    position: relative;
                    padding-left: 0;
                }

                .entry-content ul > li {
                    list-style: none;
                }

                .entry-content ul > li:before {
                    content: "• \00a0 \00a0 \00a0";
                    color: #1171AF; /* Color of the bullet */
                    position: absolute;
                    left: -1em;
                    margin-right: 5px;
                }

                /*Remove bullets on feedback page*/
                .page-template-page_feedback .entry-content ul > li:before {
                    content: "" !important;
                }

                /* Video Player Pages
				--------------------------------------------- */
                .fancybox-wrap:before {
                    background: url(wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
                    content: ".";
                    position: relative;
                    width: 100%;
                    height: 130px;
                    z-index: 8040;
                    display: block;
                    margin-bottom: 20px;
                    background-size: contain !important;
                }

                /*Homepage - Max Width HeroCard - change line under image*/
                .page-template-hero-max-landing > a > img {
                    border-bottom: 4px solid #1171AF;
                }

                /*change color on landinage page background*/
                .animated-home {
                    background-color: #1171AF;
                }

                /*Homepage - Max Width HeroCard and full page content - change line under image*/
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
                .page-template-hero-max-landing > .row > .col-md-12 a img {
                    border-bottom: 4px solid #1171AF;
                    width: 100%;
                }


                /* Archive Page
				--------------------------------------------- */
                .circle-text:after {
                    background-color: #32C5F4 !important;
                }

                body .entry-content .gfield_label {
                    font-size: 30px !important;
                    color: #1171AF;
                }

                /*Add BG image to full screen pages.  Going to remove this for now*/
                /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
					background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
					background-size: cover;
					background-position: left top !important;
				}*/

                .full-width-content.custom-background {
                    background-image: url("wp-content/uploads/ABOT-Background-Dark.jpg") !important;
                    background-attachment: fixed;
                }


                body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
                    border: 1px solid #1171AF;
                    background-color: #FFF8BD;
                }


                .gform_wrapper .percentbar_blue {
                    background-image: url('wp-content/themes/nuvo-express/images/opas_15.png');
                    background-repeat: repeat-x;
                    background-color: #1171AF;
                    color: #FFF;
                }

                /* Make some mobile Tweaks
				--------------------------------------------- */
                @media only screen and (max-width: 1023px) {
                    .mobile-cta {
                        color: #1171AF !important;
                    }

                    /*Make mobile help menu primary color*/
                }

                /* This tweaks only Small devices (tablets and phones, 768px and up) */
                @media (max-width: 900px) {
                    .fancybox-youtube > .feedperson, .feedperson.shade30 {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under main videos on homepage / Move background face down*/
                    div.col-md-12.row.no-gutters > a > div.feedperson {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under CTA buttons on homepage*/
                }

                /*Add custom color to the case study block on homepage*/
                .image-holder:after {
                    background-color: rgba(70, 173, 76, 0.7);

                }

                /*Add video overlay on main sizzle as a dynamic layer*/
                .home-hero {
                    background: rgba(17, 113, 175, 1) !important;
                }

                @media only screen and (max-width: 1024px) {

                    .home-hero:after {
                        background: rgba(17, 113, 175, 0.8);
                    }

                }

            </style>
        </div>
        <div class="mask">&nbsp;</div>
		<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
