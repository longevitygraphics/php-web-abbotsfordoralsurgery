<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>Abbotsford Oral Surgery and Dental Implant Centre</title>
	<atom:link href="https://www.abbotsfordoralsurgery.com/testimonial-types/wisdom-teeth-removal-reviews/feed/" rel="self" type="application/rss+xml" />
	<link>https://www.abbotsfordoralsurgery.com</link>
	<description></description>
	<lastBuildDate>
	Tue, 26 Mar 2019 21:53:02 +0000	</lastBuildDate>
	<language>en-US</language>
	<sy:updatePeriod>
	hourly	</sy:updatePeriod>
	<sy:updateFrequency>
	1	</sy:updateFrequency>
	<generator>https://wordpress.org/?v=5.1.1</generator>

<image>
	<url>https://www.abbotsfordoralsurgery.com/wp-content/uploads/cropped-ABOT-Favicon-32x32.png</url>
	<title>Abbotsford Oral Surgery and Dental Implant Centre</title>
	<link>https://www.abbotsfordoralsurgery.com</link>
	<width>32</width>
	<height>32</height>
</image> 
	<item>
		<title>Rita&#8217;s Son Had His Wisdom Teeth Removed</title>
		<link>https://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-rita-mother-abbotsford-bc/</link>
				<pubDate>Wed, 20 Mar 2019 17:31:49 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">https://www.abbotsfordoralsurgery.com/?post_type=testimonial&#038;p=99248</guid>
				<description><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-rita-mother-abbotsford-bc/">Rita&#8217;s Son Had His Wisdom Teeth Removed</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-rita-mother-abbotsford-bc/">Rita&#8217;s Son Had His Wisdom Teeth Removed</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
										</item>
		<item>
		<title>Kristin Needed Her Wisdom Teeth Extracted</title>
		<link>https://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-kristin-abbotsford-bc/</link>
				<pubDate>Mon, 17 Dec 2018 18:54:30 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">https://www.abbotsfordoralsurgery.com/?post_type=testimonial&#038;p=99238</guid>
				<description><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-kristin-abbotsford-bc/">Kristin Needed Her Wisdom Teeth Extracted</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-kristin-abbotsford-bc/">Kristin Needed Her Wisdom Teeth Extracted</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
										</item>
		<item>
		<title>Chris Underwent Wisdom Teeth Removal</title>
		<link>https://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-chris-abbotsford-bc/</link>
				<pubDate>Thu, 21 Jun 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-chris-abbotsford-bc/</guid>
				<description><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-chris-abbotsford-bc/">Chris Underwent Wisdom Teeth Removal</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-chris-abbotsford-bc/">Chris Underwent Wisdom Teeth Removal</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
										</item>
		<item>
		<title>Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed</title>
		<link>https://www.abbotsfordoralsurgery.com/testimonial/oral-pathology-wisdom-teeth-removal-brenda-abbotsford-bc/</link>
				<pubDate>Tue, 15 May 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/testimonial/oral-pathology-wisdom-teeth-removal-brenda-abbotsford-bc/</guid>
				<description><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/oral-pathology-wisdom-teeth-removal-brenda-abbotsford-bc/">Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/oral-pathology-wisdom-teeth-removal-brenda-abbotsford-bc/">Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
										</item>
		<item>
		<title>Justine Needed Her Wisdom Teeth Removed</title>
		<link>https://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-justine-abbotsford-bc/</link>
				<pubDate>Thu, 15 Mar 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-justine-abbotsford-bc/</guid>
				<description><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-justine-abbotsford-bc/">Justine Needed Her Wisdom Teeth Removed</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/wisdom-teeth-removal-justine-abbotsford-bc/">Justine Needed Her Wisdom Teeth Removed</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
										</item>
	</channel>
</rss>
