<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>Abbotsford Oral Surgery and Dental Implant Centre</title>
	<atom:link href="https://www.abbotsfordoralsurgery.com/testimonial-types/tooth-extraction-reviews/feed/" rel="self" type="application/rss+xml" />
	<link>https://www.abbotsfordoralsurgery.com</link>
	<description></description>
	<lastBuildDate>
	Tue, 26 Mar 2019 21:53:02 +0000	</lastBuildDate>
	<language>en-US</language>
	<sy:updatePeriod>
	hourly	</sy:updatePeriod>
	<sy:updateFrequency>
	1	</sy:updateFrequency>
	<generator>https://wordpress.org/?v=5.1.1</generator>

<image>
	<url>https://www.abbotsfordoralsurgery.com/wp-content/uploads/cropped-ABOT-Favicon-32x32.png</url>
	<title>Abbotsford Oral Surgery and Dental Implant Centre</title>
	<link>https://www.abbotsfordoralsurgery.com</link>
	<width>32</width>
	<height>32</height>
</image> 
	<item>
		<title>Rosa Needed a Tooth Extraction</title>
		<link>https://www.abbotsfordoralsurgery.com/testimonial/tooth-extraction-rosa-abbotsford-bc/</link>
				<pubDate>Mon, 07 Jan 2019 22:12:07 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">https://www.abbotsfordoralsurgery.com/?post_type=testimonial&#038;p=99243</guid>
				<description><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/tooth-extraction-rosa-abbotsford-bc/">Rosa Needed a Tooth Extraction</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/tooth-extraction-rosa-abbotsford-bc/">Rosa Needed a Tooth Extraction</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
										</item>
		<item>
		<title>Anna Needed an Extraction and Dental Implant</title>
		<link>https://www.abbotsfordoralsurgery.com/testimonial/dental-implant-tooth-extraction-anna-abbotsford-bc/</link>
				<pubDate>Mon, 16 Jul 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/testimonial/dental-implant-tooth-extraction-anna-abbotsford-bc/</guid>
				<description><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/dental-implant-tooth-extraction-anna-abbotsford-bc/">Anna Needed an Extraction and Dental Implant</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/dental-implant-tooth-extraction-anna-abbotsford-bc/">Anna Needed an Extraction and Dental Implant</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
										</item>
		<item>
		<title>Dorothy Jean Needed a Tooth Extraction</title>
		<link>https://www.abbotsfordoralsurgery.com/testimonial/tooth-extraction-dorothy-jean-abbotsford-bc/</link>
				<pubDate>Mon, 15 Jan 2018 08:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/testimonial/tooth-extraction-dorothy-jean-abbotsford-bc/</guid>
				<description><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/tooth-extraction-dorothy-jean-abbotsford-bc/">Dorothy Jean Needed a Tooth Extraction</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/testimonial/tooth-extraction-dorothy-jean-abbotsford-bc/">Dorothy Jean Needed a Tooth Extraction</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
										</item>
	</channel>
</rss>
