<?php
$page_title       = 'Testimonials Archive - Abbotsford Oral Surgery and Dental Implant Centre';
$doc_title        = '';
$meta_description = '';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
    <body class="archive post-type-archive post-type-archive-testimonial custom-background custom-header header-image full-width-content"
          itemscope itemtype="https://schema.org/WebPage">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>

    <div class="site-inner">
    <div class="wrap">
        <div class="content-sidebar-wrap">
            <main class="content" id="genesis-content">
                <div class="archive-description cpt-archive-description"><h1 class="archive-title">Stories</h1>
                    <p>Abbotsford Oral Surgery and Dental Implant Centre in Abbotsford, BC, works hard to put our
                        patients at ease about their procedures, but we know that hearing first-hand what other
                        patients have to say about their personal experiences is often the most helpful! In our
                        Stories & Reviews section, our patients share their personal experience with their
                        treatments at our practice. One or more of these treatments may be close on the horizon for
                        you, or perhaps you are just beginning to consider a certain treatment. We hope these
                        stories will help assure you that you have chosen the best surgical team and practice for
                        your oral surgery care. </p>
                    <p>Beyond our patient stories, we also include feedback from trusted medical/dental
                        professionals who refer their patients to our practice for oral surgery. These stories may
                        help support your decision to choose our practice should you or your family members require
                        oral surgery services. </p>
                    <p>We encourage you to review the following stories and reviews to help you feel informed, and
                        therefore more relaxed, about the treatments we offer at Abbotsford Oral Surgery and Dental
                        Implant Centre.</p>
                </div>
                <article
                        class="post-99238 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-wisdom-teeth-removal-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="wisdom-teeth-removal-kristin-abbotsford-bc/index.html"
                                                  title="Kristin Needed Her Wisdom Teeth Extracted ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/kristin-wisdom-teeth-removal-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Kristin the wisdom teeth patient in Abbotsford, BC"
                                                                                title="Wisdom teeth removal in Abbotsford, BC: Kristin"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Kristin Needed Her Wisdom Teeth Extracted</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99243 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-tooth-extraction-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="tooth-extraction-rosa-abbotsford-bc/index.html"
                                                  title="Rosa Needed a Tooth Extraction ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/rosa-tooth-extraction-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Rosa the extraction patient in Abbotsford, BC"
                                                                                title="Tooth extraction in Abbotsford, BC: Rosa"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Rosa Needed a Tooth Extraction</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99245 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-dental-implants-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="dental-implants-larry-abbotsford-bc/index.html"
                                                  title="Larry Is Very Pleased With His Dental Implants ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/larry-dental-implants-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Dental implants in Abbotsford, BC: Larry | Abbotsford Oral Surgery and Dental Implant Centre"
                                                                                title="Dental implants in Abbotsford, BC: Larry | Abbotsford Oral Surgery and Dental Implant Centre"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Larry Is Very Pleased With His Dental Implants</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99248 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-wisdom-teeth-removal-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="wisdom-teeth-removal-rita-mother-abbotsford-bc/index.html"
                                                  title="Rita’s Son Had His Wisdom Teeth Removed ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/rita-mother-wisdom-teeth-removal-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Wisdom Teeth Removal in Abbotsford, BC: Rita&#039;s son | Abbotsford Oral Surgery and Dental Implant Centre"
                                                                                title="Wisdom Teeth Removal in Abbotsford, BC: Rita&#039;s son | Abbotsford Oral Surgery and Dental Implant Centre"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Rita&#8217;s Son Had His Wisdom Teeth Removed</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99063 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-facial-trauma-reviews testimonial-types-jaw-surgery-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="facial-trauma-jaw-surgery-brennan-abbotsford-bc/index.html"
                                                  title="Brennan Needed Jaw Surgery After a Facial Trauma ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/brennan-jaw-surgery-abbotsford-bc-e1538090198554-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Brennan the jaw surgery patient in Abbotsford, BC"
                                                                                title="Jaw surgery in Abbotsford, BC: Brennan"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Brennan Needed Jaw Surgery After a Facial Trauma</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99055 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-oral-pathology-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="oral-pathology-jamin-abbotsford-bc/index.html"
                                                  title="Jamin Needed an Oral Pathology Treated ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/jay-oral-pathology-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Jay the oral pathology patient in Abbotsford, BC"
                                                                                title="Oral pathology in Abbotsford, BC: Jay"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Jamin Needed an Oral Pathology Treated</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99060 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-dental-implants-reviews testimonial-types-tooth-extraction-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a
                                href="dental-implant-tooth-extraction-anna-abbotsford-bc/index.html"
                                title="Anna Needed an Extraction and Dental Implant ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/anna-dental-implants-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Anna the dental implants patient in Abbotsford, BC"
                                                                                title="Dental implants in Abbotsford, BC: Anna"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Anna Needed an Extraction and Dental Implant</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99066 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-wisdom-teeth-removal-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="wisdom-teeth-removal-chris-abbotsford-bc/index.html"
                                                  title="Chris Underwent Wisdom Teeth Removal ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/chris-wisdom-teeth-removal-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Chris the wisdom teeth patient in Abbotsford, BC"
                                                                                title="Wisdom teeth removal in Abbotsford, BC: Chris"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Chris Underwent Wisdom Teeth Removal</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99064 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-oral-pathology-reviews testimonial-types-wisdom-teeth-removal-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a
                                href="oral-pathology-wisdom-teeth-removal-brenda-abbotsford-bc/index.html"
                                title="Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/brenda-oral-pathology-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Brenda the oral pathology patient in Abbotsford, BC"
                                                                                title="Oral pathology in Abbotsford, BC: Brenda"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99057 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-bone-graft-reviews testimonial-types-dental-implants-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="dental-implants-bone-graft-donna-abbotsford-bc/index.html"
                                                  title="Bone Grafting and Dental Implants Restored Donna’s Smile ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/donna-dental-implants-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Donna the dental implants patient in Abbotsford, BC"
                                                                                title="Dental implants in Abbotsford, BC: Donna"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Bone Grafting and Dental Implants Restored Donna&#8217;s Smile</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99062 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-wisdom-teeth-removal-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="wisdom-teeth-removal-justine-abbotsford-bc/index.html"
                                                  title="Justine Needed Her Wisdom Teeth Removed ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/justine-wisdom-teeth-removal-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Justine the wisdom teeth patient in Abbotsford, BC"
                                                                                title="Wisdom teeth removal in Abbotsford, BC: Justine"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Justine Needed Her Wisdom Teeth Removed</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99058 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-oral-pathology-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="oral-pathology-aatif-abbotsford-bc/index.html"
                                                  title="Aatif’s Oral Pathology Treatment ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/aatif-oral-pathology-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Aatif the oral pathology patient in Abbotsford, BC"
                                                                                title="Oral pathology in Abbotsford, BC: Aatif"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Aatif&#8217;s Oral Pathology Treatment</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99065 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-tooth-extraction-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="tooth-extraction-dorothy-jean-abbotsford-bc/index.html"
                                                  title="Dorothy Jean Needed a Tooth Extraction ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/dorothy-jean-tooth-extraction-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Dorothy Jean the extraction patient in Abbotsford, BC"
                                                                                title="Tooth extraction in Abbotsford, BC: Dorothy Jean"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Dorothy Jean Needed a Tooth Extraction</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99056 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-facial-trauma-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="facial-trauma-marc-abbotsford-bc/index.html"
                                                  title="Marc Suffered a Facial Trauma ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/marc-facial-trauma-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Marc the facial trauma patient in Abbotsford, BC"
                                                                                title="Facial trauma in Abbotsford, BC: Marc"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Marc Suffered a Facial Trauma</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99059 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-dental-implants-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="dental-implant-guya-abbotsford-bc/index.html"
                                                  title="Guya Chose a Dental Implant to Replace Her Tooth ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/guya-dental-implants-abbotsford-bc-e1538090225803-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Guya the dental implants patient in Abbotsford, BC"
                                                                                title="Dental implants in Abbotsford, BC: Guya"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Guya Chose a Dental Implant to Replace Her Tooth</span>
                            </div>
                        </a></div>
                </article>
                <article
                        class="post-99061 testimonial type-testimonial status-publish has-post-thumbnail testimonial-types-full-arch-restoration-reviews entry col-md-4 text-center col-sm-6 no-gutters-single secondary_color">
                    <div class="entry-content"><a href="full-arch-restoration-les-abbotsford-bc/index.html"
                                                  title="Les Underwent a Full-Arch Restoration ">
                            <div style=" width: 150px;  margin: 0px auto;"><img width="150" height="150"
                                                                                src="../wp-content/uploads/les-full-arch-restoration-abbotsford-bc-150x150.jpg"
                                                                                class="img-responsive center-block img-circle wp-post-image"
                                                                                alt="Les the full-arch patient in Abbotsford, BC"
                                                                                title="Full-arch restoration in Abbotsford, BC: Les"/>
                            </div>
                            <div class="archive-pages" style="height:60px; margin-top:25px;"><span>Les Underwent a Full-Arch Restoration</span>
                            </div>
                        </a></div>
                </article>
            </main>
        </div>
		<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
        <style type="text/css">
            .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
                background-color: #1171AF !important;
                color: #fff !important;
            }

            .secondary_color {
                background-color: #46AD4C;
            }

            .highlight_color {
                background-color: #32C5F4 !important;
            }

            ;
            .site-inner {
                background-color: #1171AF !important;
            }

            /*Main Homepage*/
            .gradient {
                background: #00AEEF; /* Old browsers */
                background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
                background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
                Padding: 10%;
            }

            .home input {
                color: rgba(17, 113, 175, 1) !important;
            }

            /*body.custom-background {
			  background-color: rgba(17,113,175,1) !important;
			}*/

            /*Menu*/
            .nav-primary .sub-menu a {
                background-color: #1171AF;
                border-color: #fff;
                color: #fff !important;
            }

            .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
                background-color: #1171AF;
            }

            /*Add primary color to the number CTA*/
            .callus a {
                font-weight: bold;
                color: #1171AF !important;
            }

            /*Images*/
            .background_cta {
                background-image: url();
            }

            .home-map-image {
                background-image: url();
                background-size: cover;
                background-position: Center Center !important;
            }

            /*Testimonial Page*/
            .related_videos {
                border-top: 7px solid #46AD4C;
            }

            /*Youtube Video */
            .video_thumb {
                background-size: cover;
                background-repeat: no-repeat;
                background-position: top center;
                height: 400px;
            }

            /*Change play button color on all inline video images*/
            .content #playhover,
            .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
            .main-home #playhover,
            .page-template-hero-min-landing #playhover,
            .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
            .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
                background: url(../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
            }

            /*Add line to the botton of inline video image*/
            article #playhover + img {
                border-bottom: 5px solid #46AD4C;
            }


            /*Fancy Overlay*/
            #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
                background-image: url(../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
                background-size: cover !important;
                background-position: left top !important;
            }

            /*Archive Pages*/
            .archive-description, .author-box {
                border-top: 7px solid #46AD4C;
                border-left: 0px;
                border-right: 0px;
            }

            .archive .entry-header a {
                color: #fff;
                font-size: 1em;
            }


            /*Team Page*/
            .tiled-gallery .gallery-row, .bio-image img {
                border-bottom: 3px solid #1171AF;
            }

            /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
            .team .entry-content img, .team .gallery-item img {
                border-bottom: 5px solid #1171AF;
            }

            /*Links*/
            a {
                color: #1171AF;
                text-decoration: none !important;
            }

            /*Change current menu color*/
            /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

            /*BTNS*/
            .button {
                background-color: #46AD4C !important;
            }

            /*Add colored bullet points*/
            .entry-content ul {
                list-style-type: none;
                position: relative;
                padding-left: 0;
            }

            .entry-content ul > li {
                list-style: none;
            }

            .entry-content ul > li:before {
                content: "• \00a0 \00a0 \00a0";
                color: #1171AF; /* Color of the bullet */
                position: absolute;
                left: -1em;
                margin-right: 5px;
            }

            /*Remove bullets on feedback page*/
            .page-template-page_feedback .entry-content ul > li:before {
                content: "" !important;
            }

            /* Video Player Pages
			--------------------------------------------- */
            .fancybox-wrap:before {
                background: url(../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
                content: ".";
                position: relative;
                width: 100%;
                height: 130px;
                z-index: 8040;
                display: block;
                margin-bottom: 20px;
                background-size: contain !important;
            }

            /*Homepage - Max Width HeroCard - change line under image*/
            .page-template-hero-max-landing > a > img {
                border-bottom: 4px solid #1171AF;
            }

            /*change color on landinage page background*/
            .animated-home {
                background-color: #1171AF;
            }

            /*Homepage - Max Width HeroCard and full page content - change line under image*/
            .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
            .page-template-hero-max-landing > .row > .col-md-12 a img {
                border-bottom: 4px solid #1171AF;
                width: 100%;
            }


            /* Archive Page
			--------------------------------------------- */
            .circle-text:after {
                background-color: #32C5F4 !important;
            }

            body .entry-content .gfield_label {
                font-size: 30px !important;
                color: #1171AF;
            }

            /*Add BG image to full screen pages.  Going to remove this for now*/
            /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
				background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
				background-size: cover;
				background-position: left top !important;
			}*/

            .full-width-content.custom-background {
                background-image: url("../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
                background-attachment: fixed;
            }


            body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
            body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
                border: 1px solid #1171AF;
                background-color: #FFF8BD;
            }


            .gform_wrapper .percentbar_blue {
                background-image: url('../wp-content/themes/nuvo-express/images/opas_15.png');
                background-repeat: repeat-x;
                background-color: #1171AF;
                color: #FFF;
            }


            /* Make some mobile Tweaks
			--------------------------------------------- */
            @media only screen and (max-width: 1023px) {
                .mobile-cta {
                    color: #1171AF !important;
                }

                /*Make mobile help menu primary color*/
            }

            /* This tweaks only Small devices (tablets and phones, 768px and up) */
            @media (max-width: 900px) {
                .fancybox-youtube > .feedperson, .feedperson.shade30 {
                    border-bottom: 2px solid #32C5F4;
                }

                /*Add line under main videos on homepage / Move background face down*/
                div.col-md-12.row.no-gutters > a > div.feedperson {
                    border-bottom: 2px solid #32C5F4;
                }

                /*Add line under CTA buttons on homepage*/
            }

            /*Add custom color to the case study block on homepage*/
            .image-holder:after {
                background-color: rgba(70, 173, 76, 0.7);

            }

            /*Add video overlay on main sizzle as a dynamic layer*/
            .home-hero {
                background: rgba(17, 113, 175, 1) !important;
            }

            @media only screen and (max-width: 1024px) {

                .home-hero:after {
                    background: rgba(17, 113, 175, 0.8);
                }

            }

        </style>
    </div>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>