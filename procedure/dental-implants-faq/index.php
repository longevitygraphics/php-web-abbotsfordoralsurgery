<?php
$page_title = 'Dental Implant FAQ in Abbotsford, BC';
$doc_title = 'Dental Implant FAQ';
$meta_description = 'Get answers to the most frequently asked questions about dental implants and learn more before your upcoming treatment in Abbotsford, BC.';
$og_type = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="procedure-template-default single single-procedure postid-99085 single-format-standard custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/MedicalProcedure">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="name">Dental Implant FAQ</h1>
            </header>
            <div class="breadcrumb">You are here: <span class="breadcrumb-link-wrap" itemprop="url"><a
                            class="breadcrumb-link" href="../../index.html" itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                <span class="breadcrumb-link-wrap" itemprop="url"><a class="breadcrumb-link" href="../index.html"
                                                                     itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Procedures</span></a><meta
                            itemprop="position" content="2"></span> <span aria-label="breadcrumb separator">/</span>
                Dental Implant FAQ
            </div>
            <div class="row no-gutters">
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implant-guya-abbotsford-bc/index.html"
                       title='Guya Chose a Dental Implant to Replace Her Tooth'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/guya-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Guya the dental implants patient in Abbotsford, BC'
                             title='Guya Chose a Dental Implant to Replace Her Tooth'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implant-tooth-extraction-anna-abbotsford-bc/index.html"
                       title='Anna Needed an Extraction and Dental Implant'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/anna-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Anna the dental implants patient in Abbotsford, BC'
                             title='Anna Needed an Extraction and Dental Implant'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implants-bone-graft-donna-abbotsford-bc/index.html"
                       title='Bone Grafting and Dental Implants Restored Donna’s Smile'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/donna-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Donna the dental implants patient in Abbotsford, BC'
                             title='Bone Grafting and Dental Implants Restored Donna’s Smile'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/full-arch-restoration-les-abbotsford-bc/index.html"
                       title='Les Underwent a Full-Arch Restoration'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/les-full-arch-restoration-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Les the full-arch patient in Abbotsford, BC'
                             title='Les Underwent a Full-Arch Restoration'/>
                    </a>
                </div>
            </div>
            <div class="content-sidebar-wrap">
                <main class="content" id="genesis-content">
                    <article
                            class="post-99085 procedure type-procedure status-publish format-standard procedure-types-hidden entry secondary_color">
                        <div class="entry-content" itemprop="description"><h3>Am I a candidate for dental implants?</h3>
                            <p>If you are considering implants, Dr. Esmail will need to examine your oral health
                                thoroughly and review your medical and dental history. If your mouth is not ideal for
                                implants, ways of improving outcome, such as bone grafting, may be recommended so that
                                your implants will have a stable foundation in your jaw. See our page on <a
                                        href="../../bone-grafting/index.html">Bone Grafting</a> for more information on
                                this beneficial treatment. </p>
                            <p>To find out if dental implants are the best tooth replacement solution for you, please
                                give us a call to schedule a consultation with Dr. Esmail at Abbotsford Oral Surgery and
                                Dental Implant Centre.</p>
                            <h3>How do I care for my new dental implants?</h3>
                            <p>Once your doctor has indicated that you have healed enough, you can brush and floss your
                                dental implants just like you would your natural teeth. Be sure to see your family
                                dentist regularly to help ensure your implants last a very long time.</p>
                            <h3>How long do implants last?</h3>
                            <p>Once your implants are in place, they will serve you well for many years if you take care
                                of them and keep your mouth healthy. This means taking the time for good oral hygiene
                                (brushing and flossing) and keeping regular appointments with your dental
                                specialists. </p>
                            <h3>How long is the recovery period?</h3>
                            <p>Every patient is different, and there are many factors that contribute to post-surgical
                                healing time. Your age, healing abilities, and oral and overall health are the main
                                factors that affect the duration of your recovery period. A full recovery from dental
                                implant surgery may take between three and six months – this accounts for the time it
                                will take for the titanium implant post to integrate with the bone in your jaw. The bone
                                integration is what provides the permanent, lifelong benefit of this tooth restoration,
                                allowing the implants to become just like the roots of natural teeth. However, you
                                should be able to get back to your normal routine far before complete healing has
                                occurred, once Dr. Esmail has indicated that your healing is on schedule. Be very
                                careful when chewing hard foods around the implant site until you have been advised that
                                a full recovery has occurred. See our page on <a
                                        href="../../dental-implants-abbotsford-bc/index.html">Dental Implants</a> for
                                more information on this treatment. </p>
                            <h3>When are the replacement teeth attached to the implants?</h3>
                            <p>The replacement teeth (crowns) are attached to the implant posts once your jaw bone has
                                integrated with the implant. It may be possible to begin this phase of your treatment
                                immediately or shortly after implant placement, but this will depend on a variety of
                                factors. Dr. Esmail will carefully assess your condition to determine the best time for
                                crown placement. See our page on <a
                                        href="../../dental-implants-abbotsford-bc/index.html">Dental Implants</a> for
                                more information on this treatment. </p>
                            <h3>How much do dental implants cost?</h3>
                            <p>Dental implants may seem expensive, but they are an incredible long-term value. The exact
                                cost of your dental implant placement depends on a few factors, such as the number of
                                implants you will be receiving and if you will require bone grafting or extractions to
                                prepare for your implant surgery. Your insurance coverage will also be a factor in
                                estimating the final cost. We will assist you in estimating what your payments will be
                                once we evaluate all the factors in your case. The best way to find out what your dental
                                implants will cost is to have a consultation with Dr. Esmail where you will be provided
                                with a customized treatment plan. See our page on <a
                                        href="../../dental-implants-cost/index.html">Dental Implant Cost</a> for more
                                information.</p>
                            <h3>Are dental implants painful?</h3>
                            <p>It’s natural to have some apprehension prior to a surgical procedure, but you will be
                                very well taken care of by the team at Abbotsford Oral Surgery and Dental Implant
                                Centre. We will do everything we can to ensure you feel at ease before, during, and
                                after your procedure. Dr. Esmail is highly-trained in the administration of anesthesia
                                and sedation, and you will receive the type anesthesia that you decided upon during your
                                consultation appointment. Rest assured, you will not feel any pain or discomfort during
                                your surgery. Most patients do not experience severe or significant post-operative pain
                                and are back to their regular routines quickly. Pain medication and antibiotics will be
                                prescribed for you to help your recovery go as smoothly as possible. See our page on <a
                                        href="../../anesthesia-options/index.html">Anesthesia Options</a> for more
                                detailed information.</p>
                            <h3>Will I have temporary teeth while my implants heal?</h3>
                            <p>You have many options when it comes to receiving temporary teeth while your implants are
                                healing. Removable teeth or a temporary bridge can be made, an existing denture can be
                                modified, or you can have a new temporary denture made. If you prefer non-removable
                                teeth during the healing phase, temporary transitional crowns can be placed in some
                                patients. </p>
                            <h3>How many doctors are involved in the dental implant process?</h3>
                            <p>An oral surgeon is the dental professional that has the most experience and training in
                                the surgical procedures required to place dental implants. Your general dentist usually
                                provides the temporary and permanent replacement teeth (custom crowns). Both doctors
                                will work together to plan and implement your dental implant process, and in some cases
                                other dental specialists may help with your care. See our page on <a
                                        href="../../dental-implants-abbotsford-bc/index.html">Dental Implants</a> for
                                more information on this tooth restoration solution.</p>
                            <h3>Where can I get dental implants?</h3>
                            <p>If you are interested in dental implant treatment and live near the Abbotsford, BC, area,
                                our oral surgeon, Dr. Esmail, is available to meet with you for a consultation. Dr.
                                Esmail is a trusted dental implant specialist, offering the full scope of dental implant
                                procedures. </p>
                            <h3>Other questions?</h3>
                            <p>We are always happy to answer your questions. Feel free to contact Abbotsford Oral
                                Surgery and Dental Implant Centre at (604) 504-7522. A member of our administrative
                                staff will be pleased to assist you.</p>
                        </div>
                    </article>
                    <div class="row cta-footer ">
                        <div class="col-md-3">
                            <a href="../../contact-us/index.html">
                                <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                                </button>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <p>
                                As a patient of our office, we want it to be as easy as possible for you to visit our
                                team for care. You can request an appointment by filling out our online form. </p>
                        </div>
                    </div>
                </main>
            </div>
	        <?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
    <style type="text/css">
        .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
            background-color: #1171AF !important;
            color: #fff !important;
        }

        .secondary_color {
            background-color: #46AD4C;
        }

        .highlight_color {
            background-color: #32C5F4 !important;
        }

        ;
        .site-inner {
            background-color: #1171AF !important;
        }

        /*Main Homepage*/
        .gradient {
            background: #00AEEF; /* Old browsers */
            background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
            background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
            Padding: 10%;
        }

        .home input {
            color: rgba(17, 113, 175, 1) !important;
        }

        /*body.custom-background {
		  background-color: rgba(17,113,175,1) !important;
		}*/

        /*Menu*/
        .nav-primary .sub-menu a {
            background-color: #1171AF;
            border-color: #fff;
            color: #fff !important;
        }

        .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
            background-color: #1171AF;
        }

        /*Add primary color to the number CTA*/
        .callus a {
            font-weight: bold;
            color: #1171AF !important;
        }

        /*Images*/
        .background_cta {
            background-image: url();
        }

        .home-map-image {
            background-image: url();
            background-size: cover;
            background-position: Center Center !important;
        }

        /*Testimonial Page*/
        .related_videos {
            border-top: 7px solid #46AD4C;
        }

        /*Youtube Video */
        .video_thumb {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: top center;
            height: 400px;
        }

        /*Change play button color on all inline video images*/
        .content #playhover,
        .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
        .main-home #playhover,
        .page-template-hero-min-landing #playhover,
        .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
        .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
            background: url(../../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
        }

        /*Add line to the botton of inline video image*/
        article #playhover + img {
            border-bottom: 5px solid #46AD4C;
        }


        /*Fancy Overlay*/
        #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
            background-image: url(../../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
            background-size: cover !important;
            background-position: left top !important;
        }

        /*Archive Pages*/
        .archive-description, .author-box {
            border-top: 7px solid #46AD4C;
            border-left: 0px;
            border-right: 0px;
        }

        .archive .entry-header a {
            color: #fff;
            font-size: 1em;
        }


        /*Team Page*/
        .tiled-gallery .gallery-row, .bio-image img {
            border-bottom: 3px solid #1171AF;
        }

        /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
        .team .entry-content img, .team .gallery-item img {
            border-bottom: 5px solid #1171AF;
        }

        /*Links*/
        a {
            color: #1171AF;
            text-decoration: none !important;
        }

        /*Change current menu color*/
        /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

        /*BTNS*/
        .button {
            background-color: #46AD4C !important;
        }

        /*Add colored bullet points*/
        .entry-content ul {
            list-style-type: none;
            position: relative;
            padding-left: 0;
        }

        .entry-content ul > li {
            list-style: none;
        }

        .entry-content ul > li:before {
            content: "• \00a0 \00a0 \00a0";
            color: #1171AF; /* Color of the bullet */
            position: absolute;
            left: -1em;
            margin-right: 5px;
        }

        /*Remove bullets on feedback page*/
        .page-template-page_feedback .entry-content ul > li:before {
            content: "" !important;
        }

        /* Video Player Pages
		--------------------------------------------- */
        .fancybox-wrap:before {
            background: url(../../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
            content: ".";
            position: relative;
            width: 100%;
            height: 130px;
            z-index: 8040;
            display: block;
            margin-bottom: 20px;
            background-size: contain !important;
        }

        /*Homepage - Max Width HeroCard - change line under image*/
        .page-template-hero-max-landing > a > img {
            border-bottom: 4px solid #1171AF;
        }

        /*change color on landinage page background*/
        .animated-home {
            background-color: #1171AF;
        }

        /*Homepage - Max Width HeroCard and full page content - change line under image*/
        .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
        .page-template-hero-max-landing > .row > .col-md-12 a img {
            border-bottom: 4px solid #1171AF;
            width: 100%;
        }


        /* Archive Page
		--------------------------------------------- */
        .circle-text:after {
            background-color: #32C5F4 !important;
        }

        body .entry-content .gfield_label {
            font-size: 30px !important;
            color: #1171AF;
        }

        /*Add BG image to full screen pages.  Going to remove this for now*/
        /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
			background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
			background-size: cover;
			background-position: left top !important;
		}*/

        .full-width-content.custom-background {
            background-image: url("../../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
            background-attachment: fixed;
        }


        body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
            border: 1px solid #1171AF;
            background-color: #FFF8BD;
        }


        .gform_wrapper .percentbar_blue {
            background-image: url('../../wp-content/themes/nuvo-express/images/opas_15.png');
            background-repeat: repeat-x;
            background-color: #1171AF;
            color: #FFF;
        }




        /* Make some mobile Tweaks
		--------------------------------------------- */
        @media only screen and (max-width: 1023px) {
            .mobile-cta {
                color: #1171AF !important;
            }

            /*Make mobile help menu primary color*/
        }

        /* This tweaks only Small devices (tablets and phones, 768px and up) */
        @media (max-width: 900px) {
            .fancybox-youtube > .feedperson, .feedperson.shade30 {
                border-bottom: 2px solid #32C5F4;
            }

            /*Add line under main videos on homepage / Move background face down*/
            div.col-md-12.row.no-gutters > a > div.feedperson {
                border-bottom: 2px solid #32C5F4;
            }

            /*Add line under CTA buttons on homepage*/
        }

        /*Add custom color to the case study block on homepage*/
        .image-holder:after {
            background-color: rgba(70, 173, 76, 0.7);

        }

        /*Add video overlay on main sizzle as a dynamic layer*/
        .home-hero {
            background: rgba(17, 113, 175, 1) !important;
        }

        @media only screen and (max-width: 1024px) {

            .home-hero:after {
                background: rgba(17, 113, 175, 0.8);
            }

        }

    </style>
</div>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>