<?php
$page_title       = 'Wisdom Teeth Removal in Abbotsford, BC';
$doc_title        = 'Wisdom Teeth Removal';
$meta_description = 'Wisdom teeth are generally removed in Abbotsford, BC, to prevent them from causing infection or more serious oral health issues.';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="procedure-template-default single single-procedure postid-99068 single-format-standard custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/MedicalProcedure">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="name">Wisdom Teeth Removal</h1>
            </header>
            <div class="breadcrumb">You are here: <span class="breadcrumb-link-wrap" itemprop="url"><a
                            class="breadcrumb-link" href="../../index.html" itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                <span class="breadcrumb-link-wrap" itemprop="url"><a class="breadcrumb-link" href="../index.html"
                                                                     itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Procedures</span></a><meta
                            itemprop="position" content="2"></span> <span aria-label="breadcrumb separator">/</span>
                Wisdom Teeth Removal
            </div>
            <div class="row no-gutters">
                <div class=" col-md-12 main_header" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="http://youtu.be/1s8k99J41t4?rel=0&amp;autohide=1&amp;showinfo=0" class="fancybox-youtube"
                       title='Wisdom teeth removal in Abbotsford, BC'>
                        <span id="playhover"></span>
                        <img width="1144" height="600"
                             src="../../wp-content/uploads/wisdom-teeth-removal-procedure-1144x600.jpg"
                             class="attachment-testimonial-img size-testimonial-img wp-post-image"
                             alt="What is wisdom teeth removal in Abbotsford, BC?"/> </a>
                </div>
            </div>
            <div class="clearboth"></div>

            <div class="row no-gutters">
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/wisdom-teeth-removal-justine-abbotsford-bc/index.html"
                       title='Justine Needed Her Wisdom Teeth Removed'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/justine-wisdom-teeth-removal-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Justine the wisdom teeth patient in Abbotsford, BC'
                             title='Justine Needed Her Wisdom Teeth Removed'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/oral-pathology-wisdom-teeth-removal-brenda-abbotsford-bc/index.html"
                       title='Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/brenda-oral-pathology-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Brenda the oral pathology patient in Abbotsford, BC'
                             title='Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/wisdom-teeth-removal-chris-abbotsford-bc/index.html"
                       title='Chris Underwent Wisdom Teeth Removal'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/chris-wisdom-teeth-removal-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Chris the wisdom teeth patient in Abbotsford, BC'
                             title='Chris Underwent Wisdom Teeth Removal'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/oral-pathology-aatif-abbotsford-bc/index.html"
                       title='Aatif’s Oral Pathology Treatment'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/aatif-oral-pathology-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Aatif the oral pathology patient in Abbotsford, BC'
                             title='Aatif’s Oral Pathology Treatment'/>
                    </a>
                </div>
            </div>
            <div class="content-sidebar-wrap">
                <main class="content" id="genesis-content">
                    <article
                            class="post-99068 procedure type-procedure status-publish format-standard has-post-thumbnail entry secondary_color">
                        <div class="entry-content" itemprop="description"><p>Around the ages of 17 to 25, the third
                                molars or <strong><em>wisdom teeth</em></strong> begin to emerge into the mouth. These
                                additional molars often cause overcrowding and can develop into a host of oral health
                                problems such as impaction (a tooth that is trapped under the gums), infection, pain,
                                cysts, and more. Because these common <b><i>wisdom teeth</b></i> complications are more
                                easily treated in younger patients (or can be avoided completely), most dental
                                professionals recommend that these molars be removed early as a preventative measure.
                                Your family dentist can monitor you or your child’s wisdom teeth development with
                                regular dental checkups. When wisdom teeth removal is recommended, you will be referred
                                to a trusted specialist such as Dr. Esmail at Abbotsford Oral Surgery and Dental Implant
                                Centre. </p>
                            <h3>Wisdom Teeth Removal Process</h3>
                            <p>The beginning of the process for wisdom teeth extraction is your consultation appointment
                                with Dr. Esmail. He will perform a thorough clinical exam and, if necessary, take
                                diagnostic scans of your jaws and teeth. Dr. Esmail will evaluate the position of the
                                wisdom teeth, present you with your options for extraction, and select the type of
                                anesthesia that will maximize you/your child’s well-being during the procedure. Dr.
                                Esmail has the training, license, and experience to safely and comfortably provide
                                various types of anesthesia, and our staff is highly skilled in anesthesia techniques.
                                Services are provided in an environment of optimum safety that utilizes modern
                                monitoring equipment and is accredited and certified. At our practice in <b><i>Abbotsford,
                                        BC,</b></i> we offer several types of anesthesia for wisdom teeth removal: </p>
                            <ul>
                                <li><strong>Local anesthesia.</strong> Local anesthesia is applied directly to the
                                    surgical area. You will remain fully conscious throughout your procedure but will
                                    not feel any discomfort. Local anesthesia is used in all oral surgery procedures,
                                    often along with other forms of anesthesia.
                                </li>
                                <li><strong>Intravenous (IV) sedation.</strong> In most cases, the removal of wisdom
                                    teeth is performed under intravenous sedation, and in some cases, local anesthesia.
                                    With this type of anesthesia, you will enter a sleep-like state with no awareness of
                                    the procedure as it occurs. You must not have anything to eat or drink (excluding
                                    prescription medications with a sip of water) for at least six hours before
                                    receiving IV sedation, and a responsible adult should drive you home to recover.
                                </li>
                            </ul>
                            <p>The extraction of wisdom teeth is necessary when they are prevented from properly
                                erupting within the mouth. They may grow sideways, partially emerge from the gum, and
                                even remain trapped beneath the gum and bone. If you have not been screened for the
                                presence of wisdom teeth, it is extremely important to do so during one of your regular
                                dental checkups. Dr. Esmail has performed wisdom teeth extractions for many years. If
                                you have questions about wisdom teeth removal at our <strong><em>Abbotsford,
                                        BC,</em></strong> practice, feel free to call our front office to speak with one
                                of our knowledgeable administrators about scheduling a consultation with our oral
                                surgeon. We appreciate the opportunity to care for you at Abbotsford Oral Surgery and
                                Dental Implant Centre.</p>
                            <h3>Cost of Wisdom Teeth Removal</h3>
                            <p>Most dental professionals agree that early removal of wisdom teeth can save time and
                                money by preventing expensive and uncomfortable orthodontic or surgical treatments
                                caused by complications that can occur from <b><em>wisdom teeth</em></b>. For this
                                reason, dental professionals recommend that wisdom teeth be removed before they start
                                causing oral health problems. Early wisdom teeth removal is cost-effective for many
                                reasons:</p>
                            <ul>
                                <li><strong>Prevention of oral health issues.</strong> Dental shifting, oral infections,
                                    gum tissue disease, and damage and crowding of nearby teeth are common oral health
                                    issues caused by untreated wisdom teeth. When wisdom teeth are extracted early,
                                    these problems can be avoided.
                                </li>
                                <li><strong>Easier surgery/faster recovery.</strong> In general, when third molars are
                                    extracted at an earlier age, patients recover faster. This is because the molars are
                                    less developed and therefore easier for the oral surgeon to remove. Faster healing
                                    results in less time away from work or school.
                                </li>
                                <li><strong>Less risk for complications.</strong> When <strong><i>wisdom
                                            teeth</i></strong> are left untreated, complications such as bleeding,
                                    swelling, and discomfort can arise. These costly and painful issues are eliminated
                                    when wisdom teeth are removed early. Less treatment translates into less overall
                                    cost.
                                </li>
                            </ul>
                            <p>For wisdom teeth removal to be as accessible as possible for our patients, Abbotsford
                                Oral Surgery and Dental Implant Centre accepts several insurance plans and payment
                                methods. Our knowledgeable administrative staff will be happy to help you obtain maximum
                                insurance coverage for your treatment.<br/>
                                Learn more about the cost for wisdom teeth removal on our <a
                                        href="../../wisdom-teeth-removal-cost/index.html">Wisdom Teeth Removal Cost
                                    page.</a></p>
                            <h3>Wisdom Teeth Removal FAQ</h3>
                            <p>Please see our Wisdom Teeth Removal FAQ page <a
                                        href="../../wisdom-teeth-removal-faq/index.html">here.</a></p>
                            <h3>Impacted Wisdom Teeth</h3>
                            <p>Sometimes inadequate space in the mouth prevents teeth from erupting into the proper
                                positions. When this occurs, your teeth are impacted. Wisdom teeth are the most commonly
                                impacted teeth. When you are evaluated for impacted wisdom teeth, your care team at
                                Abbotsford Oral Surgery and Dental Implant Centre will take special diagnostic images of
                                your mouth and jaws to determine if there is enough room for your wisdom teeth to erupt
                                without problems. If any of the molars are impacted, these images will also determine
                                how difficult it will be to extract them. If any of your wisdom teeth are impacted, they
                                will usually fall into one of these categories:</p>
                            <ul>
                                <li><strong>Soft Tissue Impaction</strong> occurs when there is not enough room around a
                                    tooth for proper cleaning. This can cause decay and gum disease.
                                </li>
                                <li><strong>Partial Bony Impaction</strong> occurs when a wisdom tooth has partially
                                    erupted, but it cannot function properly in the mouth because it is positioned
                                    abnormally. Cleaning problems occur with this type of impaction as well.
                                </li>
                                <li><strong>Complete Bony Impaction</strong> occurs when there is no space for a tooth
                                    to grow into the mouth, and it remains embedded in the jaw. This type of impaction
                                    requires treatment from an oral and maxillofacial surgeon because they can be
                                    difficult to remove.
                                </li>
                            </ul>
                            <p>To protect your oral health and prevent future complications, extraction is often the
                                best treatment for impacted wisdom teeth. If you have been recommended for wisdom teeth
                                removal, or if you suspect that you or your child’s wisdom teeth are causing oral health
                                issues, please contact Abbotsford Oral Surgery and Dental Implant Centre to schedule a
                                consultation with Dr. Esmail. The procedure will be performed at our comfortable and
                                state-of-the-art practice in Abbotsford, BC. </p>
                            <p>To learn more about impacted wisdom teeth, please visit our page on <a
                                        href="../../impacted-wisdom-teeth/index.html">Impacted Wisdom Teeth.</a></p>
                            <h3>Wisdom Teeth Removal Instructions</h3>
                            <p>At Abbotsford Oral Surgery and Dental Implant Centre, our priority is to provide safe and
                                comfortable procedures with optimal outcomes. Dr. Esmail has undergone extensive
                                specialized training to provide you with excellent oral surgery care, but when it comes
                                to getting the most out of your treatment, ensuring a quick recovery, and reducing the
                                risk of complications, it is important that you follow the proper self-care guidelines
                                both before and after your oral surgery procedure. </p>
                            <p>You will receive detailed care instructions from your oral surgeon; for your convenience,
                                we also include guidelines here. To best prepare you, we provide both pre- and
                                post-operative instructions. The general pre-operative instructions will help you
                                prepare for surgery by adjusting your diet, medications, and anything else needed to
                                ensure you are completely ready. The post-operative instructions will be specific to
                                your procedure and provide instructions for your care after surgery. Always feel free to
                                call our practice with any questions you may have, including questions about pre- and
                                post-operative care. If anything is unclear to you, our knowledgeable staff is happy to
                                offer guidance.</p>
                            <p>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7397.767641803216!2d-122.31575674195159!3d49.03900507166582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5484354461678c0f%3A0xd39d9619933dd651!2sAbbotsford%20Oral%20Surgery%20and%20Dental%20Implant%20Centre%3A%20Dr.%20Nayeem%20Esmail%2C%20Inc.!5e0!3m2!1sen!2sca!4v1570469357043!5m2!1sen!2sca"
                                        width="1144" height="450" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                            </p>
                        </div>
                    </article>
                    <div class="row cta-footer ">
                        <div class="col-md-3">
                            <a href="../../contact-us/index.html">
                                <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                                </button>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <p>
                                As a patient of our office, we want it to be as easy as possible for you to visit our
                                team for care. You can request an appointment by filling out our online form. </p>
                        </div>
                    </div>
                </main>
            </div>
			<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
            <style type="text/css">
                .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
                    background-color: #1171AF !important;
                    color: #fff !important;
                }

                .secondary_color {
                    background-color: #46AD4C;
                }

                .highlight_color {
                    background-color: #32C5F4 !important;
                }

                ;
                .site-inner {
                    background-color: #1171AF !important;
                }

                /*Main Homepage*/
                .gradient {
                    background: #00AEEF; /* Old browsers */
                    background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
                    background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
                    background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
                    background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
                    background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
                    background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
                    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
                    Padding: 10%;
                }

                .home input {
                    color: rgba(17, 113, 175, 1) !important;
                }

                /*body.custom-background {
				  background-color: rgba(17,113,175,1) !important;
				}*/

                /*Menu*/
                .nav-primary .sub-menu a {
                    background-color: #1171AF;
                    border-color: #fff;
                    color: #fff !important;
                }

                .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
                    background-color: #1171AF;
                }

                /*Add primary color to the number CTA*/
                .callus a {
                    font-weight: bold;
                    color: #1171AF !important;
                }

                /*Images*/
                .background_cta {
                    background-image: url();
                }

                .home-map-image {
                    background-image: url();
                    background-size: cover;
                    background-position: Center Center !important;
                }

                /*Testimonial Page*/
                .related_videos {
                    border-top: 7px solid #46AD4C;
                }

                /*Youtube Video */
                .video_thumb {
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: top center;
                    height: 400px;
                }

                /*Change play button color on all inline video images*/
                .content #playhover,
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
                .main-home #playhover,
                .page-template-hero-min-landing #playhover,
                .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
                .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
                    background: url(../../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
                }

                /*Add line to the botton of inline video image*/
                article #playhover + img {
                    border-bottom: 5px solid #46AD4C;
                }


                /*Fancy Overlay*/
                #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
                    background-image: url(../../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
                    background-size: cover !important;
                    background-position: left top !important;
                }

                /*Archive Pages*/
                .archive-description, .author-box {
                    border-top: 7px solid #46AD4C;
                    border-left: 0px;
                    border-right: 0px;
                }

                .archive .entry-header a {
                    color: #fff;
                    font-size: 1em;
                }


                /*Team Page*/
                .tiled-gallery .gallery-row, .bio-image img {
                    border-bottom: 3px solid #1171AF;
                }

                /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
                .team .entry-content img, .team .gallery-item img {
                    border-bottom: 5px solid #1171AF;
                }

                /*Links*/
                a {
                    color: #1171AF;
                    text-decoration: none !important;
                }

                /*Change current menu color*/
                /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

                /*BTNS*/
                .button {
                    background-color: #46AD4C !important;
                }

                /*Add colored bullet points*/
                .entry-content ul {
                    list-style-type: none;
                    position: relative;
                    padding-left: 0;
                }

                .entry-content ul > li {
                    list-style: none;
                }

                .entry-content ul > li:before {
                    content: "• \00a0 \00a0 \00a0";
                    color: #1171AF; /* Color of the bullet */
                    position: absolute;
                    left: -1em;
                    margin-right: 5px;
                }

                /*Remove bullets on feedback page*/
                .page-template-page_feedback .entry-content ul > li:before {
                    content: "" !important;
                }

                /* Video Player Pages
				--------------------------------------------- */
                .fancybox-wrap:before {
                    background: url(../../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
                    content: ".";
                    position: relative;
                    width: 100%;
                    height: 130px;
                    z-index: 8040;
                    display: block;
                    margin-bottom: 20px;
                    background-size: contain !important;
                }

                /*Homepage - Max Width HeroCard - change line under image*/
                .page-template-hero-max-landing > a > img {
                    border-bottom: 4px solid #1171AF;
                }

                /*change color on landinage page background*/
                .animated-home {
                    background-color: #1171AF;
                }

                /*Homepage - Max Width HeroCard and full page content - change line under image*/
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
                .page-template-hero-max-landing > .row > .col-md-12 a img {
                    border-bottom: 4px solid #1171AF;
                    width: 100%;
                }


                /* Archive Page
				--------------------------------------------- */
                .circle-text:after {
                    background-color: #32C5F4 !important;
                }

                body .entry-content .gfield_label {
                    font-size: 30px !important;
                    color: #1171AF;
                }

                /*Add BG image to full screen pages.  Going to remove this for now*/
                /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
					background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
					background-size: cover;
					background-position: left top !important;
				}*/

                .full-width-content.custom-background {
                    background-image: url("../../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
                    background-attachment: fixed;
                }


                body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
                    border: 1px solid #1171AF;
                    background-color: #FFF8BD;
                }


                .gform_wrapper .percentbar_blue {
                    background-image: url('../../wp-content/themes/nuvo-express/images/opas_15.png');
                    background-repeat: repeat-x;
                    background-color: #1171AF;
                    color: #FFF;
                }




                /* Make some mobile Tweaks
				--------------------------------------------- */
                @media only screen and (max-width: 1023px) {
                    .mobile-cta {
                        color: #1171AF !important;
                    }

                    /*Make mobile help menu primary color*/
                }

                /* This tweaks only Small devices (tablets and phones, 768px and up) */
                @media (max-width: 900px) {
                    .fancybox-youtube > .feedperson, .feedperson.shade30 {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under main videos on homepage / Move background face down*/
                    div.col-md-12.row.no-gutters > a > div.feedperson {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under CTA buttons on homepage*/
                }

                /*Add custom color to the case study block on homepage*/
                .image-holder:after {
                    background-color: rgba(70, 173, 76, 0.7);

                }

                /*Add video overlay on main sizzle as a dynamic layer*/
                .home-hero {
                    background: rgba(17, 113, 175, 1) !important;
                }

                @media only screen and (max-width: 1024px) {

                    .home-hero:after {
                        background: rgba(17, 113, 175, 0.8);
                    }

                }

            </style>
        </div>
		<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
