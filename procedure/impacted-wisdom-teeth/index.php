<?php
$page_title       = 'Impacted Wisdom Teeth in Abbotsford, BC';
$doc_title        = 'Impacted Wisdom Teeth';
$meta_description = 'When wisdom teeth become impacted, it&#039;s important to have them removed in Abbotsford, BC, before they can damage nearby teeth or cause infection.';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="procedure-template-default single single-procedure postid-99088 single-format-standard custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/MedicalProcedure">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="name">Impacted Wisdom Teeth</h1>
            </header>
            <div class="breadcrumb">You are here: <span class="breadcrumb-link-wrap" itemprop="url"><a
                            class="breadcrumb-link" href="../../index.html" itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                <span class="breadcrumb-link-wrap" itemprop="url"><a class="breadcrumb-link" href="../index.html"
                                                                     itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Procedures</span></a><meta
                            itemprop="position" content="2"></span> <span aria-label="breadcrumb separator">/</span>
                Impacted Wisdom Teeth
            </div>
            <div class="row no-gutters">
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/oral-pathology-wisdom-teeth-removal-brenda-abbotsford-bc/index.html"
                       title='Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/brenda-oral-pathology-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Brenda the oral pathology patient in Abbotsford, BC'
                             title='Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/wisdom-teeth-removal-chris-abbotsford-bc/index.html"
                       title='Chris Underwent Wisdom Teeth Removal'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/chris-wisdom-teeth-removal-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Chris the wisdom teeth patient in Abbotsford, BC'
                             title='Chris Underwent Wisdom Teeth Removal'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/wisdom-teeth-removal-justine-abbotsford-bc/index.html"
                       title='Justine Needed Her Wisdom Teeth Removed'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/justine-wisdom-teeth-removal-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Justine the wisdom teeth patient in Abbotsford, BC'
                             title='Justine Needed Her Wisdom Teeth Removed'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/oral-pathology-aatif-abbotsford-bc/index.html"
                       title='Aatif’s Oral Pathology Treatment'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/aatif-oral-pathology-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Aatif the oral pathology patient in Abbotsford, BC'
                             title='Aatif’s Oral Pathology Treatment'/>
                    </a>
                </div>
            </div>
            <div class="content-sidebar-wrap">
                <main class="content" id="genesis-content">
                    <article
                            class="post-99088 procedure type-procedure status-publish format-standard procedure-types-hidden entry secondary_color">
                        <div class="entry-content" itemprop="description"><p>When teeth are unable to erupt into the
                                proper position for chewing and cleaning, they are impacted. Impacted teeth can take
                                many positions in the bone as they attempt to find their way into the dental arch. The
                                most commonly impacted teeth are wisdom teeth because the average human jaw is not large
                                enough to accommodate them. When wisdom teeth become impacted, the pressure under the
                                gums can cause pain and even damage the nearby teeth. Impacted wisdom teeth are also
                                prone to infections and other problems. To protect your oral health and prevent future
                                complications, extraction is often the best treatment for impacted wisdom teeth. </p>
                            <p>When you are evaluated for impacted wisdom teeth, your care team at Abbotsford Oral
                                Surgery and Dental Implant Centre will take state-of-the-art diagnostic images of your
                                mouth and jaws to determine if there is enough room for your wisdom teeth to erupt
                                without problems. If any of the molars are impacted, these images will also determine
                                how difficult it will be to extract them. If you or your child have been recommended for
                                wisdom teeth removal, or if you suspect that you or your child’s wisdom teeth are
                                causing oral health issues, please contact Abbotsford Oral Surgery and Dental Implant
                                Centre to schedule a consultation with Dr. Esmail. </p>
                            <h2>Symptoms of Impacted Wisdom Teeth</h2>
                            <p>You may have an impacted wisdom tooth and not notice any symptoms, but if impacted wisdom
                                teeth begin to shift or become infected, you may notice some of the following:</p>
                            <ul>
                                <li>Swelling of the gum tissue near your back molars</li>
                                <li>Pain when opening the mouth</li>
                                <li>Pain, pressure, or sensitivity around the back molars</li>
                                <li>Persistent bad breath/unpleasant taste in the mouth</li>
                            </ul>
                            <p>It is important that you/your child see your family dentist regularly for dental exams
                                and imaging so that the wisdom teeth can be tracked as they develop. It is highly
                                recommended that wisdom teeth be addressed before they start to emerge or cause oral
                                health problems such as impaction.</p>
                            <h2>Impacted Wisdom Teeth in Abbotsford, BC</h2>
                            <p>If you or your child have been recommended for wisdom teeth removal or you think that
                                your/your child’s wisdom teeth may be impacted, please contact Abbotsford Oral Surgery
                                and Dental Implant Centre to schedule a consultation with Dr. Esmail where you will
                                collaborate on a detailed treatment plan for surgery. The procedure will be performed at
                                our practice in Abbotsford, BC, where our surgical team is highly experienced in the
                                extraction of wisdom teeth. We look forward to offering you and your family the very
                                best in oral surgery care.</p>
                        </div>
                    </article>
                    <div class="row cta-footer ">
                        <div class="col-md-3">
                            <a href="../../contact-us/index.html">
                                <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                                </button>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <p>
                                As a patient of our office, we want it to be as easy as possible for you to visit our
                                team for care. You can request an appointment by filling out our online form. </p>
                        </div>
                    </div>
                </main>
            </div>
			<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
            <style type="text/css">
                .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
                    background-color: #1171AF !important;
                    color: #fff !important;
                }

                .secondary_color {
                    background-color: #46AD4C;
                }

                .highlight_color {
                    background-color: #32C5F4 !important;
                }

                ;
                .site-inner {
                    background-color: #1171AF !important;
                }

                /*Main Homepage*/
                .gradient {
                    background: #00AEEF; /* Old browsers */
                    background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
                    background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
                    background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
                    background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
                    background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
                    background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
                    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
                    Padding: 10%;
                }

                .home input {
                    color: rgba(17, 113, 175, 1) !important;
                }

                /*body.custom-background {
				  background-color: rgba(17,113,175,1) !important;
				}*/

                /*Menu*/
                .nav-primary .sub-menu a {
                    background-color: #1171AF;
                    border-color: #fff;
                    color: #fff !important;
                }

                .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
                    background-color: #1171AF;
                }

                /*Add primary color to the number CTA*/
                .callus a {
                    font-weight: bold;
                    color: #1171AF !important;
                }

                /*Images*/
                .background_cta {
                    background-image: url();
                }

                .home-map-image {
                    background-image: url();
                    background-size: cover;
                    background-position: Center Center !important;
                }

                /*Testimonial Page*/
                .related_videos {
                    border-top: 7px solid #46AD4C;
                }

                /*Youtube Video */
                .video_thumb {
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: top center;
                    height: 400px;
                }

                /*Change play button color on all inline video images*/
                .content #playhover,
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
                .main-home #playhover,
                .page-template-hero-min-landing #playhover,
                .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
                .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
                    background: url(../../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
                }

                /*Add line to the botton of inline video image*/
                article #playhover + img {
                    border-bottom: 5px solid #46AD4C;
                }


                /*Fancy Overlay*/
                #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
                    background-image: url(../../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
                    background-size: cover !important;
                    background-position: left top !important;
                }

                /*Archive Pages*/
                .archive-description, .author-box {
                    border-top: 7px solid #46AD4C;
                    border-left: 0px;
                    border-right: 0px;
                }

                .archive .entry-header a {
                    color: #fff;
                    font-size: 1em;
                }


                /*Team Page*/
                .tiled-gallery .gallery-row, .bio-image img {
                    border-bottom: 3px solid #1171AF;
                }

                /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
                .team .entry-content img, .team .gallery-item img {
                    border-bottom: 5px solid #1171AF;
                }

                /*Links*/
                a {
                    color: #1171AF;
                    text-decoration: none !important;
                }

                /*Change current menu color*/
                /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

                /*BTNS*/
                .button {
                    background-color: #46AD4C !important;
                }

                /*Add colored bullet points*/
                .entry-content ul {
                    list-style-type: none;
                    position: relative;
                    padding-left: 0;
                }

                .entry-content ul > li {
                    list-style: none;
                }

                .entry-content ul > li:before {
                    content: "• \00a0 \00a0 \00a0";
                    color: #1171AF; /* Color of the bullet */
                    position: absolute;
                    left: -1em;
                    margin-right: 5px;
                }

                /*Remove bullets on feedback page*/
                .page-template-page_feedback .entry-content ul > li:before {
                    content: "" !important;
                }

                /* Video Player Pages
				--------------------------------------------- */
                .fancybox-wrap:before {
                    background: url(../../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
                    content: ".";
                    position: relative;
                    width: 100%;
                    height: 130px;
                    z-index: 8040;
                    display: block;
                    margin-bottom: 20px;
                    background-size: contain !important;
                }

                /*Homepage - Max Width HeroCard - change line under image*/
                .page-template-hero-max-landing > a > img {
                    border-bottom: 4px solid #1171AF;
                }

                /*change color on landinage page background*/
                .animated-home {
                    background-color: #1171AF;
                }

                /*Homepage - Max Width HeroCard and full page content - change line under image*/
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
                .page-template-hero-max-landing > .row > .col-md-12 a img {
                    border-bottom: 4px solid #1171AF;
                    width: 100%;
                }


                /* Archive Page
				--------------------------------------------- */
                .circle-text:after {
                    background-color: #32C5F4 !important;
                }

                body .entry-content .gfield_label {
                    font-size: 30px !important;
                    color: #1171AF;
                }

                /*Add BG image to full screen pages.  Going to remove this for now*/
                /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
					background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
					background-size: cover;
					background-position: left top !important;
				}*/

                .full-width-content.custom-background {
                    background-image: url("../../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
                    background-attachment: fixed;
                }


                body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
                    border: 1px solid #1171AF;
                    background-color: #FFF8BD;
                }


                .gform_wrapper .percentbar_blue {
                    background-image: url('../../wp-content/themes/nuvo-express/images/opas_15.png');
                    background-repeat: repeat-x;
                    background-color: #1171AF;
                    color: #FFF;
                }




                /* Make some mobile Tweaks
				--------------------------------------------- */
                @media only screen and (max-width: 1023px) {
                    .mobile-cta {
                        color: #1171AF !important;
                    }

                    /*Make mobile help menu primary color*/
                }

                /* This tweaks only Small devices (tablets and phones, 768px and up) */
                @media (max-width: 900px) {
                    .fancybox-youtube > .feedperson, .feedperson.shade30 {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under main videos on homepage / Move background face down*/
                    div.col-md-12.row.no-gutters > a > div.feedperson {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under CTA buttons on homepage*/
                }

                /*Add custom color to the case study block on homepage*/
                .image-holder:after {
                    background-color: rgba(70, 173, 76, 0.7);

                }

                /*Add video overlay on main sizzle as a dynamic layer*/
                .home-hero {
                    background: rgba(17, 113, 175, 1) !important;
                }

                @media only screen and (max-width: 1024px) {

                    .home-hero:after {
                        background: rgba(17, 113, 175, 0.8);
                    }

                }

            </style>
        </div>
		<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
