<?php
$page_title = 'Oral Pathology in Abbotsford, BC';
$doc_title = 'Oral Pathology';
$meta_description = 'Oral pathology refers to diseases that can develop inside your mouth, salivary glands, or jaws. Immediate treatment is recommended in Abbotsford, BC.';
$og_type = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="procedure-template-default single single-procedure postid-99073 single-format-standard custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/MedicalProcedure">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="name">Oral Pathology</h1>
            </header>
            <div class="breadcrumb">You are here: <span class="breadcrumb-link-wrap" itemprop="url"><a
                            class="breadcrumb-link" href="../../index.html" itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                <span class="breadcrumb-link-wrap" itemprop="url"><a class="breadcrumb-link" href="../index.html"
                                                                     itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Procedures</span></a><meta
                            itemprop="position" content="2"></span> <span aria-label="breadcrumb separator">/</span>
                Oral Pathology
            </div>
            <div class="row no-gutters">
                <div class=" col-md-12 main_header" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="http://youtu.be/p224DSbodew?rel=0&amp;autohide=1&amp;showinfo=0" class="fancybox-youtube"
                       title='Oral pathology in Abbotsford, BC'>
                        <span id="playhover"></span>
                        <img width="1144" height="600"
                             src="../../wp-content/uploads/oral-pathology-procedure-1144x600.jpg"
                             class="attachment-testimonial-img size-testimonial-img wp-post-image"
                             alt="What is the treatment for oral pathology in Abbotsford, BC?"/> </a>
                </div>
            </div>
            <div class="clearboth"></div>

            <div class="row no-gutters">
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/oral-pathology-jamin-abbotsford-bc/index.html"
                       title='Jamin Needed an Oral Pathology Treated'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img" src="../../wp-content/uploads/jay-oral-pathology-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Jay the oral pathology patient in Abbotsford, BC'
                             title='Jamin Needed an Oral Pathology Treated'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/oral-pathology-aatif-abbotsford-bc/index.html"
                       title='Aatif’s Oral Pathology Treatment'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/aatif-oral-pathology-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Aatif the oral pathology patient in Abbotsford, BC'
                             title='Aatif’s Oral Pathology Treatment'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/oral-pathology-wisdom-teeth-removal-brenda-abbotsford-bc/index.html"
                       title='Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/brenda-oral-pathology-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Brenda the oral pathology patient in Abbotsford, BC'
                             title='Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/wisdom-teeth-removal-chris-abbotsford-bc/index.html"
                       title='Chris Underwent Wisdom Teeth Removal'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/chris-wisdom-teeth-removal-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Chris the wisdom teeth patient in Abbotsford, BC'
                             title='Chris Underwent Wisdom Teeth Removal'/>
                    </a>
                </div>
            </div>
            <div class="content-sidebar-wrap">
                <main class="content" id="genesis-content">
                    <article
                            class="post-99073 procedure type-procedure status-publish format-standard has-post-thumbnail entry secondary_color">
                        <div class="entry-content" itemprop="description"><p><strong>Oral pathology</strong> refers to
                                diseases that can develop inside your mouth, salivary glands, or jaws. The inside of the
                                mouth is lined with a special type of skin called the mucosa that should be smooth and
                                coral pink in color. Any change in the appearance of the mucosa could be a warning sign
                                for a pathological process. When oral pathologies or diseases are caught early, they are
                                much more easily treated, so it’s important to be aware of the condition of your mouth.
                                We recommend performing an oral self-examination monthly. Although most oral pathologies
                                are benign, it is always important to get proper evaluation and treatment of any oral
                                abnormality or lesion by an expert. Even if an oral lesion is not painful, it should
                                still be evaluated and properly treated. Oral and maxillofacial surgeons are the most
                                qualified medical providers to diagnose and treat oral pathologies. </p>
                            <p>Common oral pathologies include the following:</p>
                            <ul>
                                <li>Ulcers</li>
                                <li>Tonsil stones</li>
                                <li>Salivary gland diseases</li>
                                <li>Herpes (cold sores)</li>
                                <li>Canker sores</li>
                                <li>Cysts and tumors</li>
                                <li>Infections</li>
                                <li>Oral cancers</li>
                            </ul>
                            <p>If your oral surgeon suspects that you are suffering from an oral pathology, you may have
                                a biopsy during your examination. A biopsy is a small sample of tissue that is removed
                                and sent to a lab to confirm a diagnosis. Most biopsies can be performed in our practice
                                with local anesthesia. The appropriate next steps will depend on the diagnosis. Dr.
                                Esmail will discuss his recommendations with you to personalize a treatment plan for the
                                restoration of your oral health. </p>
                            <h2>Signs of Oral and Maxillofacial Diseases</h2>
                            <p>If you notice any abnormality in your mouth, including the following, call our practice
                                right away for a pathology screening:</p>
                            <ul>
                                <li>Red or white patches in the mouth</li>
                                <li>A sore that fails to heal and bleeds easily</li>
                                <li>A lump or thickening on the skin lining the inside of the mouth</li>
                                <li>Chronic sore throat or hoarseness</li>
                                <li>Difficulty chewing or swallowing</li>
                            </ul>
                            <p>Oral diseases range from simple canker sores to more serious issues such as oral cancer.
                                It is important to note that oral pain does not always occur with a pathological
                                process, particularly in the case of oral cancer. </p>
                            <h2>Oral Pathology in Abbotsford, BC</h2>
                            <p>Most changes in your mucosa and oral cavity will not be serious, but any unusual changes
                                should be examined by your oral surgeon promptly. Early identification and treatment of
                                pathologies are important. Your overall health is our priority, so please contact
                                Abbotsford Oral Surgery and Dental Implant Centre as soon as possible if you suspect an
                                oral pathology. We will make every effort to see you promptly for an evaluation of the
                                area and a recommendation for next steps.</p>
                        </div>
                    </article>
                    <div class="row cta-footer ">
                        <div class="col-md-3">
                            <a href="../../contact-us/index.html">
                                <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                                </button>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <p>
                                As a patient of our office, we want it to be as easy as possible for you to visit our
                                team for care. You can request an appointment by filling out our online form. </p>
                        </div>
                    </div>
                </main>
            </div>
	        <?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
    <style type="text/css">
        .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
            background-color: #1171AF !important;
            color: #fff !important;
        }

        .secondary_color {
            background-color: #46AD4C;
        }

        .highlight_color {
            background-color: #32C5F4 !important;
        }

        ;
        .site-inner {
            background-color: #1171AF !important;
        }

        /*Main Homepage*/
        .gradient {
            background: #00AEEF; /* Old browsers */
            background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
            background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
            Padding: 10%;
        }

        .home input {
            color: rgba(17, 113, 175, 1) !important;
        }

        /*body.custom-background {
		  background-color: rgba(17,113,175,1) !important;
		}*/

        /*Menu*/
        .nav-primary .sub-menu a {
            background-color: #1171AF;
            border-color: #fff;
            color: #fff !important;
        }

        .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
            background-color: #1171AF;
        }

        /*Add primary color to the number CTA*/
        .callus a {
            font-weight: bold;
            color: #1171AF !important;
        }

        /*Images*/
        .background_cta {
            background-image: url();
        }

        .home-map-image {
            background-image: url();
            background-size: cover;
            background-position: Center Center !important;
        }

        /*Testimonial Page*/
        .related_videos {
            border-top: 7px solid #46AD4C;
        }

        /*Youtube Video */
        .video_thumb {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: top center;
            height: 400px;
        }

        /*Change play button color on all inline video images*/
        .content #playhover,
        .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
        .main-home #playhover,
        .page-template-hero-min-landing #playhover,
        .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
        .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
            background: url(../../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
        }

        /*Add line to the botton of inline video image*/
        article #playhover + img {
            border-bottom: 5px solid #46AD4C;
        }


        /*Fancy Overlay*/
        #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
            background-image: url(../../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
            background-size: cover !important;
            background-position: left top !important;
        }

        /*Archive Pages*/
        .archive-description, .author-box {
            border-top: 7px solid #46AD4C;
            border-left: 0px;
            border-right: 0px;
        }

        .archive .entry-header a {
            color: #fff;
            font-size: 1em;
        }


        /*Team Page*/
        .tiled-gallery .gallery-row, .bio-image img {
            border-bottom: 3px solid #1171AF;
        }

        /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
        .team .entry-content img, .team .gallery-item img {
            border-bottom: 5px solid #1171AF;
        }

        /*Links*/
        a {
            color: #1171AF;
            text-decoration: none !important;
        }

        /*Change current menu color*/
        /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

        /*BTNS*/
        .button {
            background-color: #46AD4C !important;
        }

        /*Add colored bullet points*/
        .entry-content ul {
            list-style-type: none;
            position: relative;
            padding-left: 0;
        }

        .entry-content ul > li {
            list-style: none;
        }

        .entry-content ul > li:before {
            content: "• \00a0 \00a0 \00a0";
            color: #1171AF; /* Color of the bullet */
            position: absolute;
            left: -1em;
            margin-right: 5px;
        }

        /*Remove bullets on feedback page*/
        .page-template-page_feedback .entry-content ul > li:before {
            content: "" !important;
        }

        /* Video Player Pages
		--------------------------------------------- */
        .fancybox-wrap:before {
            background: url(../../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
            content: ".";
            position: relative;
            width: 100%;
            height: 130px;
            z-index: 8040;
            display: block;
            margin-bottom: 20px;
            background-size: contain !important;
        }

        /*Homepage - Max Width HeroCard - change line under image*/
        .page-template-hero-max-landing > a > img {
            border-bottom: 4px solid #1171AF;
        }

        /*change color on landinage page background*/
        .animated-home {
            background-color: #1171AF;
        }

        /*Homepage - Max Width HeroCard and full page content - change line under image*/
        .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
        .page-template-hero-max-landing > .row > .col-md-12 a img {
            border-bottom: 4px solid #1171AF;
            width: 100%;
        }


        /* Archive Page
		--------------------------------------------- */
        .circle-text:after {
            background-color: #32C5F4 !important;
        }

        body .entry-content .gfield_label {
            font-size: 30px !important;
            color: #1171AF;
        }

        /*Add BG image to full screen pages.  Going to remove this for now*/
        /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
			background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
			background-size: cover;
			background-position: left top !important;
		}*/

        .full-width-content.custom-background {
            background-image: url("../../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
            background-attachment: fixed;
        }


        body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
        body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
            border: 1px solid #1171AF;
            background-color: #FFF8BD;
        }


        .gform_wrapper .percentbar_blue {
            background-image: url('../../wp-content/themes/nuvo-express/images/opas_15.png');
            background-repeat: repeat-x;
            background-color: #1171AF;
            color: #FFF;
        }




        /* Make some mobile Tweaks
		--------------------------------------------- */
        @media only screen and (max-width: 1023px) {
            .mobile-cta {
                color: #1171AF !important;
            }

            /*Make mobile help menu primary color*/
        }

        /* This tweaks only Small devices (tablets and phones, 768px and up) */
        @media (max-width: 900px) {
            .fancybox-youtube > .feedperson, .feedperson.shade30 {
                border-bottom: 2px solid #32C5F4;
            }

            /*Add line under main videos on homepage / Move background face down*/
            div.col-md-12.row.no-gutters > a > div.feedperson {
                border-bottom: 2px solid #32C5F4;
            }

            /*Add line under CTA buttons on homepage*/
        }

        /*Add custom color to the case study block on homepage*/
        .image-holder:after {
            background-color: rgba(70, 173, 76, 0.7);

        }

        /*Add video overlay on main sizzle as a dynamic layer*/
        .home-hero {
            background: rgba(17, 113, 175, 1) !important;
        }

        @media only screen and (max-width: 1024px) {

            .home-hero:after {
                background: rgba(17, 113, 175, 0.8);
            }

        }

    </style>
</div>
	    <?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
