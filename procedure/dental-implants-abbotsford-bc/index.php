<?php
$page_title       = 'Dental Implants in Abbotsford, BC';
$doc_title        = 'Dental Implants';
$meta_description = 'Dental implants in Abbotsford, BC, are a highly functional and long-term tooth restoration solution for broken, failing, or missing teeth.';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="procedure-template-default single single-procedure postid-99067 single-format-standard custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/MedicalProcedure">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="name">Dental Implants</h1>
            </header>
            <div class="breadcrumb">You are here: <span class="breadcrumb-link-wrap" itemprop="url"><a
                            class="breadcrumb-link" href="../../index.html" itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                <span class="breadcrumb-link-wrap" itemprop="url"><a class="breadcrumb-link" href="../index.html"
                                                                     itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Procedures</span></a><meta
                            itemprop="position" content="2"></span> <span aria-label="breadcrumb separator">/</span>
                Dental Implants
            </div>
            <div class="row no-gutters">
                <div class=" col-md-12 main_header" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="https://youtu.be/VULhCR6ItkY&rel=0&amp=&autohide=1&amp=&showinfo=0"
                       class="fancybox-youtube" title='Dental implants in Abbotsford, BC'>
                        <span id="playhover"></span>
                        <img width="1144" height="600"
                             src="../../wp-content/uploads/dental-implant-procedure-1144x600.jpg"
                             class="attachment-testimonial-img size-testimonial-img wp-post-image"
                             alt="What are dental implants in Abbotsford, BC?"</a>
                </div>
            </div>
            <div class="clearboth"></div>

            <div class="row no-gutters">
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implants-bone-graft-donna-abbotsford-bc/index.html"
                       title='Bone Grafting and Dental Implants Restored Donna’s Smile'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/donna-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Donna the dental implants patient in Abbotsford, BC'
                             title='Bone Grafting and Dental Implants Restored Donna’s Smile'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implant-guya-abbotsford-bc/index.html"
                       title='Guya Chose a Dental Implant to Replace Her Tooth'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/guya-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Guya the dental implants patient in Abbotsford, BC'
                             title='Guya Chose a Dental Implant to Replace Her Tooth'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implant-tooth-extraction-anna-abbotsford-bc/index.html"
                       title='Anna Needed an Extraction and Dental Implant'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/anna-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Anna the dental implants patient in Abbotsford, BC'
                             title='Anna Needed an Extraction and Dental Implant'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/full-arch-restoration-les-abbotsford-bc/index.html"
                       title='Les Underwent a Full-Arch Restoration'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/les-full-arch-restoration-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Les the full-arch patient in Abbotsford, BC'
                             title='Les Underwent a Full-Arch Restoration'/>
                    </a>
                </div>
            </div>
            <div class="content-sidebar-wrap">
                <main class="content" id="genesis-content">
                    <article
                            class="post-99067 procedure type-procedure status-publish format-standard has-post-thumbnail entry secondary_color">
                        <div class="entry-content" itemprop="description"><p>When patients visit Abbotsford Oral Surgery
                                and Dental Implant Centre to replace their broken, failing, or missing teeth, they have
                                the option to choose <b><em>dental implants</em></b> for a highly functional and
                                long-term tooth (or teeth) restoration solution. <i>Dental implants</i> truly change
                                lives. For people who are living with tooth loss, the emotional consequences can be high
                                — implants provide these patients with a secure foundation for replacement teeth that
                                look, feel, and function just like natural teeth. <strong>Dental implants</strong> are
                                surgically placed titanium posts that inserted into the jaw bone where teeth are
                                missing. Implants can support individual replacement teeth, or secure other types of
                                tooth restoration such as a full-arch replacement to replace an entire arch of teeth.
                                Unlike bridges, no healthy teeth are damaged to secure the restoration, and implants can
                                last a lifetime if proper care is taken. Because an implant fuses with the natural bone
                                in the jaw just like a natural tooth root, you can regain the ability to eat virtually
                                anything and smile with confidence knowing that your replacement teeth appear natural.
                                Implants also keep the jaw healthy and strong, preventing bone deterioration by
                                stimulating the bone in your jaw just like natural teeth.</p>
                            <h3>Benefits of Dental Implants</h3>
                            <p>To restore a missing tooth, oral surgeons can surgically place a dental implant made of
                                titanium that securely anchors a custom crown for a complete, healthy tooth replacement.
                                With countless incredible benefits, it is easy to see why this dental restoration
                                solution is a favorite with our Abbotsford Oral Surgery and Dental Implant Centre
                                patients. These are a few of the benefits of this transformative tooth restoration
                                treatment:</p>
                            <ul>
                                <li style="list-style-type: none">
                                <li>Durability and versatility — with proper care, dental implants can last a
                                    lifetime.
                                </li>
                                <li>Implants look, feel, and function like your natural teeth.</li>
                                <li>Improved self-confidence.</li>
                                <li>Eat your favorite foods again.</li>
                                <li>Talking, smiling, and eating with confidence again.</li>
                                <li>Dental implants replace the entire tooth, including the root, so they work to
                                    preserve your long-term oral health.
                                </li>
                                <li>Most patients are dental implant candidates.</li>
                            </ul>
                            <p>. . . and so much more!</p>
                            <p>We find that many patients are candidates for receiving dental implants — especially with
                                the advent of advanced bone grafting techniques that open dental implant treatment to
                                patients who have suffered bone loss due to long-term tooth loss, or other factors.
                                Typically, if you are healthy enough to undergo a tooth extraction, dental implants are
                                an option for you.</p>
                            <p>If you are looking to restore your oral function with dental implants, or have any
                                further questions about our procedures, please contact us to schedule a consultation
                                with Dr. Esmail at our practice in <b><em>Abbotsford, BC</em></b>. We appreciate the
                                opportunity to care for you at Abbotsford Oral Surgery and Dental Implant Centre.</p>
                            <h3>Dental Implant Procedure</h3>
                            <p>Dr. Esmail at Abbotsford Oral Surgery and Dental Implant Centre has been extensively
                                trained in dental implant placement through surgical residency, continuing education,
                                and by working with patients in this practice over the years. His training and
                                experience have made him highly qualified to perform implant placement with optimal
                                long-term results.</p>
                            <p>Dental Implant placement is a team effort between an oral and maxillofacial surgeon and a
                                restorative dentist. While Dr. Esmail performs the actual implant surgery, initial tooth
                                extractions, and bone grafting (if necessary), the restorative dentist (your family
                                dentist) designs and creates the permanent prosthesis. Your dentist will also make any
                                temporary prosthesis needed during the implant process. To fabricate the prosthetic
                                tooth or teeth, a digital 3D impression is taken of your mouth and a model is created.
                                The replacement teeth are based on this model and can be crowns, fixed bridges, or
                                removable/fixed dentures.</p>
                            <p>Most dental implants and bone graft procedures can be performed at Abbotsford Oral
                                Surgery and Dental Implant Centre under intravenous (conscious) sedation. Dr. Esmail is
                                highly trained in the administration of in-office anesthesia. To learn more about our
                                anesthesia options and procedures, please review our <a
                                        href="../../anesthesia-options/index.html">Anesthesia Options</a> page so that
                                you will be prepared to discuss your anesthesia preferences with the surgeon during your
                                consultation. The dental implant procedure typically goes as follows:</p>
                            <ul>
                                <li><strong>Consultation.</strong> The process of receiving dental implants begins on
                                    the day of your consultation with Dr. Esmail. During this appointment with your oral
                                    surgeon, your condition will be assessed, and you will work together to form a
                                    treatment plan. 3D scans may be taken to create detailed surgical guides for your
                                    procedure.
                                </li>
                                <li><strong>Surgery.</strong> You will have chosen your anesthesia preference with the
                                    doctor at the consultation, and it will be administered before your procedure. You
                                    will not feel any discomfort during surgery. Dr. Esmail will make a small incision
                                    in your gum tissue where the implant is to be placed. Next, he will insert the
                                    titanium implant post into the exposed jaw bone. After the procedure, you will
                                    receive stitches at the site of your surgery and will then be given time to rest
                                    before your designated driver takes you home. The surgical procedure for implant
                                    placement takes 30 to 60 minutes for one implant, and about 2 to 3 hours for
                                    multiple implants.
                                </li>
                                <li><strong>Recovery.</strong> After your surgery, you will return home to begin the
                                    healing process. You may have been outfitted with a temporary crown to wear during
                                    this time. We have found that many of our patients are back to their regular
                                    routines after just a couple of weeks; however, the fusion of the implant with the
                                    jaw bone will take a bit longer. This amount of healing time varies depending on the
                                    quality and quantity of bone in the jaw as well as a few other factors. Your doctor
                                    will discuss all of this with you so that you are fully informed before your
                                    procedure takes place. Follow-up care appointments with Dr. Esmail are often
                                    scheduled to ensure that you are healing well.
                                </li>
                                <li><strong>Abutment Placement.</strong> After the initial phase of healing, the surgeon
                                    will place an abutment (support post) onto your titanium implant during one of your
                                    follow-up visits. The abutment is the piece that will connect your new crown to your
                                    implant.
                                </li>
                                <li><strong>Crown placement.</strong> Once Dr. Esmail has determined that you are ready
                                    for the final phase of your treatment — the placement of your permanent tailor-made
                                    new crown — your restorative dentist will fit the crown to the dental implant. Get
                                    ready to smile!
                                </li>
                            </ul>
                            <p>Your new dental implants should last a lifetime if you keep your mouth clean and healthy
                                and maintain regular appointments with your dental specialists. Dr. Esmail is available
                                to meet with you for a consultation appointment, where he will work closely with you to
                                develop your treatment plan and restore your smile. Please call the front office to
                                learn more about us, and about the dental implant procedures we perform at our friendly
                                practice in <strong><i>Abbotsford, BC</i></strong>.</p>
                            <h3>Dental Implant Cost</h3>
                            <p>We will make every effort to provide you with the most accurate estimate of the expenses
                                involved in your dental implant treatment. The exact cost of your dental implants will
                                depend on a few factors, such as the number of implants you will be receiving, and if
                                you will require bone grafting or extractions to prepare for your implant surgery. Your
                                insurance coverage will also be a factor in estimating your cost. You will be charged
                                separately for the services of the dentist providing your custom crowns. The best way to
                                estimate your dental implant cost is to have a consultation with Dr. Esmail, where all
                                of these factors can be evaluated.</p>
                            <p>While dental implants can seem like an expensive tooth replacement option, it’s hard to
                                put a price on something as life-changing as replacement teeth that look, feel, and
                                function like natural teeth. Regaining the ability to eat the foods you love knowing
                                that your teeth appear natural and that the health of your jaw bone is being maintained
                                as with natural teeth, has lifelong value for your budget and your health. Dental
                                implants can last decades if cared for well, unlike other more traditional tooth
                                restorations that must be replaced periodically. Some of the financial and personal
                                advantages our implant patients enjoy include the following:</p>
                            <ul>
                                <li><strong>A lifelong tooth replacement.</strong> Unlike bridges and dentures, dental
                                    implants do not require replacement after a few years. With proper oral hygiene and
                                    regular dental appointments, dental implants can last a lifetime.
                                </li>
                                <li><strong>Better oral health.</strong> Because dental implants are embedded in your
                                    jaw bone through osseointegration, they stimulate jaw bone growth, just like the
                                    roots of natural teeth. This stimulation helps to prevent the unsightly and
                                    destructive bone loss that is caused by prolonged tooth loss.
                                </li>
                                <li><strong>Improved quality of life.</strong> With this restoration, patients can eat
                                    the foods they love and speak, eat, and smile with confidence. Dental implants can
                                    be a doorway to renewed self-confidence and peace of mind. This type of long-lasting
                                    oral rehabilitation is priceless.
                                </li>
                            </ul>
                            <p>Dental implants truly change lives for the better! If you are interested in learning more
                                about this long-lasting, cost-effective, and healthy way to replace damaged or missing
                                teeth, please give Abbotsford Oral Surgery and Dental Implant Centre a call.</p>
                            <p>For more on the cost and value of dental implants at our trusted practice in <strong>Abbotsford,
                                    BC</strong>, see our <a href="../../dental-implants-cost/index.html">Dental Implant
                                    Cost page.</a></p>
                            <h3>Dental Implant FAQ</h3>
                            <p>Please see our Dental Implant FAQ <a
                                        href="../../dental-implants-faq/index.html">here.</a></p>
                            <h3>Dental Implant Instructions</h3>
                            <p>At Abbotsford Oral Surgery and Dental Implant Centre, our priority is to provide safe and
                                comfortable procedures with optimal outcomes. Dr. Esmail has undergone extensive
                                specialized training to provide you with excellent oral surgery care, but when it comes
                                to getting the most out of your treatment, ensuring a quick recovery, and reducing the
                                risk of complications, it is essential that you follow the proper self-care guidelines
                                both before your oral surgery and after.</p>
                            <p>You will receive detailed care instructions from your oral surgeon and, for your
                                convenience, we also include guidelines here. To best prepare you, we provide both pre-
                                and post-operative instructions. The general pre-operative instructions will help you
                                prepare for surgery by adjusting your diet, medications, and anything else needed to
                                ensure you are completely ready. The post-operative instructions will be specific to
                                your procedure and will provide instructions for your care after surgery. Always feel
                                free to call our practice with any questions you may have, including questions about
                                pre- and post-operative care. If anything is unclear to you, our knowledgeable staff is
                                happy to offer guidance.</p>
                            <h3>Tooth Replacement Options</h3>
                            <p>Dr. Esmail of Abbotsford Oral Surgery and Dental Implant Centre is board-certified by
                                both the Royal College of Dentists of Canada and the American Board of Oral and
                                Maxillofacial Surgery. While our practice specializes in placing dental implants, we
                                understand that dental implants may not be the right tooth replacement option for
                                everyone. We proudly offer information on additional tooth replacement solutions on our
                                website. If you would like to determine the right tooth restoration option for you,
                                please contact our practice to schedule a consultation with the oral surgeon. Some of
                                your options for tooth replacement may include the following:</p>
                            <ul>
                                <li>Dental Implants</li>
                                <li>Full-Arch Replacements</li>
                                <li>Bridges</li>
                                <li>Flippers/Metal Partials</li>
                                <li>Full or Partial Dentures</li>
                            </ul>
                            <p>If you have questions, or are ready to schedule your consultation with Dr. Esmail, please
                                contact our <b><em>Abbotsford, BC</em>,</b> practice. We look forward to meeting with
                                you to discuss the restoration of your smile!</p>
                            <p>Learn more about tooth replacement options on our <a
                                        href="../../tooth-replacement-options/index.html">Tooth Replacement Options
                                    page.</a></p>
                            <p>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7397.767641803216!2d-122.31575674195159!3d49.03900507166582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5484354461678c0f%3A0xd39d9619933dd651!2sAbbotsford%20Oral%20Surgery%20and%20Dental%20Implant%20Centre%3A%20Dr.%20Nayeem%20Esmail%2C%20Inc.!5e0!3m2!1sen!2sca!4v1570469357043!5m2!1sen!2sca"
                                        width="1144" height="450" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                            </p>
                        </div>
                    </article>
                    <div class="row cta-footer ">
                        <div class="col-md-3">
                            <a href="../../contact-us/index.html">
                                <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                                </button>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <p>
                                As a patient of our office, we want it to be as easy as possible for you to visit our
                                team for care. You can request an appointment by filling out our online form. </p>
                        </div>
                    </div>
                </main>
            </div>
			<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
            <style type="text/css">
                .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
                    background-color: #1171AF !important;
                    color: #fff !important;
                }

                .secondary_color {
                    background-color: #46AD4C;
                }

                .highlight_color {
                    background-color: #32C5F4 !important;
                }

                ;
                .site-inner {
                    background-color: #1171AF !important;
                }

                /*Main Homepage*/
                .gradient {
                    background: #00AEEF; /* Old browsers */
                    background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
                    background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
                    background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
                    background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
                    background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
                    background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
                    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
                    Padding: 10%;
                }

                .home input {
                    color: rgba(17, 113, 175, 1) !important;
                }

                /*body.custom-background {
				  background-color: rgba(17,113,175,1) !important;
				}*/

                /*Menu*/
                .nav-primary .sub-menu a {
                    background-color: #1171AF;
                    border-color: #fff;
                    color: #fff !important;
                }

                .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
                    background-color: #1171AF;
                }

                /*Add primary color to the number CTA*/
                .callus a {
                    font-weight: bold;
                    color: #1171AF !important;
                }

                /*Images*/
                .background_cta {
                    background-image: url();
                }

                .home-map-image {
                    background-image: url();
                    background-size: cover;
                    background-position: Center Center !important;
                }

                /*Testimonial Page*/
                .related_videos {
                    border-top: 7px solid #46AD4C;
                }

                /*Youtube Video */
                .video_thumb {
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: top center;
                    height: 400px;
                }

                /*Change play button color on all inline video images*/
                .content #playhover,
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
                .main-home #playhover,
                .page-template-hero-min-landing #playhover,
                .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
                .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
                    background: url(../../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
                }

                /*Add line to the botton of inline video image*/
                article #playhover + img {
                    border-bottom: 5px solid #46AD4C;
                }


                /*Fancy Overlay*/
                #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
                    background-image: url(../../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
                    background-size: cover !important;
                    background-position: left top !important;
                }

                /*Archive Pages*/
                .archive-description, .author-box {
                    border-top: 7px solid #46AD4C;
                    border-left: 0px;
                    border-right: 0px;
                }

                .archive .entry-header a {
                    color: #fff;
                    font-size: 1em;
                }


                /*Team Page*/
                .tiled-gallery .gallery-row, .bio-image img {
                    border-bottom: 3px solid #1171AF;
                }

                /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
                .team .entry-content img, .team .gallery-item img {
                    border-bottom: 5px solid #1171AF;
                }

                /*Links*/
                a {
                    color: #1171AF;
                    text-decoration: none !important;
                }

                /*Change current menu color*/
                /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

                /*BTNS*/
                .button {
                    background-color: #46AD4C !important;
                }

                /*Add colored bullet points*/
                .entry-content ul {
                    list-style-type: none;
                    position: relative;
                    padding-left: 0;
                }

                .entry-content ul > li {
                    list-style: none;
                }

                .entry-content ul > li:before {
                    content: "• \00a0 \00a0 \00a0";
                    color: #1171AF; /* Color of the bullet */
                    position: absolute;
                    left: -1em;
                    margin-right: 5px;
                }

                /*Remove bullets on feedback page*/
                .page-template-page_feedback .entry-content ul > li:before {
                    content: "" !important;
                }

                /* Video Player Pages
				--------------------------------------------- */
                .fancybox-wrap:before {
                    background: url(../../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
                    content: ".";
                    position: relative;
                    width: 100%;
                    height: 130px;
                    z-index: 8040;
                    display: block;
                    margin-bottom: 20px;
                    background-size: contain !important;
                }

                /*Homepage - Max Width HeroCard - change line under image*/
                .page-template-hero-max-landing > a > img {
                    border-bottom: 4px solid #1171AF;
                }

                /*change color on landinage page background*/
                .animated-home {
                    background-color: #1171AF;
                }

                /*Homepage - Max Width HeroCard and full page content - change line under image*/
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
                .page-template-hero-max-landing > .row > .col-md-12 a img {
                    border-bottom: 4px solid #1171AF;
                    width: 100%;
                }


                /* Archive Page
				--------------------------------------------- */
                .circle-text:after {
                    background-color: #32C5F4 !important;
                }

                body .entry-content .gfield_label {
                    font-size: 30px !important;
                    color: #1171AF;
                }

                /*Add BG image to full screen pages.  Going to remove this for now*/
                /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
					background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
					background-size: cover;
					background-position: left top !important;
				}*/

                .full-width-content.custom-background {
                    background-image: url("../../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
                    background-attachment: fixed;
                }


                body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
                    border: 1px solid #1171AF;
                    background-color: #FFF8BD;
                }


                .gform_wrapper .percentbar_blue {
                    background-image: url('../../wp-content/themes/nuvo-express/images/opas_15.png');
                    background-repeat: repeat-x;
                    background-color: #1171AF;
                    color: #FFF;
                }




                /* Make some mobile Tweaks
				--------------------------------------------- */
                @media only screen and (max-width: 1023px) {
                    .mobile-cta {
                        color: #1171AF !important;
                    }

                    /*Make mobile help menu primary color*/
                }

                /* This tweaks only Small devices (tablets and phones, 768px and up) */
                @media (max-width: 900px) {
                    .fancybox-youtube > .feedperson, .feedperson.shade30 {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under main videos on homepage / Move background face down*/
                    div.col-md-12.row.no-gutters > a > div.feedperson {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under CTA buttons on homepage*/
                }

                /*Add custom color to the case study block on homepage*/
                .image-holder:after {
                    background-color: rgba(70, 173, 76, 0.7);

                }

                /*Add video overlay on main sizzle as a dynamic layer*/
                .home-hero {
                    background: rgba(17, 113, 175, 1) !important;
                }

                @media only screen and (max-width: 1024px) {

                    .home-hero:after {
                        background: rgba(17, 113, 175, 0.8);
                    }

                }

            </style>
        </div>
		<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
