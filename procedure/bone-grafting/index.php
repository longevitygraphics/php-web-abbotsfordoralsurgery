<?php
$page_title       = 'Bone Grafting in Abbotsford, BC';
$doc_title        = 'Bone Grafting';
$meta_description = 'Bone grafting is a common first step for implant patients in Abbotsford, BC, because it provides the supportive foundation needed for successful placement.';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="procedure-template-default single single-procedure postid-99070 single-format-standard custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/MedicalProcedure">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="name">Bone Grafting</h1>
            </header>
            <div class="breadcrumb">You are here: <span class="breadcrumb-link-wrap" itemprop="url"><a
                            class="breadcrumb-link" href="../../index.html" itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                <span class="breadcrumb-link-wrap" itemprop="url"><a class="breadcrumb-link" href="../index.html"
                                                                     itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Procedures</span></a><meta
                            itemprop="position" content="2"></span> <span aria-label="breadcrumb separator">/</span>
                Bone Grafting
            </div>
            <div class="row no-gutters">
                <div class=" col-md-12 main_header" itemscope itemtype="http://schema.org/ImageObject">
                    <a href="https://youtu.be/5oVycoDIuEY?rel=0&amp;autohide=1&amp;showinfo=0" class="fancybox-youtube"
                       title='Bone grafting in Abbotsford, BC'>
                        <span id="playhover"></span>
                        <img width="1144" height="600"
                             src="../../wp-content/uploads/bone-grafting-procedure-1144x600.jpg"
                             class="attachment-testimonial-img size-testimonial-img wp-post-image"
                             alt="What is bone grafting in Abbotsford, BC?"</a>
                </div>
            </div>
            <div class="clearboth"></div>

            <div class="row no-gutters">
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implants-bone-graft-donna-abbotsford-bc/index.html"
                       title='Bone Grafting and Dental Implants Restored Donna’s Smile'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/donna-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Donna the dental implants patient in Abbotsford, BC'
                             title='Bone Grafting and Dental Implants Restored Donna’s Smile'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implant-guya-abbotsford-bc/index.html"
                       title='Guya Chose a Dental Implant to Replace Her Tooth'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/guya-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Guya the dental implants patient in Abbotsford, BC'
                             title='Guya Chose a Dental Implant to Replace Her Tooth'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implant-tooth-extraction-anna-abbotsford-bc/index.html"
                       title='Anna Needed an Extraction and Dental Implant'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/anna-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Anna the dental implants patient in Abbotsford, BC'
                             title='Anna Needed an Extraction and Dental Implant'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/full-arch-restoration-les-abbotsford-bc/index.html"
                       title='Les Underwent a Full-Arch Restoration'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/les-full-arch-restoration-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Les the full-arch patient in Abbotsford, BC'
                             title='Les Underwent a Full-Arch Restoration'/>
                    </a>
                </div>
            </div>
            <div class="content-sidebar-wrap">
                <main class="content" id="genesis-content">
                    <article
                            class="post-99070 procedure type-procedure status-publish format-standard has-post-thumbnail entry secondary_color">
                        <div class="entry-content" itemprop="description"><p>Tooth loss can cause the jaw bone to
                                diminish because the root of a natural tooth must be present to stimulate the bone and
                                keep it healthy. Other causes of bone loss can be inadequate bone structure due to
                                previous extractions, gum disease, or injuries. Regardless of the cause, bone loss can
                                make the successful placement of dental implants difficult or impossible. Fortunately,
                                advancements in medical science have allowed us the ability to grow bone where it is
                                needed, which has opened the option of dental implants to more patients. The bone
                                utilized in bone grafting techniques for oral surgery can be obtained from a tissue
                                bank, or your own bone is taken from another part of your body. If you are having dental
                                implants placed and are suffering from bone loss in your jaw, you may be a candidate for
                                having bone tissue grafted into the implant area to ensure stable and permanent
                                placement. </p>
                            <p>Our Abbotsford, BC, practice is proud to offer the full scope of oral surgery procedures,
                                including various bone grafting procedures. While many patients find they require a bone
                                grafting procedure for the successful placement of dental implants, bone grafts can also
                                be used in the treatment of other oral and maxillofacial conditions that we address at
                                Abbotsford Oral Surgery and Dental Implant Centre.</p>
                            <h2>Types of Bone Grafting Procedures</h2>
                            <p>Bone grafting is a common first step for dental implant patients. In bone grafting
                                procedures, a solution of granulated bone material or other healing agents will be
                                applied in the areas of your jaw that need to be reinforced, stimulating new, healthy
                                bone growth at the site of future dental implant placement. At Abbotsford Oral Surgery
                                and Dental Implant Centre, we perform several types of bone grafting treatments to meet
                                the individual needs of our patients. We also offer soft tissue grafting to treat
                                periodontal disease and gum recession and to help stop bone loss and further recession
                                of the gums. </p>
                            <p>Some of the grafting procedures we offer include</p>
                            <ul>
                                <li><strong>Ridge Augmentation.</strong> The alveolar ridge bone is the bone that
                                    surrounds and supports the teeth. If this bone is not healthy or dense enough to
                                    support dental implants, Dr. Esmail may recommend a ridge augmentation. During ridge
                                    augmentation, the alveolar ridge is surgically split, and bone graft material is
                                    placed to stimulate bone growth.
                                </li>
                                <li><strong>Sinus Lift.</strong> If there is not enough bone present between the upper
                                    jaw and the sinus cavity for successful dental implant placement, your oral surgeon
                                    may recommend a sinus lift procedure. For this procedure, a bone graft is placed in
                                    the jaw below the sinus membrane. This graft integrates over several months to
                                    create a more solid foundation for an implant.
                                </li>
                                <li><strong>Socket preservation.</strong> Oral and maxillofacial surgeons can perform a
                                    special bone grafting procedure called socket preservation that protects the area of
                                    the jaw where a dental implant will eventually be placed. This procedure minimizes
                                    the bone loss that occurs after an extraction. To complete a socket preservation
                                    procedure, after the tooth has been extracted, your oral surgeon fills the tooth
                                    socket with bone grafting material. When the grafted site has healed, a dental
                                    implant is placed to restore the missing tooth. Sometimes your surgeon will place
                                    the dental implant at the same time the graft is placed, allowing you to enjoy your
                                    new smile faster.
                                </li>
                                <li><strong>Soft Tissue Grafting.</strong> If a patient is suffering from periodontal
                                    disease and gum recession, a soft tissue graft may be recommended so that the gum
                                    line is uniform around a dental implant for a more natural appearance. For this
                                    procedure, soft tissue grafts (small pieces of oral tissue harvested from other
                                    areas of the mouth) will be surgically implanted in the affected area. There are
                                    more than just aesthetic improvements from this procedure. Soft tissue grafts can
                                    help stop bone loss and additional recession of the gums and can reduce root
                                    sensitivity.
                                </li>
                                <li><strong>Nerve Repositioning.</strong> The nerve that gives feeling to the lower lip
                                    and chin is called the inferior alveolar nerve. If this nerve is susceptible to
                                    damage during a dental implant procedure, the nerve must be accessed through the jaw
                                    bone and then repositioned. Bone grafting material is added to the area after a
                                    nerve repositioning procedure. Nerve repositioning carries some risk as there is
                                    almost always some numbness of the lower lip and jaw area after the surgery. This
                                    typically dissipates, but it may be permanent. Most of the time, other options are
                                    considered first. Nerve repositioning surgeries are performed under IV sedation or
                                    general anesthesia.
                                </li>
                            </ul>
                            <p>Bone grafting can repair implant sites with inadequate bone structure so that more
                                patients can restore their smile with dental implants — and once the restoration site
                                has healed, the new dental implant will help maintain the jaw bone over time. Because
                                each bone grafting option has its own risks and benefits, Dr. Esmail will evaluate your
                                case with extreme care to determine which type of bone grafting technique is best suited
                                to your needs. Dr. Esmail is extensively trained to provide the full scope of bone
                                grafting procedures to ensure that your replacement tooth or teeth will look, feel, and
                                function just like natural, healthy teeth. </p>
                            <h2>Bone Morphogenetic Proteins</h2>
                            <p>Sometimes, bone grafting techniques utilize synthetic materials as a substitute for real
                                bone, which eliminates the need to source bone from other graft sites on your body or
                                use banked bone. These materials are safe and proven alternatives and include bone
                                morphogenetic proteins, or BMPs, which we use in our practice. BMPs are proteins that
                                are naturally produced in the body for creating new cartilage and bone. This material
                                can be used to increase the jaw bone support required for durable dental implant
                                placement. During BMP treatment, a protein solution is added to a sponge-like material
                                that is placed over the area of bone in need of treatment to stimulate and support bone
                                growth. After the new bone has finished growing into the graft area, your jaw bone will
                                be ready to support your new dental implant.</p>
                            <h2>Bone Grafting in Abbotsford, BC</h2>
                            <p>Bone grafting can offer a stable foundation of healthy bone for the long-term stability
                                of your dental implants. If you have any questions or would like to schedule a
                                consultation, please contact Abbotsford Oral Surgery and Dental Implant Centre. Dr.
                                Esmail will be happy to discuss your bone grafting options with you during your
                                consultation and will develop a treatment plan for you based on your unique needs.</p>
                        </div>
                    </article>
                    <div class="row cta-footer ">
                        <div class="col-md-3">
                            <a href="../../contact-us/index.html">
                                <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                                </button>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <p>
                                As a patient of our office, we want it to be as easy as possible for you to visit our
                                team for care. You can request an appointment by filling out our online form. </p>
                        </div>
                    </div>
                </main>
            </div>
			<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
            <style type="text/css">
                .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
                    background-color: #1171AF !important;
                    color: #fff !important;
                }

                .secondary_color {
                    background-color: #46AD4C;
                }

                .highlight_color {
                    background-color: #32C5F4 !important;
                }

                ;
                .site-inner {
                    background-color: #1171AF !important;
                }

                /*Main Homepage*/
                .gradient {
                    background: #00AEEF; /* Old browsers */
                    background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
                    background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
                    background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
                    background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
                    background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
                    background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
                    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
                    Padding: 10%;
                }

                .home input {
                    color: rgba(17, 113, 175, 1) !important;
                }

                /*body.custom-background {
				  background-color: rgba(17,113,175,1) !important;
				}*/

                /*Menu*/
                .nav-primary .sub-menu a {
                    background-color: #1171AF;
                    border-color: #fff;
                    color: #fff !important;
                }

                .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
                    background-color: #1171AF;
                }

                /*Add primary color to the number CTA*/
                .callus a {
                    font-weight: bold;
                    color: #1171AF !important;
                }

                /*Images*/
                .background_cta {
                    background-image: url();
                }

                .home-map-image {
                    background-image: url();
                    background-size: cover;
                    background-position: Center Center !important;
                }

                /*Testimonial Page*/
                .related_videos {
                    border-top: 7px solid #46AD4C;
                }

                /*Youtube Video */
                .video_thumb {
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: top center;
                    height: 400px;
                }

                /*Change play button color on all inline video images*/
                .content #playhover,
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
                .main-home #playhover,
                .page-template-hero-min-landing #playhover,
                .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
                .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
                    background: url(../../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
                }

                /*Add line to the botton of inline video image*/
                article #playhover + img {
                    border-bottom: 5px solid #46AD4C;
                }


                /*Fancy Overlay*/
                #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
                    background-image: url(../../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
                    background-size: cover !important;
                    background-position: left top !important;
                }

                /*Archive Pages*/
                .archive-description, .author-box {
                    border-top: 7px solid #46AD4C;
                    border-left: 0px;
                    border-right: 0px;
                }

                .archive .entry-header a {
                    color: #fff;
                    font-size: 1em;
                }


                /*Team Page*/
                .tiled-gallery .gallery-row, .bio-image img {
                    border-bottom: 3px solid #1171AF;
                }

                /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
                .team .entry-content img, .team .gallery-item img {
                    border-bottom: 5px solid #1171AF;
                }

                /*Links*/
                a {
                    color: #1171AF;
                    text-decoration: none !important;
                }

                /*Change current menu color*/
                /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

                /*BTNS*/
                .button {
                    background-color: #46AD4C !important;
                }

                /*Add colored bullet points*/
                .entry-content ul {
                    list-style-type: none;
                    position: relative;
                    padding-left: 0;
                }

                .entry-content ul > li {
                    list-style: none;
                }

                .entry-content ul > li:before {
                    content: "• \00a0 \00a0 \00a0";
                    color: #1171AF; /* Color of the bullet */
                    position: absolute;
                    left: -1em;
                    margin-right: 5px;
                }

                /*Remove bullets on feedback page*/
                .page-template-page_feedback .entry-content ul > li:before {
                    content: "" !important;
                }

                /* Video Player Pages
				--------------------------------------------- */
                .fancybox-wrap:before {
                    background: url(../../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
                    content: ".";
                    position: relative;
                    width: 100%;
                    height: 130px;
                    z-index: 8040;
                    display: block;
                    margin-bottom: 20px;
                    background-size: contain !important;
                }

                /*Homepage - Max Width HeroCard - change line under image*/
                .page-template-hero-max-landing > a > img {
                    border-bottom: 4px solid #1171AF;
                }

                /*change color on landinage page background*/
                .animated-home {
                    background-color: #1171AF;
                }

                /*Homepage - Max Width HeroCard and full page content - change line under image*/
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
                .page-template-hero-max-landing > .row > .col-md-12 a img {
                    border-bottom: 4px solid #1171AF;
                    width: 100%;
                }


                /* Archive Page
				--------------------------------------------- */
                .circle-text:after {
                    background-color: #32C5F4 !important;
                }

                body .entry-content .gfield_label {
                    font-size: 30px !important;
                    color: #1171AF;
                }

                /*Add BG image to full screen pages.  Going to remove this for now*/
                /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
					background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
					background-size: cover;
					background-position: left top !important;
				}*/

                .full-width-content.custom-background {
                    background-image: url("../../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
                    background-attachment: fixed;
                }


                body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
                    border: 1px solid #1171AF;
                    background-color: #FFF8BD;
                }


                .gform_wrapper .percentbar_blue {
                    background-image: url('../../wp-content/themes/nuvo-express/images/opas_15.png');
                    background-repeat: repeat-x;
                    background-color: #1171AF;
                    color: #FFF;
                }




                /* Make some mobile Tweaks
				--------------------------------------------- */
                @media only screen and (max-width: 1023px) {
                    .mobile-cta {
                        color: #1171AF !important;
                    }

                    /*Make mobile help menu primary color*/
                }

                /* This tweaks only Small devices (tablets and phones, 768px and up) */
                @media (max-width: 900px) {
                    .fancybox-youtube > .feedperson, .feedperson.shade30 {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under main videos on homepage / Move background face down*/
                    div.col-md-12.row.no-gutters > a > div.feedperson {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under CTA buttons on homepage*/
                }

                /*Add custom color to the case study block on homepage*/
                .image-holder:after {
                    background-color: rgba(70, 173, 76, 0.7);

                }

                /*Add video overlay on main sizzle as a dynamic layer*/
                .home-hero {
                    background: rgba(17, 113, 175, 1) !important;
                }

                @media only screen and (max-width: 1024px) {

                    .home-hero:after {
                        background: rgba(17, 113, 175, 0.8);
                    }

                }

            </style>
        </div>
		<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
