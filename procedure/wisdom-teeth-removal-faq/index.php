<?php
$page_title       = 'Wisdom Teeth Removal FAQ in Abbotsford, BC';
$doc_title        = 'Wisdom Teeth Removal FAQ';
$meta_description = 'Get answers to the most frequently asked questions about wisdom teeth and learn more before your upcoming treatment in Abbotsford, BC.';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="procedure-template-default single single-procedure postid-99087 single-format-standard custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/MedicalProcedure">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="name">Wisdom Teeth Removal FAQ</h1>
            </header>
            <div class="breadcrumb">You are here: <span class="breadcrumb-link-wrap" itemprop="url"><a
                            class="breadcrumb-link" href="../../index.html" itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                <span class="breadcrumb-link-wrap" itemprop="url"><a class="breadcrumb-link" href="../index.html"
                                                                     itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Procedures</span></a><meta
                            itemprop="position" content="2"></span> <span aria-label="breadcrumb separator">/</span>
                Wisdom Teeth Removal FAQ
            </div>
            <div class="row no-gutters">
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/wisdom-teeth-removal-justine-abbotsford-bc/index.html"
                       title='Justine Needed Her Wisdom Teeth Removed'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/justine-wisdom-teeth-removal-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Justine the wisdom teeth patient in Abbotsford, BC'
                             title='Justine Needed Her Wisdom Teeth Removed'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/wisdom-teeth-removal-chris-abbotsford-bc/index.html"
                       title='Chris Underwent Wisdom Teeth Removal'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/chris-wisdom-teeth-removal-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Chris the wisdom teeth patient in Abbotsford, BC'
                             title='Chris Underwent Wisdom Teeth Removal'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/oral-pathology-wisdom-teeth-removal-brenda-abbotsford-bc/index.html"
                       title='Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
 <span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/brenda-oral-pathology-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Brenda the oral pathology patient in Abbotsford, BC'
                             title='Brenda Had an Oral Pathology Treated and Wisdom Tooth Removed'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/oral-pathology-aatif-abbotsford-bc/index.html"
                       title='Aatif’s Oral Pathology Treatment'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/aatif-oral-pathology-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Aatif the oral pathology patient in Abbotsford, BC'
                             title='Aatif’s Oral Pathology Treatment'/>
                    </a>
                </div>
            </div>
            <div class="content-sidebar-wrap">
                <main class="content" id="genesis-content">
                    <article
                            class="post-99087 procedure type-procedure status-publish format-standard procedure-types-hidden entry secondary_color">
                        <div class="entry-content" itemprop="description"><h3>How will I know if my/my child’s wisdom
                                teeth are coming in?</h3>
                            <p>If you or your child has discomfort or swelling in the area behind the back molars, or if
                                there is an unpleasant taste or smell in the mouth, you should see a dental professional
                                for an examination to see if the wisdom teeth are impacted or there is a related
                                infection in the mouth. Sometimes there are no symptoms when wisdom teeth are
                                developing, erupting, or becoming impacted. This is why it is best to have your dentist
                                track and evaluate the development of you or your child’s wisdom teeth during regular
                                dental exams to catch problems before they arise. </p>
                            <h3>Is wisdom teeth extraction painful?</h3>
                            <p>In most cases, the removal of wisdom teeth is performed under intravenous (IV) sedation.
                                Dr. Esmail will administer the anesthesia that was decided upon during your
                                consultation, and no procedure will take place until you or your child is at ease and
                                ready. There will be no sensation of pain during the procedure. Patients receive a
                                prescription for pain medication to control discomfort during recovery, but we often
                                find that over-the-counter pain medications work fine for most of our wisdom teeth
                                extraction patients. </p>
                            <h3>What is the cost of wisdom teeth removal?</h3>
                            <p>There are several factors that will go in to estimating your final cost for a wisdom
                                teeth extraction: how many teeth will be extracted, the type of anesthesia chosen, your
                                insurance coverage, and other factors such as how difficult the removal will be (depth
                                of impaction, etc.). When you or your child is examined by the surgeon, a treatment plan
                                will be made with an estimate of your costs. The best way to find out what your exact
                                fee will be for wisdom teeth removal is to call our practice and schedule a consultation
                                with Dr. Esmail. Early removal of wisdom teeth can save time and money.</p>
                            <h3>Are wisdom teeth always extracted?</h3>
                            <p>Third molars are not always extracted, and for some patients, this is fine. In general,
                                however, the human jaw does not have enough space for wisdom teeth to grow in without
                                causing oral health problems. Your family dentist can track the development of your or
                                your child’s wisdom teeth. If they need to be extracted, you will be referred to a
                                trusted oral surgeon, such as Dr. Esmail. Early diagnosis could save time, money, and
                                discomfort later. </p>
                            <h3>Why do oral surgeons extract wisdom teeth? Why not have my dentist perform this
                                surgery?</h3>
                            <p>Wisdom teeth extraction surgery is usually performed early, before the teeth have
                                erupted. This type of oral surgery is beyond the scope of a family dentist because the
                                molars must be removed from beneath the gum tissue and overlying bone. Oral and
                                maxillofacial surgeons have years of additional training in surgery and anesthesia to
                                safely remove wisdom teeth and address the complications that can arise from problematic
                                wisdom teeth.</p>
                            <h3>What is the recovery time after wisdom teeth removal?</h3>
                            <p>We find that two to four days is sufficient recovery time for most patients who have
                                undergone a wisdom teeth extraction procedure. After this time, many of our patients are
                                comfortable enough to return to school or work.</p>
                            <h3>What foods can I eat after the extraction?</h3>
                            <p>For at least three days following wisdom teeth extraction, you or your child should stick
                                to a diet of soft foods. You can gradually introduce regular foods after three days, but
                                be sure to avoid smoking and the use of a straw during the recovery period as indicated
                                by your oral surgeon. Detailed pre- and post-operative instructions will be
                                provided.</p>
                            <h3>Where can I have my wisdom teeth removed?</h3>
                            <p>If you have been recommended for wisdom teeth removal, or if you suspect that you or your
                                child’s wisdom teeth are causing oral health issues, please contact Abbotsford Oral
                                Surgery and Dental Implant Centre to schedule a consultation with Dr. Esmail. We will
                                take excellent care of you and your loved ones at our welcoming and trusted
                                practice.</p>
                            <h3>Other questions?</h3>
                            <p>At Abbotsford Oral Surgery and Dental Implant Centre, we are proud to offer the highest
                                quality of patient care. We are available and pleased to answer any questions you may
                                have. Feel free to give us a call at (604) 504-7522.</p>
                        </div>
                    </article>
                    <div class="row cta-footer ">
                        <div class="col-md-3">
                            <a href="../../contact-us/index.html">
                                <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                                </button>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <p>
                                As a patient of our office, we want it to be as easy as possible for you to visit our
                                team for care. You can request an appointment by filling out our online form. </p>
                        </div>
                    </div>
                </main>
            </div>
			<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
            <style type="text/css">
                .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
                    background-color: #1171AF !important;
                    color: #fff !important;
                }

                .secondary_color {
                    background-color: #46AD4C;
                }

                .highlight_color {
                    background-color: #32C5F4 !important;
                }

                ;
                .site-inner {
                    background-color: #1171AF !important;
                }

                /*Main Homepage*/
                .gradient {
                    background: #00AEEF; /* Old browsers */
                    background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
                    background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
                    background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
                    background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
                    background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
                    background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
                    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
                    Padding: 10%;
                }

                .home input {
                    color: rgba(17, 113, 175, 1) !important;
                }

                /*body.custom-background {
				  background-color: rgba(17,113,175,1) !important;
				}*/

                /*Menu*/
                .nav-primary .sub-menu a {
                    background-color: #1171AF;
                    border-color: #fff;
                    color: #fff !important;
                }

                .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
                    background-color: #1171AF;
                }

                /*Add primary color to the number CTA*/
                .callus a {
                    font-weight: bold;
                    color: #1171AF !important;
                }

                /*Images*/
                .background_cta {
                    background-image: url();
                }

                .home-map-image {
                    background-image: url();
                    background-size: cover;
                    background-position: Center Center !important;
                }

                /*Testimonial Page*/
                .related_videos {
                    border-top: 7px solid #46AD4C;
                }

                /*Youtube Video */
                .video_thumb {
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: top center;
                    height: 400px;
                }

                /*Change play button color on all inline video images*/
                .content #playhover,
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
                .main-home #playhover,
                .page-template-hero-min-landing #playhover,
                .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
                .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
                    background: url(../../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
                }

                /*Add line to the botton of inline video image*/
                article #playhover + img {
                    border-bottom: 5px solid #46AD4C;
                }


                /*Fancy Overlay*/
                #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
                    background-image: url(../../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
                    background-size: cover !important;
                    background-position: left top !important;
                }

                /*Archive Pages*/
                .archive-description, .author-box {
                    border-top: 7px solid #46AD4C;
                    border-left: 0px;
                    border-right: 0px;
                }

                .archive .entry-header a {
                    color: #fff;
                    font-size: 1em;
                }


                /*Team Page*/
                .tiled-gallery .gallery-row, .bio-image img {
                    border-bottom: 3px solid #1171AF;
                }

                /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
                .team .entry-content img, .team .gallery-item img {
                    border-bottom: 5px solid #1171AF;
                }

                /*Links*/
                a {
                    color: #1171AF;
                    text-decoration: none !important;
                }

                /*Change current menu color*/
                /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

                /*BTNS*/
                .button {
                    background-color: #46AD4C !important;
                }

                /*Add colored bullet points*/
                .entry-content ul {
                    list-style-type: none;
                    position: relative;
                    padding-left: 0;
                }

                .entry-content ul > li {
                    list-style: none;
                }

                .entry-content ul > li:before {
                    content: "• \00a0 \00a0 \00a0";
                    color: #1171AF; /* Color of the bullet */
                    position: absolute;
                    left: -1em;
                    margin-right: 5px;
                }

                /*Remove bullets on feedback page*/
                .page-template-page_feedback .entry-content ul > li:before {
                    content: "" !important;
                }

                /* Video Player Pages
				--------------------------------------------- */
                .fancybox-wrap:before {
                    background: url(../../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
                    content: ".";
                    position: relative;
                    width: 100%;
                    height: 130px;
                    z-index: 8040;
                    display: block;
                    margin-bottom: 20px;
                    background-size: contain !important;
                }

                /*Homepage - Max Width HeroCard - change line under image*/
                .page-template-hero-max-landing > a > img {
                    border-bottom: 4px solid #1171AF;
                }

                /*change color on landinage page background*/
                .animated-home {
                    background-color: #1171AF;
                }

                /*Homepage - Max Width HeroCard and full page content - change line under image*/
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
                .page-template-hero-max-landing > .row > .col-md-12 a img {
                    border-bottom: 4px solid #1171AF;
                    width: 100%;
                }


                /* Archive Page
				--------------------------------------------- */
                .circle-text:after {
                    background-color: #32C5F4 !important;
                }

                body .entry-content .gfield_label {
                    font-size: 30px !important;
                    color: #1171AF;
                }

                /*Add BG image to full screen pages.  Going to remove this for now*/
                /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
					background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
					background-size: cover;
					background-position: left top !important;
				}*/

                .full-width-content.custom-background {
                    background-image: url("../../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
                    background-attachment: fixed;
                }


                body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
                    border: 1px solid #1171AF;
                    background-color: #FFF8BD;
                }


                .gform_wrapper .percentbar_blue {
                    background-image: url('../../wp-content/themes/nuvo-express/images/opas_15.png');
                    background-repeat: repeat-x;
                    background-color: #1171AF;
                    color: #FFF;
                }




                /* Make some mobile Tweaks
				--------------------------------------------- */
                @media only screen and (max-width: 1023px) {
                    .mobile-cta {
                        color: #1171AF !important;
                    }

                    /*Make mobile help menu primary color*/
                }

                /* This tweaks only Small devices (tablets and phones, 768px and up) */
                @media (max-width: 900px) {
                    .fancybox-youtube > .feedperson, .feedperson.shade30 {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under main videos on homepage / Move background face down*/
                    div.col-md-12.row.no-gutters > a > div.feedperson {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under CTA buttons on homepage*/
                }

                /*Add custom color to the case study block on homepage*/
                .image-holder:after {
                    background-color: rgba(70, 173, 76, 0.7);

                }

                /*Add video overlay on main sizzle as a dynamic layer*/
                .home-hero {
                    background: rgba(17, 113, 175, 1) !important;
                }

                @media only screen and (max-width: 1024px) {

                    .home-hero:after {
                        background: rgba(17, 113, 175, 0.8);
                    }

                }

            </style>
        </div>
		<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
