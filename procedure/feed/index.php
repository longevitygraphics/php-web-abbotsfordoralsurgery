<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>Abbotsford Oral Surgery and Dental Implant Centre</title>
	<atom:link href="https://www.abbotsfordoralsurgery.com/procedure/feed/" rel="self" type="application/rss+xml" />
	<link>https://www.abbotsfordoralsurgery.com</link>
	<description></description>
	<lastBuildDate>
	Tue, 26 Mar 2019 21:53:02 +0000	</lastBuildDate>
	<language>en-US</language>
	<sy:updatePeriod>
	hourly	</sy:updatePeriod>
	<sy:updateFrequency>
	1	</sy:updateFrequency>
	<generator>https://wordpress.org/?v=5.1.1</generator>

<image>
	<url>https://www.abbotsfordoralsurgery.com/wp-content/uploads/cropped-ABOT-Favicon-32x32.png</url>
	<title>Abbotsford Oral Surgery and Dental Implant Centre</title>
	<link>https://www.abbotsfordoralsurgery.com</link>
	<width>32</width>
	<height>32</height>
</image> 
	<item>
		<title>Dental Implants</title>
		<link>https://www.abbotsfordoralsurgery.com/procedure/dental-implants-abbotsford-bc/</link>
				<comments>https://www.abbotsfordoralsurgery.com/procedure/dental-implants-abbotsford-bc/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/procedure/dental-implants-abbotsford-bc/</guid>
				<description><![CDATA[<p>When patients visit Abbotsford Oral Surgery and Dental Implant Centre to replace their broken, failing, or missing teeth, they have the option to choose dental implants for a highly functional and long-term tooth (or teeth) restoration solution. Dental implants truly change lives. For people who are living with tooth loss, the emotional consequences can be high — implants provide these&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/dental-implants-abbotsford-bc/">Dental Implants</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>When patients visit Abbotsford Oral Surgery and Dental Implant Centre to replace their broken, failing, or missing teeth, they have the option to choose <b><em>dental implants</em></b> for a highly functional and long-term tooth (or teeth) restoration solution. <i>Dental implants</i> truly change lives. For people who are living with tooth loss, the emotional consequences can be high — implants provide these patients with a secure foundation for replacement teeth that look, feel, and function just like natural teeth. <strong>Dental implants</strong> are surgically placed titanium posts that inserted into the jaw bone where teeth are missing. Implants can support individual replacement teeth, or secure other types of tooth restoration such as a full-arch replacement to replace an entire arch of teeth. Unlike bridges, no healthy teeth are damaged to secure the restoration, and implants can last a lifetime if proper care is taken. Because an implant fuses with the natural bone in the jaw just like a natural tooth root, you can regain the ability to eat virtually anything and smile with confidence knowing that your replacement teeth appear natural. Implants also keep the jaw healthy and strong, preventing bone deterioration by stimulating the bone in your jaw just like natural teeth.</p>
<h3>Benefits of Dental Implants</h3>
<p>To restore a missing tooth, oral surgeons can surgically place a dental implant made of titanium that securely anchors a custom crown for a complete, healthy tooth replacement. With countless incredible benefits, it is easy to see why this dental restoration solution is a favorite with our Abbotsford Oral Surgery and Dental Implant Centre patients. These are a few of the benefits of this transformative tooth restoration treatment:</p>
<ul>
<li style="list-style-type: none">
<ul>
<li>Durability and versatility — with proper care, dental implants can last a lifetime.</li>
<li>Implants look, feel, and function like your natural teeth.</li>
<li>Improved self-confidence.</li>
<li>Eat your favorite foods again.</li>
<li>Talking, smiling, and eating with confidence again.</li>
<li>Dental implants replace the entire tooth, including the root, so they work to preserve your long-term oral health.</li>
<li>Most patients are dental implant candidates.</li>
</ul>
</li>
</ul>
<p>. . . and so much more!</p>
<p>We find that many patients are candidates for receiving dental implants — especially with the advent of advanced bone grafting techniques that open dental implant treatment to patients who have suffered bone loss due to long-term tooth loss, or other factors. Typically, if you are healthy enough to undergo a tooth extraction, dental implants are an option for you.</p>
<p>If you are looking to restore your oral function with dental implants, or have any further questions about our procedures, please contact us to schedule a consultation with Dr. Esmail at our practice in <b><em>Abbotsford, BC</em></b>. We appreciate the opportunity to care for you at Abbotsford Oral Surgery and Dental Implant Centre.</p>
<h3>Dental Implant Procedure</h3>
<p>Dr. Esmail at Abbotsford Oral Surgery and Dental Implant Centre has been extensively trained in dental implant placement through surgical residency, continuing education, and by working with patients in this practice over the years. His training and experience have made him highly qualified to perform implant placement with optimal long-term results.</p>
<p>Dental Implant placement is a team effort between an oral and maxillofacial surgeon and a restorative dentist. While Dr. Esmail performs the actual implant surgery, initial tooth extractions, and bone grafting (if necessary), the restorative dentist (your family dentist) designs and creates the permanent prosthesis. Your dentist will also make any temporary prosthesis needed during the implant process. To fabricate the prosthetic tooth or teeth, a digital 3D impression is taken of your mouth and a model is created. The replacement teeth are based on this model and can be crowns, fixed bridges, or removable/fixed dentures.</p>
<p>Most dental implants and bone graft procedures can be performed at Abbotsford Oral Surgery and Dental Implant Centre under intravenous (conscious) sedation. Dr. Esmail is highly trained in the administration of in-office anesthesia. To learn more about our anesthesia options and procedures, please review our <a href="/anesthesia-options/">Anesthesia Options</a> page so that you will be prepared to discuss your anesthesia preferences with the surgeon during your consultation. The dental implant procedure typically goes as follows:</p>
<ul>
<li><strong>Consultation.</strong> The process of receiving dental implants begins on the day of your consultation with Dr. Esmail. During this appointment with your oral surgeon, your condition will be assessed, and you will work together to form a treatment plan. 3D scans may be taken to create detailed surgical guides for your procedure.</li>
<li><strong>Surgery.</strong> You will have chosen your anesthesia preference with the doctor at the consultation, and it will be administered before your procedure. You will not feel any discomfort during surgery. Dr. Esmail will make a small incision in your gum tissue where the implant is to be placed. Next, he will insert the titanium implant post into the exposed jaw bone. After the procedure, you will receive stitches at the site of your surgery and will then be given time to rest before your designated driver takes you home. The surgical procedure for implant placement takes 30 to 60 minutes for one implant, and about 2 to 3 hours for multiple implants.</li>
<li><strong>Recovery.</strong> After your surgery, you will return home to begin the healing process. You may have been outfitted with a temporary crown to wear during this time. We have found that many of our patients are back to their regular routines after just a couple of weeks; however, the fusion of the implant with the jaw bone will take a bit longer. This amount of healing time varies depending on the quality and quantity of bone in the jaw as well as a few other factors. Your doctor will discuss all of this with you so that you are fully informed before your procedure takes place. Follow-up care appointments with Dr. Esmail are often scheduled to ensure that you are healing well.</li>
<li><strong>Abutment Placement.</strong> After the initial phase of healing, the surgeon will place an abutment (support post) onto your titanium implant during one of your follow-up visits. The abutment is the piece that will connect your new crown to your implant.</li>
<li><strong>Crown placement.</strong> Once Dr. Esmail has determined that you are ready for the final phase of your treatment — the placement of your permanent tailor-made new crown — your restorative dentist will fit the crown to the dental implant. Get ready to smile!</li>
</ul>
<p>Your new dental implants should last a lifetime if you keep your mouth clean and healthy and maintain regular appointments with your dental specialists. Dr. Esmail is available to meet with you for a consultation appointment, where he will work closely with you to develop your treatment plan and restore your smile. Please call the front office to learn more about us, and about the dental implant procedures we perform at our friendly practice in <strong><i>Abbotsford, BC</i></strong>.</p>
<h3>Dental Implant Cost</h3>
<p>We will make every effort to provide you with the most accurate estimate of the expenses involved in your dental implant treatment. The exact cost of your dental implants will depend on a few factors, such as the number of implants you will be receiving, and if you will require bone grafting or extractions to prepare for your implant surgery. Your insurance coverage will also be a factor in estimating your cost. You will be charged separately for the services of the dentist providing your custom crowns. The best way to estimate your dental implant cost is to have a consultation with Dr. Esmail, where all of these factors can be evaluated.</p>
<p>While dental implants can seem like an expensive tooth replacement option, it’s hard to put a price on something as life-changing as replacement teeth that look, feel, and function like natural teeth. Regaining the ability to eat the foods you love knowing that your teeth appear natural and that the health of your jaw bone is being maintained as with natural teeth, has lifelong value for your budget and your health. Dental implants can last decades if cared for well, unlike other more traditional tooth restorations that must be replaced periodically. Some of the financial and personal advantages our implant patients enjoy include the following:</p>
<ul>
<li><strong>A lifelong tooth replacement.</strong> Unlike bridges and dentures, dental implants do not require replacement after a few years. With proper oral hygiene and regular dental appointments, dental implants can last a lifetime.</li>
<li><strong>Better oral health.</strong> Because dental implants are embedded in your jaw bone through osseointegration, they stimulate jaw bone growth, just like the roots of natural teeth. This stimulation helps to prevent the unsightly and destructive bone loss that is caused by prolonged tooth loss.</li>
<li><strong>Improved quality of life.</strong> With this restoration, patients can eat the foods they love and speak, eat, and smile with confidence. Dental implants can be a doorway to renewed self-confidence and peace of mind. This type of long-lasting oral rehabilitation is priceless.</li>
</ul>
<p>Dental implants truly change lives for the better! If you are interested in learning more about this long-lasting, cost-effective, and healthy way to replace damaged or missing teeth, please give Abbotsford Oral Surgery and Dental Implant Centre a call.</p>
<p>For more on the cost and value of dental implants at our trusted practice in <strong>Abbotsford, BC</strong>, see our <a href="/dental-implants-cost/">Dental Implant Cost page.</a></p>
<h3>Dental Implant FAQ</h3>
<p>Please see our Dental Implant FAQ <a href="/dental-implants-faq/">here.</a></p>
<h3>Dental Implant Instructions</h3>
<p>At Abbotsford Oral Surgery and Dental Implant Centre, our priority is to provide safe and comfortable procedures with optimal outcomes. Dr. Esmail has undergone extensive specialized training to provide you with excellent oral surgery care, but when it comes to getting the most out of your treatment, ensuring a quick recovery, and reducing the risk of complications, it is essential that you follow the proper self-care guidelines both before your oral surgery and after.</p>
<p>You will receive detailed care instructions from your oral surgeon and, for your convenience, we also include guidelines here. To best prepare you, we provide both pre- and post-operative instructions. The general pre-operative instructions will help you prepare for surgery by adjusting your diet, medications, and anything else needed to ensure you are completely ready. The post-operative instructions will be specific to your procedure and will provide instructions for your care after surgery. Always feel free to call our practice with any questions you may have, including questions about pre- and post-operative care. If anything is unclear to you, our knowledgeable staff is happy to offer guidance.</p>
<h3>Tooth Replacement Options</h3>
<p>Dr. Esmail of Abbotsford Oral Surgery and Dental Implant Centre is board-certified by both the Royal College of Dentists of Canada and the American Board of Oral and Maxillofacial Surgery. While our practice specializes in placing dental implants, we understand that dental implants may not be the right tooth replacement option for everyone. We proudly offer information on additional tooth replacement solutions on our website. If you would like to determine the right tooth restoration option for you, please contact our practice to schedule a consultation with the oral surgeon. Some of your options for tooth replacement may include the following:</p>
<ul>
<li>Dental Implants</li>
<li>Full-Arch Replacements</li>
<li>Bridges</li>
<li>Flippers/Metal Partials</li>
<li>Full or Partial Dentures</li>
</ul>
<p>If you have questions, or are ready to schedule your consultation with Dr. Esmail, please contact our <b><em>Abbotsford, BC</em>,</b> practice. We look forward to meeting with you to discuss the restoration of your smile!</p>
<p>Learn more about tooth replacement options on our <a href="/tooth-replacement-options/">Tooth Replacement Options page.</a></p>
<p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7397.767641803216!2d-122.31575674195159!3d49.03900507166582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5484354461678c0f%3A0xd39d9619933dd651!2sAbbotsford%20Oral%20Surgery%20and%20Dental%20Implant%20Centre%3A%20Dr.%20Nayeem%20Esmail%2C%20Inc.!5e0!3m2!1sen!2sca!4v1570469357043!5m2!1sen!2sca" width="1144" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/dental-implants-abbotsford-bc/">Dental Implants</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/procedure/dental-implants-abbotsford-bc/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Wisdom Teeth Removal</title>
		<link>https://www.abbotsfordoralsurgery.com/procedure/wisdom-teeth-removal-abbotsford-bc/</link>
				<comments>https://www.abbotsfordoralsurgery.com/procedure/wisdom-teeth-removal-abbotsford-bc/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/procedure/wisdom-teeth-removal-abbotsford-bc/</guid>
				<description><![CDATA[<p>Around the ages of 17 to 25, the third molars or wisdom teeth begin to emerge into the mouth. These additional molars often cause overcrowding and can develop into a host of oral health problems such as impaction (a tooth that is trapped under the gums), infection, pain, cysts, and more. Because these common wisdom teeth complications are more easily&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/wisdom-teeth-removal-abbotsford-bc/">Wisdom Teeth Removal</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>Around the ages of 17 to 25, the third molars or <strong><em>wisdom teeth</em></strong> begin to emerge into the mouth. These additional molars often cause overcrowding and can develop into a host of oral health problems such as impaction (a tooth that is trapped under the gums), infection, pain, cysts, and more. Because these common <b><i>wisdom teeth</b></i> complications are more easily treated in younger patients (or can be avoided completely), most dental professionals recommend that these molars be removed early as a preventative measure. Your family dentist can monitor you or your child’s wisdom teeth development with regular dental checkups. When wisdom teeth removal is recommended, you will be referred to a trusted specialist such as Dr. Esmail at Abbotsford Oral Surgery and Dental Implant Centre.  </p>
<h3>Wisdom Teeth Removal Process</h3>
<p>The beginning of the process for wisdom teeth extraction is your consultation appointment with Dr. Esmail. He will perform a thorough clinical exam and, if necessary, take diagnostic scans of your jaws and teeth. Dr. Esmail will evaluate the position of the wisdom teeth, present you with your options for extraction, and select the type of anesthesia that will maximize you/your child’s well-being during the procedure. Dr. Esmail has the training, license, and experience to safely and comfortably provide various types of anesthesia, and our staff is highly skilled in anesthesia techniques. Services are provided in an environment of optimum safety that utilizes modern monitoring equipment and is accredited and certified. At our practice in <b><i>Abbotsford, BC,</b></i> we offer several types of anesthesia for wisdom teeth removal: </p>
<ul>
<li><strong>Local anesthesia.</strong> Local anesthesia is applied directly to the surgical area. You will remain fully conscious throughout your procedure but will not feel any discomfort. Local anesthesia is used in all oral surgery procedures, often along with other forms of anesthesia.</li>
<li><strong>Intravenous (IV) sedation.</strong> In most cases, the removal of wisdom teeth is performed under intravenous sedation, and in some cases, local anesthesia. With this type of anesthesia, you will enter a sleep-like state with no awareness of the procedure as it occurs. You must not have anything to eat or drink (excluding prescription medications with a sip of water) for at least six hours before receiving IV sedation, and a responsible adult should drive you home to recover. </li>
</ul>
<p>The extraction of wisdom teeth is necessary when they are prevented from properly erupting within the mouth. They may grow sideways, partially emerge from the gum, and even remain trapped beneath the gum and bone. If you have not been screened for the presence of wisdom teeth, it is extremely important to do so during one of your regular dental checkups. Dr. Esmail has performed wisdom teeth extractions for many years. If you have questions about wisdom teeth removal at our <strong><em>Abbotsford, BC,</em></strong> practice, feel free to call our front office to speak with one of our knowledgeable administrators about scheduling a consultation with our oral surgeon. We appreciate the opportunity to care for you at Abbotsford Oral Surgery and Dental Implant Centre.</p>
<h3>Cost of Wisdom Teeth Removal</h3>
<p>Most dental professionals agree that early removal of wisdom teeth can save time and money by preventing expensive and uncomfortable orthodontic or surgical treatments caused by complications that can occur from <b><em>wisdom teeth</em></b>. For this reason, dental professionals recommend that wisdom teeth be removed before they start causing oral health problems. Early wisdom teeth removal is cost-effective for many reasons:</p>
<ul>
<li><strong>Prevention of oral health issues.</strong> Dental shifting, oral infections, gum tissue disease, and damage and crowding of nearby teeth are common oral health issues caused by untreated wisdom teeth. When wisdom teeth are extracted early, these problems can be avoided.</li>
<li><strong>Easier surgery/faster recovery.</strong> In general, when third molars are extracted at an earlier age, patients recover faster. This is because the molars are less developed and therefore easier for the oral surgeon to remove. Faster healing results in less time away from work or school.</li>
<li><strong>Less risk for complications.</strong> When <strong><i>wisdom teeth</i></strong> are left untreated, complications such as bleeding, swelling, and discomfort can arise. These costly and painful issues are eliminated when wisdom teeth are removed early. Less treatment translates into less overall cost. </li>
</ul>
<p>For wisdom teeth removal to be as accessible as possible for our patients, Abbotsford Oral Surgery and Dental Implant Centre accepts several insurance plans and payment methods. Our knowledgeable administrative staff will be happy to help you obtain maximum insurance coverage for your treatment.<br />
Learn more about the cost for wisdom teeth removal on our <a href="/wisdom-teeth-removal-cost/">Wisdom Teeth Removal Cost page.</a></p>
<h3>Wisdom Teeth Removal FAQ</h3>
<p>Please see our Wisdom Teeth Removal FAQ page <a href="/wisdom-teeth-removal-faq/">here.</a></p>
<h3>Impacted Wisdom Teeth</h3>
<p>Sometimes inadequate space in the mouth prevents teeth from erupting into the proper positions. When this occurs, your teeth are impacted. Wisdom teeth are the most commonly impacted teeth. When you are evaluated for impacted wisdom teeth, your care team at Abbotsford Oral Surgery and Dental Implant Centre will take special diagnostic images of your mouth and jaws to determine if there is enough room for your wisdom teeth to erupt without problems. If any of the molars are impacted, these images will also determine how difficult it will be to extract them. If any of your wisdom teeth are impacted, they will usually fall into one of these categories:</p>
<ul>
<li><strong>Soft Tissue Impaction</strong> occurs when there is not enough room around a tooth for proper cleaning. This can cause decay and gum disease.</li>
<li><strong>Partial Bony Impaction</strong> occurs when a wisdom tooth has partially erupted, but it cannot function properly in the mouth because it is positioned abnormally. Cleaning problems occur with this type of impaction as well.</li>
<li><strong>Complete Bony Impaction</strong> occurs when there is no space for a tooth to grow into the mouth, and it remains embedded in the jaw. This type of impaction requires treatment from an oral and maxillofacial surgeon because they can be difficult to remove. </li>
</ul>
<p>To protect your oral health and prevent future complications, extraction is often the best treatment for impacted wisdom teeth. If you have been recommended for wisdom teeth removal, or if you suspect that you or your child’s wisdom teeth are causing oral health issues, please contact Abbotsford Oral Surgery and Dental Implant Centre to schedule a consultation with Dr. Esmail. The procedure will be performed at our comfortable and state-of-the-art practice in Abbotsford, BC. </p>
<p>To learn more about impacted wisdom teeth, please visit our page on <a href="/impacted-wisdom-teeth/">Impacted Wisdom Teeth.</a></p>
<h3>Wisdom Teeth Removal Instructions</h3>
<p>At Abbotsford Oral Surgery and Dental Implant Centre, our priority is to provide safe and comfortable procedures with optimal outcomes. Dr. Esmail has undergone extensive specialized training to provide you with excellent oral surgery care, but when it comes to getting the most out of your treatment, ensuring a quick recovery, and reducing the risk of complications, it is important that you follow the proper self-care guidelines both before and after your oral surgery procedure. </p>
<p>You will receive detailed care instructions from your oral surgeon; for your convenience, we also include guidelines here. To best prepare you, we provide both pre- and post-operative instructions. The general pre-operative instructions will help you prepare for surgery by adjusting your diet, medications, and anything else needed to ensure you are completely ready. The post-operative instructions will be specific to your procedure and provide instructions for your care after surgery. Always feel free to call our practice with any questions you may have, including questions about pre- and post-operative care. If anything is unclear to you, our knowledgeable staff is happy to offer guidance.</p>
<p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7397.767641803216!2d-122.31575674195159!3d49.03900507166582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5484354461678c0f%3A0xd39d9619933dd651!2sAbbotsford%20Oral%20Surgery%20and%20Dental%20Implant%20Centre%3A%20Dr.%20Nayeem%20Esmail%2C%20Inc.!5e0!3m2!1sen!2sca!4v1570469357043!5m2!1sen!2sca" width="1144" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/wisdom-teeth-removal-abbotsford-bc/">Wisdom Teeth Removal</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/procedure/wisdom-teeth-removal-abbotsford-bc/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Full-Arch Restoration</title>
		<link>https://www.abbotsfordoralsurgery.com/procedure/full-arch-restoration/</link>
				<comments>https://www.abbotsfordoralsurgery.com/procedure/full-arch-restoration/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/procedure/full-arch-restoration/</guid>
				<description><![CDATA[<p>A full-arch replacement is a revolutionary tooth restoration treatment that completely restores an entire arch of your teeth. We find that many of our patients who require replacements for an entire upper or lower arch of teeth are good candidates for full-arch replacement. During your consultation with Dr. Esmail at Abbotsford Oral Surgery and Dental Implant Centre, you and your&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/full-arch-restoration/">Full-Arch Restoration</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>A full-arch replacement is a revolutionary tooth restoration treatment that completely restores an entire arch of your teeth. We find that many of our patients who require replacements for an entire upper or lower arch of teeth are good candidates for full-arch replacement. During your consultation with Dr. Esmail at Abbotsford Oral Surgery and Dental Implant Centre, you and your doctor will decide if this is the best treatment option for you. Your new, fully functional and natural-looking teeth will be fixed in place and therefore permanent and secure, freeing you from the inconvenience of other replacement solutions such as removable dentures. Furthermore, like individual dental implants, a full-arch replacement can prevent bone resorption and other oral health problems while being more cost-effective and requiring less surgery than individual implants.   </p>
<p>The full-arch replacement treatment is a collaboration between a restorative dentist and an oral surgeon. Your first visit to our practice will be your consultation. To determine the ideal placement of your new implants, we will use advanced diagnostic tools and evaluate your oral health condition. You and Dr. Esmail will form a unique treatment plan for your dental restoration, including your choice of anesthesia. In most cases, your next appointment will be your surgical procedure. If necessary, your oral surgeon will remove any teeth that are remaining in the mouth, and then the implant posts will be strategically placed into your jaw bone. The surgical sites will be stitched closed, and you will rest comfortably until the effects of the anesthesia have worn off. </p>
<p>The healing period will take place over the next several weeks to allow time for the implant posts to fuse with the jaw bone. This creates a stable and healthy foundation for your new teeth. While your implant sites are healing, you will wear a natural-looking temporary denture. Once the implant sites have healed completely and the implants have successfully fused with your jaw bone, your permanent prosthesis will be placed. The final dental prosthesis (fixed denture) will be designed and completed by the restorative dentist. </p>
<h2>What are the benefits of full-arch restoration?</h2>
<p>Full-arch replacement is the most cost-effective dental implant-based treatment option. It offers patients the permanency and health benefits of having individual dental implants with a significantly reduced healing time, allowing patients to enjoy their new, fully-functional smile much faster. </p>
<p>This dental implant-based surgery is popular for many reasons:</p>
<ul>
<li>Less post-operative discomfort, less swelling, and less bruising</li>
<li>Reduced healing time</li>
<li>Permanent, fully functional, and stable replacement for multiple teeth</li>
<li>Prevents bone loss and preserves oral health</li>
</ul>
<p>The beautiful result of this implant-based tooth restoration will be a new smile, custom-designed for your mouth, that looks and functions just like healthy, natural teeth. You will once again be able to enjoy the foods that you love and speak and smile with confidence. </p>
<h2>Full-Arch Restoration in Abbotsford, BC</h2>
<p>At Abbotsford Oral Surgery and Dental Implant Centre, our highest goal is to provide you with a smile that looks and functions like natural, healthy teeth. If you are looking to restore your oral function and smile with a full-arch restoration or other tooth restoration treatment, or have questions about any of our procedures, please contact us to schedule a consultation. We look forward to seeing you smile again!</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/full-arch-restoration/">Full-Arch Restoration</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/procedure/full-arch-restoration/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Bone Grafting</title>
		<link>https://www.abbotsfordoralsurgery.com/procedure/bone-grafting/</link>
				<comments>https://www.abbotsfordoralsurgery.com/procedure/bone-grafting/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/procedure/bone-grafting/</guid>
				<description><![CDATA[<p>Tooth loss can cause the jaw bone to diminish because the root of a natural tooth must be present to stimulate the bone and keep it healthy. Other causes of bone loss can be inadequate bone structure due to previous extractions, gum disease, or injuries. Regardless of the cause, bone loss can make the successful placement of dental implants difficult&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/bone-grafting/">Bone Grafting</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>Tooth loss can cause the jaw bone to diminish because the root of a natural tooth must be present to stimulate the bone and keep it healthy. Other causes of bone loss can be inadequate bone structure due to previous extractions, gum disease, or injuries. Regardless of the cause, bone loss can make the successful placement of dental implants difficult or impossible. Fortunately, advancements in medical science have allowed us the ability to grow bone where it is needed, which has opened the option of dental implants to more patients. The bone utilized in bone grafting techniques for oral surgery can be obtained from a tissue bank, or your own bone is taken from another part of your body. If you are having dental implants placed and are suffering from bone loss in your jaw, you may be a candidate for having bone tissue grafted into the implant area to ensure stable and permanent placement. </p>
<p>Our Abbotsford, BC, practice is proud to offer the full scope of oral surgery procedures, including various bone grafting procedures. While many patients find they require a bone grafting procedure for the successful placement of dental implants, bone grafts can also be used in the treatment of other oral and maxillofacial conditions that we address at Abbotsford Oral Surgery and Dental Implant Centre.</p>
<h2>Types of Bone Grafting Procedures</h2>
<p>Bone grafting is a common first step for dental implant patients. In bone grafting procedures, a solution of granulated bone material or other healing agents will be applied in the areas of your jaw that need to be reinforced, stimulating new, healthy bone growth at the site of future dental implant placement. At Abbotsford Oral Surgery and Dental Implant Centre, we perform several types of bone grafting treatments to meet the individual needs of our patients. We also offer soft tissue grafting to treat periodontal disease and gum recession and to help stop bone loss and further recession of the gums. </p>
<p>Some of the grafting procedures we offer include</p>
<ul>
<li><strong>Ridge Augmentation.</strong> The alveolar ridge bone is the bone that surrounds and supports the teeth. If this bone is not healthy or dense enough to support dental implants, Dr. Esmail may recommend a ridge augmentation. During ridge augmentation, the alveolar ridge is surgically split, and bone graft material is placed to stimulate bone growth.</li>
<li><strong>Sinus Lift.</strong> If there is not enough bone present between the upper jaw and the sinus cavity for successful dental implant placement, your oral surgeon may recommend a sinus lift procedure. For this procedure, a bone graft is placed in the jaw below the sinus membrane. This graft integrates over several months to create a more solid foundation for an implant.</li>
<li><strong>Socket preservation.</strong> Oral and maxillofacial surgeons can perform a special bone grafting procedure called socket preservation that protects the area of the jaw where a dental implant will eventually be placed. This procedure minimizes the bone loss that occurs after an extraction. To complete a socket preservation procedure, after the tooth has been extracted, your oral surgeon fills the tooth socket with bone grafting material. When the grafted site has healed, a dental implant is placed to restore the missing tooth. Sometimes your surgeon will place the dental implant at the same time the graft is placed, allowing you to enjoy your new smile faster. </li>
<li><strong>Soft Tissue Grafting.</strong> If a patient is suffering from periodontal disease and gum recession, a soft tissue graft may be recommended so that the gum line is uniform around a dental implant for a more natural appearance. For this procedure, soft tissue grafts (small pieces of oral tissue harvested from other areas of the mouth) will be surgically implanted in the affected area. There are more than just aesthetic improvements from this procedure. Soft tissue grafts can help stop bone loss and additional recession of the gums and can reduce root sensitivity.</li>
<li><strong>Nerve Repositioning.</strong> The nerve that gives feeling to the lower lip and chin is called the inferior alveolar nerve. If this nerve is susceptible to damage during a dental implant procedure, the nerve must be accessed through the jaw bone and then repositioned. Bone grafting material is added to the area after a nerve repositioning procedure. Nerve repositioning carries some risk as there is almost always some numbness of the lower lip and jaw area after the surgery. This typically dissipates, but it may be permanent. Most of the time, other options are considered first. Nerve repositioning surgeries are performed under IV sedation or general anesthesia.</li>
</ul>
<p>Bone grafting can repair implant sites with inadequate bone structure so that more patients can restore their smile with dental implants — and once the restoration site has healed, the new dental implant will help maintain the jaw bone over time. Because each bone grafting option has its own risks and benefits, Dr. Esmail will evaluate your case with extreme care to determine which type of bone grafting technique is best suited to your needs. Dr. Esmail is extensively trained to provide the full scope of bone grafting procedures to ensure that your replacement tooth or teeth will look, feel, and function just like natural, healthy teeth. </p>
<h2>Bone Morphogenetic Proteins</h2>
<p>Sometimes, bone grafting techniques utilize synthetic materials as a substitute for real bone, which eliminates the need to source bone from other graft sites on your body or use banked bone. These materials are safe and proven alternatives and include bone morphogenetic proteins, or BMPs, which we use in our practice. BMPs are proteins that are naturally produced in the body for creating new cartilage and bone. This material can be used to increase the jaw bone support required for durable dental implant placement. During BMP treatment, a protein solution is added to a sponge-like material that is placed over the area of bone in need of treatment to stimulate and support bone growth. After the new bone has finished growing into the graft area, your jaw bone will be ready to support your new dental implant.</p>
<h2>Bone Grafting in Abbotsford, BC</h2>
<p>Bone grafting can offer a stable foundation of healthy bone for the long-term stability of your dental implants. If you have any questions or would like to schedule a consultation, please contact Abbotsford Oral Surgery and Dental Implant Centre. Dr. Esmail will be happy to discuss your bone grafting options with you during your consultation and will develop a treatment plan for you based on your unique needs.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/bone-grafting/">Bone Grafting</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/procedure/bone-grafting/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Tooth Extraction</title>
		<link>https://www.abbotsfordoralsurgery.com/procedure/general-tooth-extraction/</link>
				<comments>https://www.abbotsfordoralsurgery.com/procedure/general-tooth-extraction/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/procedure/general-tooth-extraction/</guid>
				<description><![CDATA[<p>Gum disease, tooth decay, or the fracture of a tooth can all be reasons why a tooth is recommended for removal. To determine if a tooth needs to be removed, you will need to schedule a consultation with Dr. Esmail. During your consultation, you will have an oral exam and an X-ray evaluation, and you and Dr. Esmail will discuss&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/general-tooth-extraction/">Tooth Extraction</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>Gum disease, tooth decay, or the fracture of a tooth can all be reasons why a tooth is recommended for removal. To determine if a tooth needs to be removed, you will need to schedule a consultation with Dr. Esmail. During your consultation, you will have an oral exam and an X-ray evaluation, and you and Dr. Esmail will discuss the best options available to you for tooth restoration — we offer several tooth replacement options at our practice. Your anesthesia options for your extraction will be discussed at this consultation as well. Local anesthesia is most commonly used for tooth extractions, so you will be conscious but will not feel any discomfort during the procedure. For multiple extractions or complicated extractions, your oral surgeon may recommend intravenous (IV) sedation for your procedure.</p>
<h2>Why do I need to have my tooth extracted?</h2>
<p>Whenever possible, dental professionals will work hard to repair a damaged tooth with a crown, filling, or root canal, but if the damage is too severe or threatens the health of the surrounding teeth, extraction may be the best course of action. </p>
<p>There are several reasons your tooth may need to be extracted, including </p>
<ul>
<li>Oral health problems are arising because your jaw is too small to accommodate all your teeth, or your teeth are too crowded</li>
<li>Previous restorative treatments, such as a root canal, have failed to save the tooth</li>
<li>Your tooth was injured and/or broken and cannot be restored</li>
<li>Your tooth is impacted and is unable to erupt from the gums properly (a common occurrence with wisdom teeth)</li>
<li>Advanced gum disease (periodontal disease)</li>
<li>Severe infection or tooth decay</li>
<li>Teeth that are malformed</li>
<li>As part of a tooth restoration treatment such as the placement of dental implants</li>
</ul>
<p>Each case is different, which is why we will create an individualized treatment plan for your extraction during your consultation. If you are recommended for an extraction, your tooth/teeth will be removed in the comfort of our Abbotsford, BC, practice by our experienced oral surgeon. </p>
<h2>Tooth Extraction in Abbotsford, BC</h2>
<p>We are proud to provide the full scope of oral surgery care and tooth replacement solutions to a wide region in the lower British Columbia mainland in the comfort and safety of our practice in Abbotsford, BC. As an experienced oral surgeon, Dr. Esmail can help you restore your missing tooth or teeth after extraction. If you have questions about tooth extraction or any of our procedures, please contact us to schedule a consultation. We appreciate the opportunity to restore your smile at Abbotsford Oral Surgery and Dental Implant Centre.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/general-tooth-extraction/">Tooth Extraction</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/procedure/general-tooth-extraction/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Impacted Tooth Exposure</title>
		<link>https://www.abbotsfordoralsurgery.com/procedure/impacted-tooth-exposure/</link>
				<comments>https://www.abbotsfordoralsurgery.com/procedure/impacted-tooth-exposure/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/procedure/impacted-tooth-exposure/</guid>
				<description><![CDATA[<p>An impacted tooth is a tooth that is stuck beneath the gums and cannot erupt into the mouth the way it should. Impacted teeth can cause an unstable bite and lead to costly orthodontic or restorative work later in life. The Canadian Association of Orthodontists recommends that a dental examination with diagnostic images be performed on children by the age&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/impacted-tooth-exposure/">Impacted Tooth Exposure</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>An impacted tooth is a tooth that is stuck beneath the gums and cannot erupt into the mouth the way it should. Impacted teeth can cause an unstable bite and lead to costly orthodontic or restorative work later in life. The Canadian Association of Orthodontists recommends that a dental examination with diagnostic images be performed on children by the age of seven to determine if there are problems with the eruption of the adult teeth. </p>
<p>Patients most commonly develop problems with impacted third molar (wisdom) teeth. When these teeth get trapped beneath gum tissue and bone, they can develop painful infections and other problems. Since there is rarely a functional need for wisdom teeth, extraction by an oral surgeon is usually recommended. Other teeth, such as the canines, are generally not extracted but are treated orthodontically or surgically so that they can emerge successfully into the mouth. </p>
<h2>Canine Impaction</h2>
<p>The canine teeth are the last primary (permanent) teeth to come in, emerging at about the age of 11 or 12, and they are very important for a proper bite. If an impacted canine is diagnosed, an orthodontist and oral surgeon will work together to help the canine tooth erupt into its proper position in the dental arch. </p>
<p>Canine teeth may become impacted due to several reasons:</p>
<ul>
<li>The baby teeth do not fall out and block the eruption of the canines.</li>
<li>There’s not enough room in the mouth for them to emerge.</li>
<li>Unusual growths block the route of the tooth. </li>
<li>Dental crowding due to extra teeth (this is rare).</li>
</ul>
<p>Your child’s dentist will recommend further treatment if an impacted tooth is suspected. Dental professionals usually recommend that an impacted canine be surgically exposed as early as possible so that it has the best chance to grow into the dental arch on its own. As with the wisdom teeth, without treatment, the roots of impacted canines can fuse with the jaw bone over time. This makes it less likely that these important teeth will emerge properly.</p>
<h2>Exposure and Bonding for Impacted Canines</h2>
<p>Most commonly, when there is an impacted canine tooth (also called an eyetooth if located in the upper jaw), orthodontic braces will be used to provide room for the tooth to move into its proper position. Once the space is ready, the orthodontist will refer the patient to the oral surgeon to have the impacted canine exposed and bracketed to help align the tooth as it erupts. </p>
<p>The surgery to expose and bracket an impacted tooth is a simple surgical procedure. For most patients, it is performed with intravenous (IV) sedation. This can be discussed in detail at your pre-operative consultation with your oral surgeon. During the procedure, the gum tissue will be lifted to expose the tooth underneath. If there is a baby tooth present, it will be removed at the same time. Once the tooth is exposed, the oral surgeon will bond an orthodontic bracket to the exposed tooth. The bracket will have a small chain attached that will be used to guide the tooth into position over a few months’ time.</p>
<h2>Impacted Tooth Exposure in Abbotsford, BC</h2>
<p>At our practice, we provide oral surgery services to patients of all ages, including young children and teens. We strive to create personalized treatment plans for each of our patients and provide them with the most comfortable experience possible. If you or your child requires the surgical treatment of an impacted tooth, please contact Abbotsford Oral Surgery and Dental Implant Centre to schedule a consultation with Dr. Esmail. We look forward to caring for you and your family at our trusted Abbotsford, BC, practice.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/impacted-tooth-exposure/">Impacted Tooth Exposure</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/procedure/impacted-tooth-exposure/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Oral Pathology</title>
		<link>https://www.abbotsfordoralsurgery.com/procedure/oral-pathology/</link>
				<comments>https://www.abbotsfordoralsurgery.com/procedure/oral-pathology/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/procedure/oral-pathology/</guid>
				<description><![CDATA[<p>Oral pathology refers to diseases that can develop inside your mouth, salivary glands, or jaws. The inside of the mouth is lined with a special type of skin called the mucosa that should be smooth and coral pink in color. Any change in the appearance of the mucosa could be a warning sign for a pathological process. When oral pathologies&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/oral-pathology/">Oral Pathology</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p><strong>Oral pathology</strong> refers to diseases that can develop inside your mouth, salivary glands, or jaws. The inside of the mouth is lined with a special type of skin called the mucosa that should be smooth and coral pink in color. Any change in the appearance of the mucosa could be a warning sign for a pathological process. When oral pathologies or diseases are caught early, they are much more easily treated, so it’s important to be aware of the condition of your mouth. We recommend performing an oral self-examination monthly. Although most oral pathologies are benign, it is always important to get proper evaluation and treatment of any oral abnormality or lesion by an expert. Even if an oral lesion is not painful, it should still be evaluated and properly treated. Oral and maxillofacial surgeons are the most qualified medical providers to diagnose and treat oral pathologies. </p>
<p>Common oral pathologies include the following:</p>
<ul>
<li>Ulcers</li>
<li>Tonsil stones</li>
<li>Salivary gland diseases</li>
<li>Herpes (cold sores)</li>
<li>Canker sores</li>
<li>Cysts and tumors</li>
<li>Infections</li>
<li>Oral cancers</li>
</ul>
<p>If your oral surgeon suspects that you are suffering from an oral pathology, you may have a biopsy during your examination. A biopsy is a small sample of tissue that is removed and sent to a lab to confirm a diagnosis. Most biopsies can be performed in our practice with local anesthesia. The appropriate next steps will depend on the diagnosis. Dr. Esmail will discuss his recommendations with you to personalize a treatment plan for the restoration of your oral health. </p>
<h2>Signs of Oral and Maxillofacial Diseases</h2>
<p>If you notice any abnormality in your mouth, including the following, call our practice right away for a pathology screening:</p>
<ul>
<li>Red or white patches in the mouth</li>
<li>A sore that fails to heal and bleeds easily</li>
<li>A lump or thickening on the skin lining the inside of the mouth</li>
<li>Chronic sore throat or hoarseness</li>
<li>Difficulty chewing or swallowing</li>
</ul>
<p>Oral diseases range from simple canker sores to more serious issues such as oral cancer. It is important to note that oral pain does not always occur with a pathological process, particularly in the case of oral cancer. </p>
<h2>Oral Pathology in Abbotsford, BC</h2>
<p>Most changes in your mucosa and oral cavity will not be serious, but any unusual changes should be examined by your oral surgeon promptly. Early identification and treatment of pathologies are important. Your overall health is our priority, so please contact Abbotsford Oral Surgery and Dental Implant Centre as soon as possible if you suspect an oral pathology. We will make every effort to see you promptly for an evaluation of the area and a recommendation for next steps.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/oral-pathology/">Oral Pathology</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/procedure/oral-pathology/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Orthognathic Surgery</title>
		<link>https://www.abbotsfordoralsurgery.com/procedure/orthognathic-surgery/</link>
				<comments>https://www.abbotsfordoralsurgery.com/procedure/orthognathic-surgery/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/procedure/orthognathic-surgery/</guid>
				<description><![CDATA[<p>When the jaws don’t meet correctly, or there are other abnormalities with the jaw (often caused by injury or birth defects), patients may need corrective jaw surgery, also called orthognathic surgery. Jaw misalignments can affect chewing function, speech, and long-term oral health and appearance. If the issue is not very severe, corrections can be made with the use of orthodontics.&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/orthognathic-surgery/">Orthognathic Surgery</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>When the jaws don’t meet correctly, or there are other abnormalities with the jaw (often caused by injury or birth defects), patients may need <strong>corrective jaw surgery</strong>, also called <strong>orthognathic surgery</strong>. Jaw misalignments can affect chewing function, speech, and long-term oral health and appearance. If the issue is not very severe, corrections can be made with the use of orthodontics. When the issue is more serious, it’s time for an oral and maxillofacial surgeon to help. </p>
<p>If you are suffering from any of these difficulties, you may be a candidate for orthognathic surgery and should be evaluated by Dr. Esmail:</p>
<ul>
<li>Difficulty in chewing, biting, or swallowing</li>
<li>Speech problems</li>
<li>Chronic jaw or TMJ pain</li>
<li>Open bite</li>
<li>Protruding jaw</li>
<li>Breathing problems</li>
</ul>
<p>If corrective jaw surgery is recommended for you, you will be in excellent hands with Dr. Esmail. Oral and maxillofacial surgeons are uniquely qualified to perform a jaw surgery. Before your treatment, you will have a consultation with a complete examination and X-rays. The doctor will answer any questions you may have and will fully inform you about your condition and your options for care. </p>
<h2>Corrective Jaw Surgery Process</h2>
<p>Dr. Esmail with plan your proposed treatment using state-of-the-art diagnostic imaging and will work closely with your dentist and orthodontist during your treatment. This treatment can be life-changing for our patients, offering an opportunity for a healthy, attractive, and functional jaw structure. The first step in the process is to work with your orthodontist, who will prepare your mouth for the procedure by using braces to align your teeth properly. The orthodontic phase of treatment usually lasts 6 to 18 months. The next step is the surgical alignment of your jaws. This procedure is performed by your oral surgeon in a hospital setting and can last between one to four hours. Hospital stays of one to three days are normal. </p>
<p>The recovery can be somewhat long, as patients are typically required to be off work or school from two weeks to one month after surgery. It may take a couple of months to return to normal chewing function, and full function may take up to one year. Regular appointments with your oral surgeon are required for up to two months after surgery, and braces are maintained for six to twelve months after surgery. </p>
<p>Orthognathic surgery is an incredibly rewarding process for our patients, offering remarkable positive transformations and improvements in their daily lives. Vastly improved chewing function, speech, and breathing can all be achieved, as well as more balanced facial aesthetics.</p>
<h2>Orthognathic Surgery in Abbotsford, BC</h2>
<p>The surgical team at Abbotsford Oral Surgery and Dental Implant Centre is expertly trained in orthognathic surgery techniques. If you believe you may require orthognathic surgery/corrective jaw surgery, or if this type of surgery has been recommended for you, we encourage you to schedule a consultation with Dr. Esmail to be evaluated for this life-changing treatment.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/orthognathic-surgery/">Orthognathic Surgery</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/procedure/orthognathic-surgery/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Pre-Prosthetic Surgery</title>
		<link>https://www.abbotsfordoralsurgery.com/procedure/pre-prosthetic-surgery/</link>
				<comments>https://www.abbotsfordoralsurgery.com/procedure/pre-prosthetic-surgery/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/procedure/pre-prosthetic-surgery/</guid>
				<description><![CDATA[<p>Pre-prosthetic surgery is an oral surgery treatment that is commonly recommended to ensure that a patient’s partial or complete denture is comfortable and fits properly in the mouth. It’s no fun when dentures are uncomfortable or slip out! Dentures sit on a bony ridge in the mouth called the alveolar ridge that normally supports teeth. For denture wearers, it is&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/pre-prosthetic-surgery/">Pre-Prosthetic Surgery</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p><strong>Pre-prosthetic surgery</strong> is an oral surgery treatment that is commonly recommended to ensure that a patient’s partial or complete denture is comfortable and fits properly in the mouth. It’s no fun when dentures are uncomfortable or slip out! </p>
<p>Dentures sit on a bony ridge in the mouth called the alveolar ridge that normally supports teeth. For denture wearers, it is crucial that this ridge is the proper shape and size to support the denture comfortably. For the best fit of a denture, the bony ridge may need to be smoothed or reshaped, or excess bone may need to be removed. Dr. Esmail is an expert at pre-prosthetic surgery techniques to ensure that his patients have a comfortable and secure fit for their dentures. Pre-prosthetic surgery is performed in the comfort of our practice in Abbotsford, BC. </p>
<h2>Why have pre-prosthetic surgery?</h2>
<p>Even after multiple fittings, some patients find that their dentures do not fit well, and daily activities such as eating and speaking have become painful or embarrassing. In these cases, the alveolar ridge or jaw shape may require surgical correction. Factors such as genetics and tooth loss may have affected the shape and size of your alveolar ridge, and there may be other correctable oral issues that are causing your discomfort. Pre-prosthetic surgery can vastly improve your well-being by taking the discomfort out of important daily activities such as chewing and speaking. </p>
<h2>Types of Pre-Prosthetic Surgery</h2>
<p>The inconsistencies in your jaw/alveolar ridge that need to be corrected, and the type of dentures or other prosthetic devices you have or will be receiving, will affect what type of pre-prosthetic surgery procedures you require. The following procedures are some of the pre-prosthetic surgeries we offer to prepare your mouth for well-fitting prosthetic devices:</p>
<ul>
<li>Bone smoothing and/or reshaping</li>
<li>Bone ridge reduction/alveolar ridge reduction</li>
<li>Removal of excess bone or soft (gum) tissue</li>
<li>Bone or tissue grafting procedures</li>
<li>Exposure of impacted teeth</li>
</ul>
<p>These procedures have very few risks for the patient and can usually be performed from the comfort of our Abbotsford, BC practice. Your needs will be discussed in detail when you have your consultation for pre-prosthetic surgery with Dr. Esmail.  </p>
<h2>Pre-Prosthetic Surgery in Abbotsford, BC</h2>
<p>When dentures are uncomfortable or ill-fitting, it can affect so many aspects of your life. If you have dentures that are bothering you or you are due to receive dentures and have been recommended for pre-prosthetic surgery, please give Abbotsford Oral Surgery and Dental Implant Centre a call. Dr. Esmail will discuss your case with you and create a plan for appropriate treatment. If you are investigating other options, such as dental implants or other tooth replacement options, the doctor will be happy to discuss these options with you as well at that time.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/pre-prosthetic-surgery/">Pre-Prosthetic Surgery</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/procedure/pre-prosthetic-surgery/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
		<item>
		<title>Facial Trauma</title>
		<link>https://www.abbotsfordoralsurgery.com/procedure/facial-trauma/</link>
				<comments>https://www.abbotsfordoralsurgery.com/procedure/facial-trauma/#respond</comments>
				<pubDate>Thu, 13 Sep 2018 07:00:00 +0000</pubDate>
		<dc:creator><![CDATA[nuvopet]]></dc:creator>
		
		<guid isPermaLink="false">http://www.abbotsfordoralsurgery.com/procedure/facial-trauma/</guid>
				<description><![CDATA[<p>Facial trauma injuries are always unexpected and stressful — thankfully, there are many advanced treatments available to restore function and aesthetics when an injury has occurred. As an oral and maxillofacial surgeon, Dr. Esmail is the ideal medical professional to treat your facial injury case and ensure optimal healing for your injuries with the least amount of visible scarring. He&#8230;</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/facial-trauma/">Facial Trauma</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></description>
								<content:encoded><![CDATA[<p>Facial trauma injuries are always unexpected and stressful — thankfully, there are many advanced treatments available to restore function and aesthetics when an injury has occurred. As an oral and maxillofacial surgeon, Dr. Esmail is the ideal medical professional to treat your facial injury case and ensure optimal healing for your injuries with the least amount of visible scarring. He will do everything he can to attend to your injury case as quickly as possible. During his training, he completed a hospital-based surgical residency, allowing him to address all the issues that arise in a trauma setting. </p>
<p>Dr. Esmail is uniquely qualified to manage and treat facial trauma, with the goal of treating your injuries in a way that ensures your facial appearance is minimally affected. Some of the types of facial trauma we treat at Abbotsford Oral Surgery and Dental Implant Centre include the following: </p>
<ul>
<li>Facial lacerations</li>
<li>Intraoral lacerations</li>
<li>Avulsed (knocked out) teeth</li>
<li>Fractured facial bones</li>
<li>Fractured jaws </li>
</ul>
<p>Facial injuries include soft tissue injuries (skin and gums), bone injuries (fractures), and injuries to sensitive regions such as the eyes, facial nerves, or the salivary glands. For many of the less severe injuries, we provide treatment in our practice. However, for more extreme cases, or injuries that occur after-hours, you should go immediately to the emergency room for treatment. </p>
<p>If your tooth is knocked out, place it in salt water or milk and call us as soon as possible — the sooner the tooth is re-inserted into its socket, the better the chance it will survive. Never attempt to wipe off the tooth as remnants of the ligament that hold the tooth in the jaw are vital to re-inserting it successfully. </p>
<h2>Facial Trauma Treatment in Abbotsford, BC</h2>
<p>Oral and maxillofacial surgeons like Dr. Esmail are specialists in the treatment of facial injuries. This type of surgeon has experience in emergency care as well as long-term reconstruction and healing. Call Abbotsford Oral Surgery and Dental Implant Centre as soon as you can after an injury at (604) 504-7522, but for severe injuries and injuries that occur after hours, please go to the emergency room where there will be an oral and maxillofacial surgeon on call to provide treatment. If you have questions about facial trauma treatment and prevention, please call us. We appreciate the opportunity to care for you at Abbotsford Oral Surgery.</p>
<p>The post <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com/procedure/facial-trauma/">Facial Trauma</a> appeared first on <a rel="nofollow" href="https://www.abbotsfordoralsurgery.com">Abbotsford Oral Surgery and Dental Implant Centre</a>.</p>
]]></content:encoded>
							<wfw:commentRss>https://www.abbotsfordoralsurgery.com/procedure/facial-trauma/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
							</item>
	</channel>
</rss>
