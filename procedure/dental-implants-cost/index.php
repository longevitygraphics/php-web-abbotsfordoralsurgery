<?php
$page_title       = 'Dental Implant Cost in Abbotsford, BC';
$doc_title        = 'Dental Implant Cost';
$meta_description = 'The cost of dental implant treatment varies with everyone, but we can discuss the contributing factors at your consultation in Abbotsford, BC.';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="procedure-template-default single single-procedure postid-99084 single-format-standard custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/MedicalProcedure">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="name">Dental Implant Cost</h1>
            </header>
            <div class="breadcrumb">You are here: <span class="breadcrumb-link-wrap" itemprop="url"><a
                            class="breadcrumb-link" href="../../index.html" itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                <span class="breadcrumb-link-wrap" itemprop="url"><a class="breadcrumb-link" href="../index.html"
                                                                     itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Procedures</span></a><meta
                            itemprop="position" content="2"></span> <span aria-label="breadcrumb separator">/</span>
                Dental Implant Cost
            </div>
            <div class="row no-gutters">
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implants-bone-graft-donna-abbotsford-bc/index.html"
                       title='Bone Grafting and Dental Implants Restored Donna’s Smile'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/donna-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Donna the dental implants patient in Abbotsford, BC'
                             title='Bone Grafting and Dental Implants Restored Donna’s Smile'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implant-guya-abbotsford-bc/index.html"
                       title='Guya Chose a Dental Implant to Replace Her Tooth'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/guya-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Guya the dental implants patient in Abbotsford, BC'
                             title='Guya Chose a Dental Implant to Replace Her Tooth'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implant-tooth-extraction-anna-abbotsford-bc/index.html"
                       title='Anna Needed an Extraction and Dental Implant'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/anna-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Anna the dental implants patient in Abbotsford, BC'
                             title='Anna Needed an Extraction and Dental Implant'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/full-arch-restoration-les-abbotsford-bc/index.html"
                       title='Les Underwent a Full-Arch Restoration'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/les-full-arch-restoration-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Les the full-arch patient in Abbotsford, BC'
                             title='Les Underwent a Full-Arch Restoration'/>
                    </a>
                </div>
            </div>
            <div class="content-sidebar-wrap">
                <main class="content" id="genesis-content">
                    <article
                            class="post-99084 procedure type-procedure status-publish format-standard procedure-types-hidden entry secondary_color">
                        <div class="entry-content" itemprop="description"><p>At Dr. Esmail’s practice, we provide the
                                finest oral surgery care at competitive prices for Abbotsford, BC, and the surrounding
                                communities. To make payment as convenient as possible, we accept multiple payment
                                methods and insurance plans. We will make every effort to provide you with the most
                                accurate estimate of the expenses involved in your dental implant treatment. </p>
                            <p>The exact cost of your dental implant placement will depend on a few factors, such as the
                                number of implants you will be receiving and if you will require bone grafting or
                                extractions to prepare for your implant surgery. Your insurance coverage will also be a
                                factor in estimating cost. You will be charged separately for the services of the
                                dentist providing your custom crowns. We will assist you in estimating what your
                                payments will be once we evaluate how these factors affect your case. The very best way
                                to estimate your dental implant cost is to have a consultation with Dr. Esmail. </p>
                            <h2>Your Dental Implant Investment</h2>
                            <p>While dental implants can seem like an expensive tooth replacement option, it’s hard to
                                put a price on something as life-changing as replacement teeth that look, feel, and
                                function like natural teeth. There is incredible life-long value in regaining the
                                ability to eat your favorite foods knowing that your teeth appear natural and the health
                                of your jaw bone is being maintained as it would be with natural teeth. Dental implants
                                can last decades if cared for well, unlike more traditional tooth restorations that need
                                to be replaced periodically. Some of the financial and personal advantages our implant
                                patients enjoy include the following:</p>
                            <ul>
                                <li><strong>A lifelong tooth replacement.</strong> Unlike bridges and dentures, dental
                                    implants do not require replacement after a few years. With proper oral hygiene and
                                    regular dental appointments, dental implants can last a lifetime.
                                </li>
                                <li><strong>Better oral health.</strong> Because dental implants are embedded in your
                                    jaw bone through osseointegration, they stimulate the jaw bone to help prevent bone
                                    loss.
                                </li>
                                <li><strong>Improved quality of life.</strong> Dental implant patients can eat the foods
                                    they love and speak, eat, and smile with confidence. Dental implants can be a
                                    doorway to renewed self-confidence and peace of mind. This type of long-lasting oral
                                    rehabilitation is priceless.
                                </li>
                            </ul>
                            <p>Dental implants change lives for the better! If you are interested in learning more about
                                this long-lasting, cost-effective, and healthy way to replace damaged or missing teeth,
                                please give Abbotsford Oral Surgery and Dental Implant Centre a call. Our friendly
                                administrative team will schedule a consultation appointment for you with the oral
                                surgeon. </p>
                            <h2>Dental Implant Cost in Abbotsford, BC</h2>
                            <p>Dr. Esmail is highly experienced at dental implant procedures of all kinds, and we
                                specialize in this surgery at our practice. If you would like to determine what your
                                dental implant treatment will cost, please call our practice in Abbotsford, BC, to
                                schedule a consultation appointment.</p>
                        </div>
                    </article>
                    <div class="row cta-footer ">
                        <div class="col-md-3">
                            <a href="../../contact-us/index.html">
                                <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                                </button>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <p>
                                As a patient of our office, we want it to be as easy as possible for you to visit our
                                team for care. You can request an appointment by filling out our online form. </p>
                        </div>
                    </div>
                </main>
            </div>
			<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
            <style type="text/css">
                .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
                    background-color: #1171AF !important;
                    color: #fff !important;
                }

                .secondary_color {
                    background-color: #46AD4C;
                }

                .highlight_color {
                    background-color: #32C5F4 !important;
                }

                ;
                .site-inner {
                    background-color: #1171AF !important;
                }

                /*Main Homepage*/
                .gradient {
                    background: #00AEEF; /* Old browsers */
                    background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
                    background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
                    background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
                    background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
                    background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
                    background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
                    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
                    Padding: 10%;
                }

                .home input {
                    color: rgba(17, 113, 175, 1) !important;
                }

                /*body.custom-background {
				  background-color: rgba(17,113,175,1) !important;
				}*/

                /*Menu*/
                .nav-primary .sub-menu a {
                    background-color: #1171AF;
                    border-color: #fff;
                    color: #fff !important;
                }

                .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
                    background-color: #1171AF;
                }

                /*Add primary color to the number CTA*/
                .callus a {
                    font-weight: bold;
                    color: #1171AF !important;
                }

                /*Images*/
                .background_cta {
                    background-image: url();
                }

                .home-map-image {
                    background-image: url();
                    background-size: cover;
                    background-position: Center Center !important;
                }

                /*Testimonial Page*/
                .related_videos {
                    border-top: 7px solid #46AD4C;
                }

                /*Youtube Video */
                .video_thumb {
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: top center;
                    height: 400px;
                }

                /*Change play button color on all inline video images*/
                .content #playhover,
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
                .main-home #playhover,
                .page-template-hero-min-landing #playhover,
                .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
                .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
                    background: url(../../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
                }

                /*Add line to the botton of inline video image*/
                article #playhover + img {
                    border-bottom: 5px solid #46AD4C;
                }


                /*Fancy Overlay*/
                #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
                    background-image: url(../../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
                    background-size: cover !important;
                    background-position: left top !important;
                }

                /*Archive Pages*/
                .archive-description, .author-box {
                    border-top: 7px solid #46AD4C;
                    border-left: 0px;
                    border-right: 0px;
                }

                .archive .entry-header a {
                    color: #fff;
                    font-size: 1em;
                }


                /*Team Page*/
                .tiled-gallery .gallery-row, .bio-image img {
                    border-bottom: 3px solid #1171AF;
                }

                /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
                .team .entry-content img, .team .gallery-item img {
                    border-bottom: 5px solid #1171AF;
                }

                /*Links*/
                a {
                    color: #1171AF;
                    text-decoration: none !important;
                }

                /*Change current menu color*/
                /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

                /*BTNS*/
                .button {
                    background-color: #46AD4C !important;
                }

                /*Add colored bullet points*/
                .entry-content ul {
                    list-style-type: none;
                    position: relative;
                    padding-left: 0;
                }

                .entry-content ul > li {
                    list-style: none;
                }

                .entry-content ul > li:before {
                    content: "• \00a0 \00a0 \00a0";
                    color: #1171AF; /* Color of the bullet */
                    position: absolute;
                    left: -1em;
                    margin-right: 5px;
                }

                /*Remove bullets on feedback page*/
                .page-template-page_feedback .entry-content ul > li:before {
                    content: "" !important;
                }

                /* Video Player Pages
				--------------------------------------------- */
                .fancybox-wrap:before {
                    background: url(../../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
                    content: ".";
                    position: relative;
                    width: 100%;
                    height: 130px;
                    z-index: 8040;
                    display: block;
                    margin-bottom: 20px;
                    background-size: contain !important;
                }

                /*Homepage - Max Width HeroCard - change line under image*/
                .page-template-hero-max-landing > a > img {
                    border-bottom: 4px solid #1171AF;
                }

                /*change color on landinage page background*/
                .animated-home {
                    background-color: #1171AF;
                }

                /*Homepage - Max Width HeroCard and full page content - change line under image*/
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
                .page-template-hero-max-landing > .row > .col-md-12 a img {
                    border-bottom: 4px solid #1171AF;
                    width: 100%;
                }


                /* Archive Page
				--------------------------------------------- */
                .circle-text:after {
                    background-color: #32C5F4 !important;
                }

                body .entry-content .gfield_label {
                    font-size: 30px !important;
                    color: #1171AF;
                }

                /*Add BG image to full screen pages.  Going to remove this for now*/
                /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
					background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
					background-size: cover;
					background-position: left top !important;
				}*/

                .full-width-content.custom-background {
                    background-image: url("../../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
                    background-attachment: fixed;
                }


                body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
                    border: 1px solid #1171AF;
                    background-color: #FFF8BD;
                }


                .gform_wrapper .percentbar_blue {
                    background-image: url('../../wp-content/themes/nuvo-express/images/opas_15.png');
                    background-repeat: repeat-x;
                    background-color: #1171AF;
                    color: #FFF;
                }




                /* Make some mobile Tweaks
				--------------------------------------------- */
                @media only screen and (max-width: 1023px) {
                    .mobile-cta {
                        color: #1171AF !important;
                    }

                    /*Make mobile help menu primary color*/
                }

                /* This tweaks only Small devices (tablets and phones, 768px and up) */
                @media (max-width: 900px) {
                    .fancybox-youtube > .feedperson, .feedperson.shade30 {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under main videos on homepage / Move background face down*/
                    div.col-md-12.row.no-gutters > a > div.feedperson {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under CTA buttons on homepage*/
                }

                /*Add custom color to the case study block on homepage*/
                .image-holder:after {
                    background-color: rgba(70, 173, 76, 0.7);

                }

                /*Add video overlay on main sizzle as a dynamic layer*/
                .home-hero {
                    background: rgba(17, 113, 175, 1) !important;
                }

                @media only screen and (max-width: 1024px) {

                    .home-hero:after {
                        background: rgba(17, 113, 175, 0.8);
                    }

                }

            </style>
        </div>
		<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
