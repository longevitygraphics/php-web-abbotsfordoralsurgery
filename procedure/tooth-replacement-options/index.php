<?php
$page_title       = 'Tooth Replacement Options in Abbotsford, BC';
$doc_title        = 'Tooth Replacement Options';
$meta_description = 'If you have missing or damaged teeth in Abbotsford, BC, we offer a wide variety of tooth replacement options for you to choose from.';
$og_type          = 'article';
?>
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/head.inc' ?>
<body class="procedure-template-default single single-procedure postid-99083 single-format-standard custom-background custom-header header-image full-width-content"
      itemscope itemtype="https://schema.org/MedicalProcedure">
<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/header.inc' ?>
<div class="site-container">
    <ul class="genesis-skip-link">
        <li><a href="#genesis-nav-primary" class="screen-reader-shortcut"> Skip to primary navigation</a></li>
        <li><a href="#genesis-content" class="screen-reader-shortcut"> Skip to content</a></li>
    </ul>
    <div class="site-inner">
        <div class="wrap">
            <header class="entry-header"><h1 class="entry-title" itemprop="name">Tooth Replacement Options</h1>
            </header>
            <div class="breadcrumb">You are here: <span class="breadcrumb-link-wrap" itemprop="url"><a
                            class="breadcrumb-link" href="../../index.html" itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Home</span></a><meta
                            itemprop="position" content="1"></span> <span aria-label="breadcrumb separator">/</span>
                <span class="breadcrumb-link-wrap" itemprop="url"><a class="breadcrumb-link" href="../index.html"
                                                                     itemprop="url"><span
                                class="breadcrumb-link-text-wrap" itemprop="name">Procedures</span></a><meta
                            itemprop="position" content="2"></span> <span aria-label="breadcrumb separator">/</span>
                Tooth Replacement Options
            </div>
            <div class="row no-gutters">
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implants-bone-graft-donna-abbotsford-bc/index.html"
                       title='Bone Grafting and Dental Implants Restored Donna’s Smile'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/donna-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Donna the dental implants patient in Abbotsford, BC'
                             title='Bone Grafting and Dental Implants Restored Donna’s Smile'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implant-guya-abbotsford-bc/index.html"
                       title='Guya Chose a Dental Implant to Replace Her Tooth'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/guya-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Guya the dental implants patient in Abbotsford, BC'
                             title='Guya Chose a Dental Implant to Replace Her Tooth'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/dental-implant-tooth-extraction-anna-abbotsford-bc/index.html"
                       title='Anna Needed an Extraction and Dental Implant'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/anna-dental-implants-abbotsford-bc-286x200.jpg" width="286"
                             height="200" alt='Anna the dental implants patient in Abbotsford, BC'
                             title='Anna Needed an Extraction and Dental Implant'/>
                    </a>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <a href="../../testimonial/full-arch-restoration-les-abbotsford-bc/index.html"
                       title='Les Underwent a Full-Arch Restoration'>
<span id="playhover"
      style="background: linear-gradient( rgba(17,113,175,0.8), rgba(17,113,175,0.8) ) !important; background-position: center !important;">
<div class="col-md-8 col-xs-8 col-md-offset-4  ">
<span class="spacer50"></span>
<span class="spacer30"></span>
<h4 class="text-light hidden-md hidden-sm hidden-xs">Watch Video</h4>
<sub class="hidden-md hidden-sm hidden-xs text-light ">For The Full Story</sub>
</div>
</span>
                        <span id="playhover" class="col-md-4 col-xs-4 feedperson "
                              style="background-position: center !important; background-size: contain !important;"></span>
                        <img class="fit-img"
                             src="../../wp-content/uploads/les-full-arch-restoration-abbotsford-bc-286x200.jpg"
                             width="286" height="200" alt='Les the full-arch patient in Abbotsford, BC'
                             title='Les Underwent a Full-Arch Restoration'/>
                    </a>
                </div>
            </div>
            <div class="content-sidebar-wrap">
                <main class="content" id="genesis-content">
                    <article
                            class="post-99083 procedure type-procedure status-publish format-standard entry secondary_color">
                        <div class="entry-content" itemprop="description"><p>Missing teeth can negatively affect a
                                patient’s self-esteem and quality of life. Eating problems, dental shifting, and
                                progressive bone loss that affect facial aesthetics all contribute to this. At
                                Abbotsford Oral Surgery and Dental Implant Centre, we believe you deserve the highest
                                quality tooth replacement options, including dental implants and the advanced bone
                                grafting techniques that allow more of our patients the possibility of having dental
                                implants placed. </p>
                            <p>Dr. Esmail of Abbotsford Oral Surgery and Dental Implant Centre is board-certified by
                                both the Royal College of Dentists of Canada and the American Board of Oral and
                                Maxillofacial Surgery. While our practice specializes in dental implant placement, we
                                recognize that implants may not be the preferred tooth replacement option for everyone.
                                That’s why we offer a list of the most popular tooth replacement options here. If you
                                would like to learn more about your tooth replacement options, please contact our
                                practice to schedule a consultation with the oral surgeon to determine the right
                                restoration for you. We look forward to meeting with you to discuss the restoration of
                                your smile.</p>
                            <h2>Dental Implants</h2>
                            <p>Dental implants are designed to provide a foundation for replacement teeth that look,
                                feel, and function just like natural teeth. With dental implants, you can regain the
                                ability to eat comfortably, and smile with confidence knowing that your replacement
                                teeth appear natural and your facial contours are preserved. Implants also keep the jaw
                                healthy and strong, preventing bone deterioration by stimulating the bone in your jaw
                                just like natural teeth. An incredibly versatile tooth replacement solution, dental
                                implants can replace a single tooth, multiple teeth, or even an entire arch of teeth.
                                Unlike bridges, no healthy teeth are damaged to secure the restoration, and implants can
                                last a lifetime with proper care. </p>
                            <p>We perform dental implant placements in the comfort of our Abbotsford, BC, practice. You
                                can read our page on <a href="../../dental-implants-abbotsford-bc/index.html">Dental
                                    Implants</a> to learn more. </p>
                            <h2>Full-Arch Replacement</h2>
                            <p>If you are missing an entire upper or lower arch of teeth, full-arch replacement may be
                                an excellent tooth replacement option for you. During your consultation with Dr. Esmail,
                                you can discuss this option and decide if it is the best solution for your oral health
                                condition.</p>
                            <h3>Is a full-arch replacement right for me?</h3>
                            <p>Most healthy patients can enjoy this life-changing alternative to more traditional tooth
                                restorations such as removable dentures. When you receive a full-arch restoration, you
                                are replacing an entire upper or lower arch of teeth in a single procedure. The result
                                is a fully functional set of new, stable, and life-like teeth, just as with multiple
                                individual implants. Because this treatment is completed in one procedure using just a
                                few implants to stabilize the new teeth, it is cost-effective and efficient. Also,
                                patients still benefit from the durability and health benefits of having individual
                                dental implants placed, but with less post-surgical discomfort and reduced healing time,
                                so you can enjoy your new smile much faster!</p>
                            <p>Full-arch restoration treatment is popular with our patients for many reasons:</p>
                            <ul>
                                <li>Reduced healing time — less post-surgical discomfort and swelling</li>
                                <li>Lasting, non-removable restoration for an entire arch of teeth at one time</li>
                                <li>Fully functional new smile</li>
                                <li>Prevents bone loss and preserves your oral health just like individual dental
                                    implants
                                </li>
                            </ul>
                            <p>Your full-arch of new teeth will be custom designed for your mouth and be very realistic.
                                During your post-surgical healing time, your set of teeth will be created by your
                                restorative dentist. You will be outfitted with a temporary set of teeth the day of your
                                procedure, so you can feel confident in your smile while you wait.</p>
                            <p>We perform full-arch restoration procedures in our Abbotsford, BC, practice. You can
                                learn more about this tooth replacement option on our <a
                                        href="../../full-arch-restoration/index.html">Full-Arch Restoration</a> page.
                            </p>
                            <h2>Bridges (Tooth-Supported)</h2>
                            <p>Fixed tooth-supported bridges are a very common solution for replacing one or more teeth
                                that are next to each other. In this restoration, the bridge is secured into position by
                                using the teeth adjacent to the missing teeth as anchors. Most of the time, your family
                                dentist will need to reshape or grind down the teeth that are used to anchor the bridge.
                                Your dentist will place the bridge over the re-shaped teeth. Bridges are popular because
                                they are a relatively inexpensive way to replace missing teeth. However, bridges do need
                                to be replaced after several years of wear. </p>
                            <h2>Full or Partial Dentures</h2>
                            <p>Full or partial dentures are popular, traditional solutions for replacing any number of
                                teeth in one or both jaws. While some people find dentures to be uncomfortable and
                                cumbersome, others adapt well to them. Your family dentist (or denturist) will fit you
                                with traditional dentures, and the process is generally quick and easy. However,
                                differences in jaw size and shape can make fitting a patient with dentures a challenge.
                                If your dentist/denturist finds that your mouth or jaw needs work from an oral surgeon
                                to prepare for dentures, you will be referred to a trusted local oral surgeon for
                                pre-prosthetic surgery. This type of oral surgery reshapes the bony ridge under the gums
                                so that your dentures are more comfortable and stay in place. </p>
                            <p>Our practice offers pre-prosthetic surgery procedures. You can learn more about this
                                treatment on our <a href="../../pre-prosthetic-surgery/index.html">Pre-Prosthetic
                                    Surgery</a> page.</p>
                        </div>
                    </article>
                    <div class="row cta-footer ">
                        <div class="col-md-3">
                            <a href="../../contact-us/index.html">
                                <button type="button" class="btn-xs btn-alt secondary_color">Request Appointment
                                </button>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <p>
                                As a patient of our office, we want it to be as easy as possible for you to visit our
                                team for care. You can request an appointment by filling out our online form. </p>
                        </div>
                    </div>
                </main>
            </div>
			<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/footer.inc' ?>
            <style type="text/css">
                .primary_color, .breadcrumb, .breadcrumb a, .entry-header {
                    background-color: #1171AF !important;
                    color: #fff !important;
                }

                .secondary_color {
                    background-color: #46AD4C;
                }

                .highlight_color {
                    background-color: #32C5F4 !important;
                }

                ;
                .site-inner {
                    background-color: #1171AF !important;
                }

                /*Main Homepage*/
                .gradient {
                    background: #00AEEF; /* Old browsers */
                    background: -moz-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* FF3.6+ */
                    background: -webkit-gradient(linear, left top, right top, color-stop(0%, #00AEEF), color-stop(100%, #32C5F4)); /* Chrome,Safari4+ */
                    background: -webkit-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Chrome10+,Safari5.1+ */
                    background: -o-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* Opera 11.10+ */
                    background: -ms-linear-gradient(left, #00AEEF 0%, #32C5F4 100%); /* IE10+ */
                    background: linear-gradient(to right, #00AEEF 0%, #32C5F4 100%); /* W3C */
                    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00AEEF', endColorstr='#32C5F4', GradientType=1); /* IE6-9 */
                    Padding: 10%;
                }

                .home input {
                    color: rgba(17, 113, 175, 1) !important;
                }

                /*body.custom-background {
				  background-color: rgba(17,113,175,1) !important;
				}*/

                /*Menu*/
                .nav-primary .sub-menu a {
                    background-color: #1171AF;
                    border-color: #fff;
                    color: #fff !important;
                }

                .nav-primary .genesis-nav-menu.responsive-menu .sub-menu {
                    background-color: #1171AF;
                }

                /*Add primary color to the number CTA*/
                .callus a {
                    font-weight: bold;
                    color: #1171AF !important;
                }

                /*Images*/
                .background_cta {
                    background-image: url();
                }

                .home-map-image {
                    background-image: url();
                    background-size: cover;
                    background-position: Center Center !important;
                }

                /*Testimonial Page*/
                .related_videos {
                    border-top: 7px solid #46AD4C;
                }

                /*Youtube Video */
                .video_thumb {
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: top center;
                    height: 400px;
                }

                /*Change play button color on all inline video images*/
                .content #playhover,
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a #playhover,
                .main-home #playhover,
                .page-template-hero-min-landing #playhover,
                .page-template-hero-max-landing > div.row.no-gutters > div > a > #playhover,
                .single-procedure > div.site-container > div > div > div:nth-child(3) > div > a > #playhover {
                    background: url(../../wp-content/uploads/ABOT-play-button.png) no-repeat center center !important;
                }

                /*Add line to the botton of inline video image*/
                article #playhover + img {
                    border-bottom: 5px solid #46AD4C;
                }


                /*Fancy Overlay*/
                #fancybox-overlay, .fancybox-lock .fancybox-overlay, .fancybox-overlay-fixed, .fancybox-overlay {
                    background-image: url(../../wp-content/uploads/ABOT-Background-Dark.jpg) !important;
                    background-size: cover !important;
                    background-position: left top !important;
                }

                /*Archive Pages*/
                .archive-description, .author-box {
                    border-top: 7px solid #46AD4C;
                    border-left: 0px;
                    border-right: 0px;
                }

                .archive .entry-header a {
                    color: #fff;
                    font-size: 1em;
                }


                /*Team Page*/
                .tiled-gallery .gallery-row, .bio-image img {
                    border-bottom: 3px solid #1171AF;
                }

                /*Hot fix 7.10.2015 Adding border to the images of the staff -BA*/
                .team .entry-content img, .team .gallery-item img {
                    border-bottom: 5px solid #1171AF;
                }

                /*Links*/
                a {
                    color: #1171AF;
                    text-decoration: none !important;
                }

                /*Change current menu color*/
                /*.menu-primary .current-menu-item > ul > li > a > span  {color: #1171AF;}*/

                /*BTNS*/
                .button {
                    background-color: #46AD4C !important;
                }

                /*Add colored bullet points*/
                .entry-content ul {
                    list-style-type: none;
                    position: relative;
                    padding-left: 0;
                }

                .entry-content ul > li {
                    list-style: none;
                }

                .entry-content ul > li:before {
                    content: "• \00a0 \00a0 \00a0";
                    color: #1171AF; /* Color of the bullet */
                    position: absolute;
                    left: -1em;
                    margin-right: 5px;
                }

                /*Remove bullets on feedback page*/
                .page-template-page_feedback .entry-content ul > li:before {
                    content: "" !important;
                }

                /* Video Player Pages
				--------------------------------------------- */
                .fancybox-wrap:before {
                    background: url(../../wp-content/uploads/ABOT-Mark.png) no-repeat center center !important;
                    content: ".";
                    position: relative;
                    width: 100%;
                    height: 130px;
                    z-index: 8040;
                    display: block;
                    margin-bottom: 20px;
                    background-size: contain !important;
                }

                /*Homepage - Max Width HeroCard - change line under image*/
                .page-template-hero-max-landing > a > img {
                    border-bottom: 4px solid #1171AF;
                }

                /*change color on landinage page background*/
                .animated-home {
                    background-color: #1171AF;
                }

                /*Homepage - Max Width HeroCard and full page content - change line under image*/
                .page-template-hero-max-landing-fullpage > .row > .col-md-12 a img,
                .page-template-hero-max-landing > .row > .col-md-12 a img {
                    border-bottom: 4px solid #1171AF;
                    width: 100%;
                }


                /* Archive Page
				--------------------------------------------- */
                .circle-text:after {
                    background-color: #32C5F4 !important;
                }

                body .entry-content .gfield_label {
                    font-size: 30px !important;
                    color: #1171AF;
                }

                /*Add BG image to full screen pages.  Going to remove this for now*/
                /*.full-width-content > div.site-container > div > div > div:nth-child(3) > main > article {
					background-image: url( http://www.abbotsfordoralsurgery.com/wp-content/uploads/ABOT-Background-Main.jpg ) !important;
					background-size: cover;
					background-position: left top !important;
				}*/

                .full-width-content.custom-background {
                    background-image: url("../../wp-content/uploads/ABOT-Background-Dark.jpg") !important;
                    background-attachment: fixed;
                }


                body .gform_wrapper .gform_body .gform_fields .gfield input[type=text]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=email]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=tel]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=url]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=number]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield input[type=password]:focus,
                body .gform_wrapper .gform_body .gform_fields .gfield select:focus {
                    border: 1px solid #1171AF;
                    background-color: #FFF8BD;
                }


                .gform_wrapper .percentbar_blue {
                    background-image: url('../../wp-content/themes/nuvo-express/images/opas_15.png');
                    background-repeat: repeat-x;
                    background-color: #1171AF;
                    color: #FFF;
                }




                /* Make some mobile Tweaks
				--------------------------------------------- */
                @media only screen and (max-width: 1023px) {
                    .mobile-cta {
                        color: #1171AF !important;
                    }

                    /*Make mobile help menu primary color*/
                }

                /* This tweaks only Small devices (tablets and phones, 768px and up) */
                @media (max-width: 900px) {
                    .fancybox-youtube > .feedperson, .feedperson.shade30 {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under main videos on homepage / Move background face down*/
                    div.col-md-12.row.no-gutters > a > div.feedperson {
                        border-bottom: 2px solid #32C5F4;
                    }

                    /*Add line under CTA buttons on homepage*/
                }

                /*Add custom color to the case study block on homepage*/
                .image-holder:after {
                    background-color: rgba(70, 173, 76, 0.7);

                }

                /*Add video overlay on main sizzle as a dynamic layer*/
                .home-hero {
                    background: rgba(17, 113, 175, 1) !important;
                }

                @media only screen and (max-width: 1024px) {

                    .home-hero:after {
                        background: rgba(17, 113, 175, 0.8);
                    }

                }

            </style>
        </div>
		<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/foot.inc' ?>
